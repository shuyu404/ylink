package com.ylink.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.gateway
 * @Author: shuyu
 * @CreateTime: 2022-10-31  19:31
 * @Description:
 * @Version: 1.0
 */
@SpringBootApplication(scanBasePackages = {"com.ylink.gateway","com.ylink.common.redis"})
public class YlinkGateWayApplication {
    public static void main(String[] args) {
        SpringApplication.run(YlinkGateWayApplication.class,args);
    }
}
