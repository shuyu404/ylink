package com.ylink.gateway.config.properties;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.gateway.config.properties
 * @Author: shuyu
 * @CreateTime: 2022-11-01  19:26
 * @Description:
 * @Version: 1.0
 */

public class ValiPathProperties {
    private static Set<String> vailpath=new HashSet<>();

    static {
       //vailpath.add("a/api/auth/login");
    }

    public static Set<String> getValiPath(){
        return vailpath;
    }

    public static boolean hasPath(String path){
        return getValiPath().contains(path);
    }

}
