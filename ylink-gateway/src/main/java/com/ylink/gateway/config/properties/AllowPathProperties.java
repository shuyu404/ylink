package com.ylink.gateway.config.properties;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.gateway.config.properties
 * @Author: shuyu
 * @CreateTime: 2022-11-01  14:19
 * @Description:
 * @Version: 1.0
 */

public class AllowPathProperties {
    private static Set<String> white=new HashSet<>();

    static {
        white.add("/api/auth/login");
        white.add("/api/auth/logout");
        white.add("/api/auth/register");
    }

    public static Set<String> getWhite() {
       return white;
    }

    public static boolean hasPath(String path){
        return getWhite().contains(path);
    }
}
