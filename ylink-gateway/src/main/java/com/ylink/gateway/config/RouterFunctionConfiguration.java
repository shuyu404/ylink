package com.ylink.gateway.config;

import com.ylink.gateway.handler.ValidateCodeHandler;
import com.ylink.gateway.service.ValidateCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.gateway.config
 * @Author: shuyu
 * @CreateTime: 2022-11-01  19:07
 * @Description:
 * @Version: 1.0
 */
@Configuration
public class RouterFunctionConfiguration {
    @Autowired
    private ValidateCodeHandler validateCodeHandler;

    @Bean
    public RouterFunction routerFunction(){
        return RouterFunctions.route(
                RequestPredicates.GET("/api/captchaImage").and(RequestPredicates.accept(MediaType.TEXT_PLAIN)),
                validateCodeHandler);
    }

}
