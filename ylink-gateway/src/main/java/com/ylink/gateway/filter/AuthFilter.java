package com.ylink.gateway.filter;

import com.ylink.common.core.constant.CacheConstants;
import com.ylink.common.core.constant.SecurityConstants;
import com.ylink.common.core.utils.JwtUtils;
import com.ylink.common.core.utils.ServletUtils;
import com.ylink.common.core.utils.StringUtils;
import com.ylink.common.core.utils.TokenUtils;
import com.ylink.common.redis.service.RedisService;
import com.ylink.gateway.config.properties.AllowPathProperties;
import com.ylink.gateway.config.properties.ValiPathProperties;
import com.ylink.gateway.service.ValidateCodeService;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.gateway.filter
 * @Author: shuyu
 * @CreateTime: 2022-11-01  14:18
 * @Description:    认证
 * @Version: 1.0
 */
@Component
public class AuthFilter implements GlobalFilter , Ordered {


    @Autowired
    RedisService redisService;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpRequest.Builder mutate = request.mutate();

        String url = request.getURI().getPath();
        //跳过不需要验证的路径
        if(AllowPathProperties.hasPath(url)){
            return chain.filter(exchange);
        }

        String token = getToken(request);
        if (StringUtils.isEmpty(token)) {
         return  unauthorizedResponse(exchange,"令牌为空，请重新登录");
        }

        Claims claims = JwtUtils.parseToken(token);
        if(claims==null){
            return unauthorizedResponse(exchange,"令牌过期或不正确，请重新登录");
        }
        String userKey = JwtUtils.getUserKey(claims);
        Boolean isLogin = redisService.hasKey(getTokenKey(token));
        if(!isLogin){
            return unauthorizedResponse(exchange,"认证过期，请重新登录");
        }
        String userId = JwtUtils.getUserId(claims);
        String userName = JwtUtils.getUserName(claims);
        if(StringUtils.isEmpty(userId)||StringUtils.isEmpty(userName)){
            return unauthorizedResponse(exchange,"认证失败，请重新登录");
        }
        //设置验证成功信息
        addHeader(mutate, SecurityConstants.USER_KEY,userKey);
        addHeader(mutate, SecurityConstants.DETAILS_USER_ID,userId);
        addHeader(mutate,SecurityConstants.DETAILS_USERNAME,userName);

        //内部请求来源参数清除
        removeHeader(mutate, SecurityConstants.FROM_SOURCE);

        return chain.filter(exchange.mutate().request(mutate.build()).build());
    }

    private String getToken(ServerHttpRequest request){
        return TokenUtils.getToken(request);
    }

    private Mono<Void> unauthorizedResponse(ServerWebExchange exchange,String msg){
        return ServletUtils.webFluxResponseWriter(exchange.getResponse(),msg, HttpStatus.UNAUTHORIZED.value());
    }

    private static String getTokenKey(String token){
        return CacheConstants.LOGIN_TOKEN_KEY+token;
    }

    private void addHeader(ServerHttpRequest.Builder mutate,String key,Object value){
        if(value==null){return;}
        String valueStr = value.toString();
        String valueEncode = ServletUtils.urlEncode(valueStr);
        mutate.header(key,valueEncode);
    }

    private void removeHeader(ServerHttpRequest.Builder mutate, String name)
    {
        mutate.headers(httpHeaders -> httpHeaders.remove(name)).build();
    }

    @Override
    public int getOrder()
    {
        return -100;
    }
}
