package com.ylink.gateway.service.impl;

import com.google.code.kaptcha.Producer;
import com.ylink.common.core.constant.CacheConstants;
import com.ylink.common.core.constant.CaptchaConstants;
import com.ylink.common.core.constant.Constants;
import com.ylink.common.core.domain.R;
import com.ylink.common.core.exception.CaptchaException;
import com.ylink.common.core.utils.StringUtils;
import com.ylink.common.core.utils.sign.Base64;
import com.ylink.common.core.utils.uuid.IdUtils;
import com.ylink.common.redis.service.RedisService;
import com.ylink.gateway.config.properties.CaptchaProperties;
import com.ylink.gateway.service.ValidateCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FastByteArrayOutputStream;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.gateway.service.impl
 * @Author: shuyu
 * @CreateTime: 2022-11-01  19:08
 * @Description: 验证码生成
 * @Version: 1.0
 */
@Service
public class ValidateCodeServiceImpl implements ValidateCodeService {

    @Resource(name = "captchaProducer")
    private Producer captchaProducer;

    @Resource(name = "captchaProducerMath")
    private Producer captchaProducerMath;

    @Autowired
    private CaptchaProperties captchaProperties;
    @Autowired
    RedisService redisService;

    @Override
    public R createCaptcha()  {


        Boolean enabled = captchaProperties.getEnabled();

        if (!enabled) {
            return R.error().put("captchaEnabled", enabled);
        }


        // 保存验证码信息
        String uuid = IdUtils.simpleUUID();
        String verifyKey = CacheConstants.CAPTCHA_CODE_KEY + uuid;

        String capStr = null, code = null;
        BufferedImage image = null;


        String capchaType = captchaProperties.getType();
        if (CaptchaConstants.MATH.equals(capchaType)) {
            String capText = captchaProducerMath.createText();
            capStr = capText.substring(0, capText.lastIndexOf("@"));
            code = capText.substring(capText.lastIndexOf("@") + 1);
            image = captchaProducerMath.createImage(capStr);
        } else if (CaptchaConstants.CHAR.equals(capchaType)) {
            capStr=code= captchaProducer.createText();
            image = captchaProducer.createImage(capStr);
        }

        redisService.setCacheObject(verifyKey,code, Constants.CAPTCHA_EXPIRATION, TimeUnit.MINUTES);

        //转换信息流写出
        FastByteArrayOutputStream os=new FastByteArrayOutputStream();
        try {
            ImageIO.write(image,"jpg",os);
        }catch (IOException e){
            return R.error("验证码转换出错");
        }
        return R.ok().put("uuid",uuid).put("img", Base64.encode(os.toByteArray()));
    }

    @Override
    public void checkCaptcha(String uuid, String code) {
        if(StringUtils.isEmpty(code)){
           throw  new CaptchaException("验证码不能为空");
        }
        if (StringUtils.isEmpty(uuid)){
            throw new CaptchaException("验证码过期");
        }
        String verifyKey = CacheConstants.CAPTCHA_CODE_KEY + uuid;
        String captcha = redisService.getCacheObject(verifyKey);
        redisService.deleteObject(verifyKey);
        if(code.equals(captcha)){
            throw new CaptchaException("验证码错误");
        }
    }
}
