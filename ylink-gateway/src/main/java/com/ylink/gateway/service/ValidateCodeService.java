package com.ylink.gateway.service;

import com.ylink.common.core.domain.R;

import java.io.IOException;

/**
 * @author shuyu
 */
public interface ValidateCodeService {

    /**
     * 生成验证码
     *
     * @return
     * @throws IOException
     */
    public R createCaptcha() throws IOException;


    /**
     * 验证校验码
     *
     * @param uuid   验证码图片uuid
     * @param code 对应的验证码内容
     */
    public void checkCaptcha(String uuid, String code);
}
