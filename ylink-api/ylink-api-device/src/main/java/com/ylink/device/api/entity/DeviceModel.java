package com.ylink.device.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ylink.common.core.web.domain.BaseEntity;
import lombok.Data;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.entity
 * @Author: shuyu
 * @CreateTime: 2022-11-08  17:01
 * @Description: 设备模型类
 * @Version: 1.0
 */
@Data
@TableName("device_model")
public class DeviceModel extends BaseEntity {

    /**模型类型*/
    @TableId(type = IdType.AUTO)
    String modelId;

    /**模型名称*/
    String name;

    /**模型类型*/
    String type;

    /**根id*/
    String rootId;
}
