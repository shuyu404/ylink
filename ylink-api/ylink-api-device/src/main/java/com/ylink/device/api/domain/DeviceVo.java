package com.ylink.device.api.domain;

import com.ylink.device.api.entity.Device;
import com.ylink.device.api.entity.DeviceModel;
import com.ylink.device.api.entity.ModelData;
import lombok.Data;

import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.api.domain
 * @Author: shuyu
 * @CreateTime: 2022-11-10  13:43
 * @Description:
 * @Version: 1.0
 */
@Data
public class DeviceVo {
    Device device;
    DeviceModel deviceModel;
    List<ModelData> modelDataList;

}
