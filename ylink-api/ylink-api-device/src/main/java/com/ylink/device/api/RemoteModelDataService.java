package com.ylink.device.api;

import com.ylink.common.core.constant.ServiceNameConstant;
import com.ylink.common.core.domain.Rpc;
import com.ylink.device.api.domain.DeviceVo;
import com.ylink.device.api.entity.ModelData;
import com.ylink.device.api.factory.RemoteModelDataFailbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author shuyu
 */
@FeignClient(contextId = "remoteModelDataService",value = ServiceNameConstant.DEVICE_SERVICE,fallbackFactory= RemoteModelDataFailbackFactory.class)
public interface RemoteModelDataService {

    /**
     * 根据设备id获取数据模型列表
     * @param clientId  设备id
     * @return  数据模型列表
     */
    @GetMapping("/modelData/deviceVo/{clientId}")
    public Rpc<DeviceVo> getDeviceVo(@PathVariable("clientId") String clientId);

}
