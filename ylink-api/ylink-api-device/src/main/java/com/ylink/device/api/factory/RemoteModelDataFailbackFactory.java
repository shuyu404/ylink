package com.ylink.device.api.factory;

import com.ylink.common.core.domain.Rpc;
import com.ylink.device.api.RemoteModelDataService;
import com.ylink.device.api.domain.DeviceVo;
import com.ylink.device.api.entity.ModelData;
import org.springframework.cloud.openfeign.FallbackFactory;

import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.api.factory
 * @Author: shuyu
 * @CreateTime: 2022-11-09  21:38
 * @Description:
 * @Version: 1.0
 */

public class RemoteModelDataFailbackFactory implements FallbackFactory<RemoteModelDataService> {
    @Override
    public RemoteModelDataService create(Throwable cause) {
        return new RemoteModelDataService() {


            @Override
            public Rpc<DeviceVo> getDeviceVo(String clientId) {
                return null;
            }
        };
    }
}
