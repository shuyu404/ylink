package com.ylink.device.api.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ylink.common.core.web.domain.BaseEntity;
import lombok.Data;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.entity
 * @Author: shuyu
 * @CreateTime: 2022-11-09  13:44
 * @Description:
 * @Version: 1.0
 */
@Data
@TableName("model_data")
public class ModelData extends BaseEntity {

    /**
     * 数据id
     */
    @TableId
    String dataId;

    /**
     * 数据名称
     */
    String name;

    /**
     * 数据标识
     */
    String flag;

    /**
     * 数据类型
     */
    Integer type;

    /**
     * 模型id
     */
    String modelId;

    /**
     * 下发数据类型
     */
    String sendType;

    /**
     * 打开时触发数据
     */
    String sendOnData;

    /**
     * 关闭时触发数据
     */
    String sendOffData;
}
