package com.ylink.device.api.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ylink.common.core.web.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.entity
 * @Author: shuyu
 * @CreateTime: 2022-11-08  14:38
 * @Description:    设备模型
 * @Version: 1.0
 */
@Data
@TableName("device")
public class Device extends BaseEntity {
    /**
     * 设备clientId
     */
    @TableId(type = IdType.AUTO)
    String clientId;

    /**设备用户名*/
    String username;

    /**设备密码*/
    String password;

    /**设备状态*/
    String status;

    /**设备模型id*/
    String modelId;

    /**设备位置*/
    String position;

    /**设备订阅*/
    String sub;

    /**设备发布*/
    String pub;

    /**设备名称*/
    String name;

    /**设备根名称*/
    String rootId;

    /**
     * 状态最近刷新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @TableField(exist = false)
    Date lastUpdateTime;

    @TableField(exist = false)
    /**报警状态*/
    String warnStatus;
}
