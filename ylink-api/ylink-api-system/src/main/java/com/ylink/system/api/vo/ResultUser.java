package com.ylink.system.api.vo;

import com.ylink.system.api.entity.SysUser;
import lombok.Data;

import java.sql.Date;
import java.util.Set;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.api.vo
 * @Author: shuyu
 * @CreateTime: 2022-11-02  15:43
 * @Description:    返回用户类型
 * @Version: 1.0
 */
@Data
public class ResultUser {

    /**
     * 用户名id
     */
    private String userid;

    /**
     * 用户名账号
     */
    private String username;

    /**
     * 登录时间
     */
    private Long loginTime;

    /**
     * 过期时间
     */
    private Long expireTime;

    /**
     * 登录IP地址
     */
    private String ipaddr;

    /**
     * 权限列表
     */
    private Set<String> permissions;

    /**
     * 角色列表
     */
    private Set<String> roles;

    /**
     * 用户信息
     */
    private UserVo userVo;
}
