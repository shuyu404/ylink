package com.ylink.system.api.model;

import lombok.Data;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.api.model
 * @Author: shuyu
 * @CreateTime: 2022-11-01  22:30
 * @Description:
 * @Version: 1.0
 */
@Data
public class RegisterUser {

    String username;

    String password;
}
