package com.ylink.system.api;

import com.ylink.common.core.domain.Rpc;
import com.ylink.system.api.factory.RemoteUserFailbackFactory;
import com.ylink.system.api.model.LoginUser;
import com.ylink.system.api.model.RegisterUser;
import org.springframework.cloud.openfeign.FeignClient;
import com.ylink.common.core.constant.ServiceNameConstant;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author shuyu
 */
@FeignClient(contextId = "remoteUserService",value = ServiceNameConstant.SYSTEM_SERVICE,fallbackFactory = RemoteUserFailbackFactory.class)
public interface RemoteUserService {

    /**
     * 远程获取loginUser
     * @param username  用户名
     * @return
     */
    @GetMapping("/user/info/{username}")
    public Rpc<LoginUser> info(@PathVariable("username")String username);

    /**
     * 注册用户
     * @param registerUser 用户注册类
     * @return 返回是否注册成功
     */
    @PostMapping("/user/regist")
    public Rpc<Boolean> registerUser(@RequestBody RegisterUser registerUser);
}
