package com.ylink.system.api.model;

import com.ylink.system.api.entity.SysUser;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.api.model
 * @Author: shuyu
 * @CreateTime: 2022-10-31  21:09
 * @Description:
 * @Version: 1.0
 */
@Data
public class LoginUser implements Serializable {

    /**
     * 用户唯一标识
     */
    private String token;

    /**
     * 用户名id
     */
    private String userid;

    /**
     * 用户名账号
     */
    private String username;

    /**
     * 登录时间
     */
    private Long loginTime;

    /**
     * 过期时间
     */
    private Long expireTime;

    /**
     * 登录IP地址
     */
    private String ipaddr;

    /**
     * 权限列表
     */
    private Set<String> permissions;

    /**
     * 角色列表
     */
    private Set<String> roles;

    /**
     * 用户信息
     */
    private SysUser sysUser;
}
