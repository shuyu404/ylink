package com.ylink.system.api.factory;

import com.ylink.common.core.domain.R;
import com.ylink.common.core.domain.Rpc;
import com.ylink.system.api.RemoteUserService;
import com.ylink.system.api.model.LoginUser;
import com.ylink.system.api.model.RegisterUser;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.api.factory
 * @Author: shuyu
 * @CreateTime: 2022-10-31  21:55
 * @Description:
 * @Version: 1.0
 */

public class RemoteUserFailbackFactory implements FallbackFactory<RemoteUserService> {

    private static final Logger log = LoggerFactory.getLogger(RemoteUserFailbackFactory.class);

    @Override
    public RemoteUserService create(Throwable cause) {

        return new RemoteUserService() {
            @Override
            public Rpc<LoginUser> info(String username) {
                log.error("用户info接口调用出错");
                return Rpc.fail();
            }

            @Override
            public Rpc<Boolean> registerUser(RegisterUser registerUser) {
                log.error("用户registerUser接口调用出错");
                return Rpc.fail();
            }
        };
    }


}
