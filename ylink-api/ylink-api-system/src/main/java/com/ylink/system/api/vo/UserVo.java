package com.ylink.system.api.vo;

import lombok.Data;

import java.sql.Date;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.api.vo
 * @Author: shuyu
 * @CreateTime: 2022-11-02  15:48
 * @Description:
 * @Version: 1.0
 */
@Data
public class UserVo {
    /** 用户ID */
    String userId;

    /** 根ID */
    String rootId;

    /** 用户账号 */
    String userName;

    /** 用户昵称 */
    String nickName;


    /** 用户性别 */
    String sex;

    /** 用户头像 */
    String avatar;

    /** 帐号状态（0正常 1停用） */
    String status;

    /** 删除标志（0代表存在 2代表删除） */
    String delFlag;

    /** 最后登录IP */
    String loginIp;

    /** 最后登录时间 */
    Date loginDate;


    /** 角色ID */
    String roleId;
}
