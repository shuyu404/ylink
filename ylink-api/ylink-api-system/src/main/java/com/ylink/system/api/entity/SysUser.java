package com.ylink.system.api.entity;


import com.baomidou.mybatisplus.annotation.TableId;
import com.ylink.common.core.web.domain.BaseEntity;
import lombok.Data;

import java.sql.Date;


/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.entity
 * @Author: shuyu
 * @CreateTime: 2022-10-31  19:39
 * @Description:    用户对象
 * @Version: 1.0
 */
@Data
public class SysUser extends BaseEntity {

    /** 用户ID */
    @TableId
    String userId;

    /** 根ID */
    String rootId;

    /** 用户账号 */
    String userName;

    /** 用户昵称 */
    String nickName;

    /**用户类型*/
    String userType;

    /** 用户邮箱 */
    String email;

    /** 手机号码 */
    String phoneNumber;

    /** 用户性别 */
    String sex;

    /** 用户头像 */
    String avatar;

    /** 密码 */
    String password;

    /** 帐号状态（0正常 1停用） */
    String status;

    /** 删除标志（0代表存在 2代表删除） */
    String delFlag;

    /** 最后登录IP */
    String loginIp;

    /** 最后登录时间 */
    Date loginDate;

    /** 角色ID */
    String roleId;

}
