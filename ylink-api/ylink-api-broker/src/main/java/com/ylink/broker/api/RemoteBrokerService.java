package com.ylink.broker.api;

import com.ylink.broker.api.entity.SendControllerData;
import com.ylink.broker.api.factory.RemoteBrokerFailbackFactory;
import com.ylink.common.core.constant.ServiceNameConstant;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @author Alie
 */
@FeignClient(contextId = "remoteBrokerService",value = ServiceNameConstant.BROKER_SERVICE,fallbackFactory= RemoteBrokerFailbackFactory.class)
public interface RemoteBrokerService {

    @PostMapping("/sendMsg/send")
    public void send(SendControllerData controllerData);
}
