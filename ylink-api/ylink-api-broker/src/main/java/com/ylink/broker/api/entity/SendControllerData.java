package com.ylink.broker.api.entity;

import lombok.Data;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.broker.api.entity
 * @Author: shuyu
 * @CreateTime: 2022-12-17  17:10
 * @Description:    控制下发数据类型
 * @Version: 1.0
 */
@Data
public class SendControllerData {
    /**
     *设备id
     */
    String deviceId;

    /**
     * 根id
     */
    String rootId;

    /**
     * 类型
     */
    String type;

    /**
     * 数据
     */
    String data;
}
