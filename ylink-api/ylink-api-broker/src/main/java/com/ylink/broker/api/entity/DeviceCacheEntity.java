package com.ylink.broker.api.entity;

import lombok.Data;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.broker.api.entity
 * @Author: shuyu
 * @CreateTime: 2022-12-31  15:11
 * @Description: 缓存数据类
 * @Version: 1.0
 */
@Data
public class DeviceCacheEntity {

    /**
     * 在线状态
     */
    String onlineStatus;

    /**
     * 报警状态
     */
    String warnStatus;

    /**
     * 在线状态更新时间
     */
    String onlineUpdateTime;
}
