package com.ylink.broker.api.factory;

import com.ylink.broker.api.RemoteBrokerService;
import com.ylink.broker.api.entity.SendControllerData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.broker.api.factory
 * @Author: shuyu
 * @CreateTime: 2022-12-17  17:08
 * @Description:
 * @Version: 1.0
 */

public class RemoteBrokerFailbackFactory implements FallbackFactory<RemoteBrokerService> {
    private static Logger LOGGER=LoggerFactory.getLogger(RemoteBrokerFailbackFactory.class);

    @Override
    public RemoteBrokerService create(Throwable cause) {
        return new RemoteBrokerService() {

            @Override
            public void send(SendControllerData controllerData) {
                LOGGER.error("SendControllerData 执行失败执，行数据：{}",controllerData);
            }
        };
    }
}
