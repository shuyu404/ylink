export const menuApi={}

import request from '@/utils/request'

const baseUrl="/system/menu"

/**获取选择树 */
menuApi.TreeSelect=()=>{
    return request({
        url: baseUrl + '/treeselect',
        method: 'get',
      })
}