import request from '@/utils/request'

export const modelDataApi = {}

const baseUrl = '/device/modelData'

/**获取数据可选项 */
modelDataApi.getOptions=()=>{
    return request({
        url: baseUrl + '/types',
        method: 'get',
      })
}

/**添加数据 */
modelDataApi.add=(data)=>{
  return request({
    url: baseUrl + '/add',
    method: 'post',
    data
  })
}

/**删除模型数据 */
modelDataApi.delete=(dataId)=>{
  return request({
    url: baseUrl + '/delete/'+dataId,
    method: 'delete'
  })
}

/**获取模型列表 */
modelDataApi.getList=(modeId)=>{
  return request({
    url: baseUrl + '/list/'+modeId,
    method: 'get',
  })
}

/**获取模型项详细数据 */
modelDataApi.getInfo=(modelDataId)=>{
  return request({
    url:baseUrl + '/info/'+modelDataId,
    method:'get'
  })
}

/**获取模型项详细数据 */
modelDataApi.update=(data)=>{
  return request({
    url:baseUrl + '/update',
    method:'put',
    data
  })
}