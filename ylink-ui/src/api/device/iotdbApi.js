import request from '@/utils/request'

export const iotdbApi = {}

const baseUrl = "/device/dataDML"

/**
 * 获取最近数据
 * @param {} clientId 
 * @returns 
 */
iotdbApi.getLast = (clientId) => {
  return request({
    url: baseUrl + '/getLast/' + clientId,
    method: 'get',
  })
}

/**
 * 获取序列数据
 * @param {*} param0 
 * @returns 
 */
iotdbApi.getDataSeries = ({ deviceId, dataId, start, end }) => {
  var data = {
    deviceId,
    dataId,
    start,
    end
  }
  return request({
    url: baseUrl + '/dataSeries',
    method: 'get',
    params: data
  })
}

/**获取数据数量序列 */
iotdbApi.getDataCountSeries = ({ rootId, deviceId, dataId, start, end }) => {
  var data = {
    rootId, deviceId, dataId, start, end
  }
  return request({
    url: baseUrl + "/dataCountSeries",
    method: 'get',
    params: data
  })
}


/**获取报警数据序列 */
iotdbApi.getDataSeriesWarn = ({ rootId, deviceId, dataId, start, end }) => {
  var data = {
    rootId, deviceId, dataId, start, end
  }
  return request({
    url: baseUrl + "/dataSeriesWarn",
    method: 'get',
    params: data
  })
}