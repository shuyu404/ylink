import request from '@/utils/request'

export const deviceModelApi={}

const baseUrl = '/device/deviceModel'

/**添加设备模型*/
deviceModelApi.add=(data)=>{
  return request({
    url: baseUrl + '/add',
    method: 'post',
    data
  })
}

/**获取模型列表*/
deviceModelApi.getList=(query)=>{
  return request({
    url: baseUrl + '/list',
    method: 'get',
    params:query
  })
}

/**更新设备信息*/
deviceModelApi.update=(data)=>{
  return request({
    url: baseUrl + '/update',
    method: 'put',
    data
  })
}

/**删除设备信息*/
deviceModelApi.delete=(modelId)=>{
  return request({
    url: baseUrl + '/delete/'+modelId,
    method: 'delete',
  })
}

/**获取设备详细信息 */
deviceModelApi.getInfo=(modelId)=>{
  return request({
    url: baseUrl + '/info/'+modelId,
    method: 'get',
  })
}

/**获取可选项 */
deviceModelApi.getOptions=()=>{
  return request({
    url: baseUrl + '/options',
    method: 'get',
  })
}

