import request from '@/utils/request'

export const sendDataApi={}

const baseUrl='/device/sendData'

sendDataApi.setOn=({deviceId,dataId})=>{
    return request({
        url: baseUrl + '/on/'+deviceId+'/'+dataId,
        method: 'get',
      })
}

sendDataApi.setOff=({deviceId,dataId})=>{
    return request({
        url: baseUrl + '/off/'+deviceId+'/'+dataId,
        method: 'get',
      })
}

