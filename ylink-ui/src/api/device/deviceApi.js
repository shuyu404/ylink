import request from '@/utils/request'

export const deviceApi = {}

const baseUrl = '/device/device'

/**添加设备*/
deviceApi.add = (data) => {
  return request({
    url: baseUrl + '/add',
    method: 'post',
    data
  })
}

/**获取设备列表*/
deviceApi.getList = (query) => {
  return request({
    url: baseUrl + '/list',
    method: 'get',
    params: query
  })
}

/**删除设备*/
deviceApi.delete = (clientId) => {
  return request({
    url: baseUrl + '/delete/' + clientId,
    method: 'delete'
  })
}

/**更新设备数据*/
deviceApi.update = (data) => {
  return request({
    url: baseUrl + '/update',
    method: 'put',
    data
  })
}

/**获取详细信息 */
deviceApi.getInfo = (clientId) => {
  return request({
    url: baseUrl + '/info/' + clientId,
    method: 'get',
  })
}

deviceApi.getSelectOptions = () => {
  return request({
    url: baseUrl + '/selectList',
    method: 'get',
  })
}

/**根据设备id获取模型下拉列表 */
deviceApi.getModelOptions = (deviceId) => {
  return request({
    url: baseUrl + '/modelSelectList/' + deviceId,
    method: 'get',
  })
}

/**获取设备在线报警数量 */
deviceApi.getDeviceStatusCount = () => {
  return request({
    url: baseUrl + '/deviceStatusCount',
    method: 'get',
  })
}