import request from '@/utils/request'

export const sceneApi = {}

const baseUrl = "/device/scene"

/**获取列表 */
sceneApi.getList = (query) => {
    return request({
        url: baseUrl + '/list',
        method: 'get',
        params: query
    })
}

/**添加 */
sceneApi.add = (data) => {
    return request({
        url: baseUrl + '/add',
        method: 'post',
        data
    })
}

/**更新 */
sceneApi.update = (data) => {
    return request({
        url: baseUrl + '/update',
        method: 'put',
        data
    })
}

/**删除 */
sceneApi.delete=(sceneId)=>{
    return request({
        url: baseUrl + '/delete/'+sceneId,
        method:'delete'
    })
}

/**获取下拉列表 */
sceneApi.getSelectOptions=()=>{
    return request({
        url: baseUrl + '/selectList',
        method: 'get',
    })
}


