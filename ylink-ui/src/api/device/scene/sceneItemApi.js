import request from '@/utils/request'

export const sceneItemApi = {}

const baseUrl = "/device/sceneItem"

/**获取类型列表 */
sceneItemApi.getClassList = () => {
    return request({
        url: baseUrl + '/class',
        method: 'get',
    })
}

/**获取类型列表 */
sceneItemApi.getBoard = (configId) => {
    return request({
        url: baseUrl + '/getBoard/' + configId,
        method: 'get',
    })
}

/**获取类型列表 */
sceneItemApi.getLib = (classId) => {
    return request({
        url: baseUrl + '/lib/' + classId,
        method: 'get',
    })
}

/**保存画布 */
sceneItemApi.saveBoard = (configId, data) => {
    return request({
        url: baseUrl + '/saveBoard/' + configId,
        method: 'post',
        data
    })
}