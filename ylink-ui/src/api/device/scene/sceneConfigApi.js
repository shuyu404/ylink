import request from '@/utils/request'

export const sceneConfigApi = {}

const baseUrl = "/device/sceneConfig"

/**获取列表 */
sceneConfigApi.getList = (query) => {
    return request({
        url: baseUrl + '/list',
        method: 'get',
        params: query
    })
}

sceneConfigApi.add=(data)=>{
    return request({
        url: baseUrl + '/add',
        method: 'post',
        data
    })
}

/**删除列表 */
sceneConfigApi.delete = (id) => {
    return request({
        url: baseUrl + '/delete/'+id,
        method: 'delete'
    })
}

