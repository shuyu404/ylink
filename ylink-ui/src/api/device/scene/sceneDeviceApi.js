import request from '@/utils/request'

export const sceneDeviceApi = {}

const baseUrl = "/device/sceneDevice"


/**获取列表 */
sceneDeviceApi.getList = (sceneId,query) => {
    return request({
        url: baseUrl + '/list/'+sceneId,
        method: 'get',
        params: query
    })
}

/**添加 */
sceneDeviceApi.add=(data)=>{
    return request({
        url: baseUrl + '/add',
        method: 'post',
        data
    })
}

/**删除 */
sceneDeviceApi.delete=(id)=>{
    return request({
        url: baseUrl + '/delete/'+id,
        method: 'delete'
    })
}

/**获取组态设备下拉列表 */
sceneDeviceApi.deviceOptions=(configId)=>{
    return request({
        url: baseUrl + '/selectList/'+configId,
        method: 'get'
    })
}