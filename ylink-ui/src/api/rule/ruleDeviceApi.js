import request from '@/utils/request'

export const ruleDeviceApi = {}


ruleDeviceApi.add=(data)=>{
    return request({
        url: '/rule/ruleDevice/add',
        method: 'post',
        data
    })
}

ruleDeviceApi.getInfo=({deviceId,dataId})=>{
    return request({
        url: '/rule/ruleDevice/info/'+deviceId+"/"+dataId,
        method: 'get'
    })
}