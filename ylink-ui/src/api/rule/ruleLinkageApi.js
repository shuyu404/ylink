import request from '@/utils/request'

export const ruleLinkageApi = {}

ruleLinkageApi.getList = (params) => {
    return request({
        url: '/rule/ruleLinkage/list',
        method: 'get',
        params
    })
}

ruleLinkageApi.add = (data) => {
    return request({
        url: '/rule/ruleLinkage/add',
        method: 'post',
        data
    })
}

ruleLinkageApi.getInfo = (linkageId) => {
    return request({
        url: '/rule/ruleLinkage/info/' + linkageId,
        method: 'get',
    })
}

ruleLinkageApi.delete = (linkageId) => {
    return request({
        url: '/rule/ruleLinkage/delete/' + linkageId,
        method: 'delete'
    })
}

ruleLinkageApi.changeStatus = ( id, status ) => {
    var data = { id, status }
    return request({
        url: '/rule/ruleLinkage/changeStatus',
        method: 'put',
        data
    })
}

ruleLinkageApi.update=(data)=>{
    return request({
        url: '/rule/ruleLinkage/update',
        method: 'put',
        data
    })
}