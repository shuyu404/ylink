import request from '@/utils/request'

export const ruleTrigger = {}

// 获取触发器列表
ruleTrigger.getList = (params) => {
    return request({
        url: '/rule/trigger/list',
        method: 'get',
        params
    })
}

//添加触发器
ruleTrigger.add = (data) => {
    return request({
        url: '/rule/trigger/add',
        method: 'post',
        data
    })
}

//删除触发器
ruleTrigger.delete=(triggerId)=>{
    return request({
        url: '/rule/trigger/delete/'+triggerId,
        method: 'delete'
    })
}

//改变状态
ruleTrigger.changeStatus=(id,status)=>{
    var data={id,status}
    return request({
        url: '/rule/trigger/update',
        method: 'put',
        data
    })
}

//获取下拉选项
ruleTrigger.getSelectOptios=()=>{
    return request({
        url: '/rule/trigger/selectList',
        method: 'get'
    })
}