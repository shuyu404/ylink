import request from '@/utils/request'

export const roleApi = {}

const baseUrl = '/system/role'


/**
 * 获取角色列表
 */
roleApi.getList = function (query) {
  return request({
    url: baseUrl + '/list',
    method: 'get',
    params: query
  })
}
/**所有角色选项列表 */
roleApi.getListAll = function () {
  return request({
    url: baseUrl + '/listAll',
    method: 'get',
  })
}

/**
 * 添加角色
 */
roleApi.addRole = function (data) {
  return request({
    url: baseUrl + '/add',
    method: 'post',
    data
  })
}

/**
 * 更新角色信息
 */
roleApi.upateRole = function (data) {
  return request({
    url: baseUrl + '/update',
    method: 'put',
    data
  })
}

/**
 * 获取角色详细信息
 */
roleApi.getInfo = function (roleid) {
  return request({
    url: baseUrl + '/info/' + roleid,
    method: 'get',
  })
}

/**
 * 改变状态
*/
roleApi.changeStatus = (roleId, status) => {
  const data = {
    roleId,
    status
  }
  return request({
    url: baseUrl + '/changeStatus',
    method: 'put',
    data
  })
}

/**
 * 删除角色
 */
roleApi.deleteRole = function (roleid) {
  return request({
    url: baseUrl + '/delete/' + roleid,
    method: 'delete',
  })
}
