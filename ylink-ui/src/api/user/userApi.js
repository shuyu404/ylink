import request from '@/utils/request'

export const userApi = {}

const baseUrl = "/system/user"

/**获取用户列表 */
userApi.getList = (query) => {
    return request({
        url: baseUrl + '/list',
        method: 'get',
        params: query
    })
}

/** 添加用户信息 */
userApi.addUser = (data) => {
    return request({
        url: baseUrl + '/add',
        method: 'post',
        data
    })
}

/**删除用户 */
userApi.deleteUser = (userId) => {
    return request({
        url: baseUrl + '/delete/' + userId,
        method: 'delete',
    })
}

/**获取用户信息 */
userApi.getUserInfo = (userId) => {
    return request({
        url: baseUrl + '/userInfo/' + userId,
        method: 'get',
    })
}

/** 更新用户信息*/
userApi.updateUser = (data) => {
    return request({
        url: baseUrl + '/update',
        method: 'put',
        data
    })
}

/**更新用户状态 */
userApi.changeStatus = (userId, status) => {
    const data = {
        userId,
        status
    }
    return request({
        url: baseUrl + '/changeStatus',
        method: 'put',
        data
    })
}