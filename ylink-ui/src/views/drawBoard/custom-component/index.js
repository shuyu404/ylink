import Vue from 'vue'

const components = [
  //  'LineShape',
   // 'Picture',
    'VText'
]

Vue.component('VText',()=>import ('./VText/Component'))
Vue.component('VTextAttr', () => import(`./VText/Attr.vue`))

Vue.component('LineShape',()=>import ('./LineShape/Component.vue'))
Vue.component('LineShapeAttr', () => import(`./LineShape/Attr.vue`))

Vue.component('Picture',()=>import ('./Picture/Component.vue'))
Vue.component('PictureAttr', () => import(`./Picture/Attr.vue`))

Vue.component('VButton',()=>import ('./VButton/Component.vue'))
Vue.component('VButtonAttr', () => import(`./VButton/Attr.vue`))