// 公共样式
export const commonStyle = {
    rotate: 0,
    opacity: 1,
}

export const commonAttr = {
    events: {},
 //   groupStyle: {}, // 当一个组件成为 Group 的子组件时使用
    isLock: false, // 是否锁定组件// 编辑组件时记录当前使用的是哪个折叠面板，再次回来时恢复上次打开的折叠面板，优化用户体验
}

// 编辑器左侧组件列表
const list = [
    {   cid:'1',
        component: 'VText',
        label: '文字',
        propValue: '双击编辑文字',
        icon: 'wenben',
        style: {
            width: 200,
            height: 28,
            fontSize: 16,
            fontWeight: 400,
            color: '',
        },
    },
    {
        cid:'2',
        component: 'VButton',
        label: '按钮',
        propValue: '按钮',
        icon: 'button',
        style: {
            width: 100,
            height: 34,
            borderWidth: 1,
            borderColor: '',
            fontSize: '',
            color: '',
            backgroundColor: '',
        },
    },
    {
        cid:'3',
        component: 'Picture',
        label: '图片',
        icon: 'tupian',
        propValue: {
            url: "https://img1.baidu.com/it/u=1530415541,1168703334&fm=253&fmt=auto&app=138&f=JPEG?w=305&h=221",
            flip: {
                horizontal: false,
                vertical: false,
            },
        },
        style: {
            width: 300,
            height: 200,
        },
    },
    {
        cid:'5',
        component: 'LineShape',
        label: '直线',
        propValue: '',
        icon: 'zhixian',
        style: {
            width: 200,
            height: 2,
            backgroundColor: '#000',
        },
    },



]

for (let i = 0, len = list.length; i < len; i++) {
    const item = list[i]
    item.style = { ...commonStyle, ...item.style }
    list[i] = { ...commonAttr, ...item }
}

export default list
