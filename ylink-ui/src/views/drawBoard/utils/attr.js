export const styleData = [
    { key: 'left', label: 'x 坐标' },
    { key: 'top', label: 'y 坐标' },
    { key: 'rotate', label: '旋转角度' },
    { key: 'width', label: '宽' },
    { key: 'height', label: '高' },
    { key: 'color', label: '颜色' },
    { key: 'backgroundColor', label: '背景色' },
    { key: 'borderWidth', label: '边框宽度' },
    { key: 'borderColor', label: '边框颜色' },
    { key: 'borderRadius', label: '边框半径' },
    { key: 'fontSize', label: '字体大小' },
    { key: 'fontWeight', label: '字体粗细' },
    { key: 'opacity', label: '不透明度' },
]

export const styleMap = {
    left: 'x 坐标',
    top: 'y 坐标',
    rotate: '旋转角度',
    width: '宽',
    height: '高',
    color: '颜色',
    backgroundColor: '背景色',
    borderWidth: '边框宽度',
    borderColor: '边框颜色',
    borderRadius: '边框半径',
    fontSize: '字体大小',
    fontWeight: '字体粗细',
    lineHeight: '行高',
    letterSpacing: '字间距',
    textAlign: '左右对齐',
    verticalAlign: '上下对齐',
    opacity: '不透明度',
}

export const styleLib={
    left: {
        label:'x 坐标',type:'number'
    },
    top: {
        label:'y 坐标',type:'number'
    },
    rotate: {
        label:'旋转角度',type:'number'
    },
    width: {
        label:'宽',type:'number'
    },
    height: {
        label:'高',type:'number'
    },
    color: {
        label:'颜色',type:'color'
    },
    backgroundColor:{
        label:'背景色',type:'color'
    },
    borderWidth: {
        label:'边框宽度',type:'number'
    },
    borderColor: {
        label:'边框颜色',type:'number'
    },
    fontSize: {
        label:'字体大小',type:'number'
    },
    fontWeight: {
        label:'字体粗细',type:'number'
    },
    opacity: {
        label:'不透明度',type:'number'
    }
}

export const textAlignOptions = [
    {
        label: '左对齐',
        value: 'left',
    },
    {
        label: '居中',
        value: 'center',
    },
    {
        label: '右对齐',
        value: 'right',
    },
]

export const borderStyleOptions = [
    {
        label: '实线',
        value: 'solid',
    },
    {
        label: '虚线',
        value: 'dashed',
    },
]

export const verticalAlignOptions = [
    {
        label: '上对齐',
        value: 'top',
    },
    {
        label: '居中对齐',
        value: 'middle',
    },
    {
        label: '下对齐',
        value: 'bottom',
    },
]

export const selectKey = ['textAlign', 'borderStyle', 'verticalAlign']

export const optionMap = {
    textAlign: textAlignOptions,
    borderStyle: borderStyleOptions,
    verticalAlign: verticalAlignOptions,
}
