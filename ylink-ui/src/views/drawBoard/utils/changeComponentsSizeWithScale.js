import { deepCopy } from './utils'
import store from '@/store'
import { divide, multiply } from 'mathjs'

const needToChangeAttrs = ['top', 'left', 'width', 'height', 'fontSize']
export default function changeComponentsSizeWithScale(scale) {
    const componentData = deepCopy(store.state.drawBoard.componentData)

    componentData.forEach(component => {
        Object.keys(component.style).forEach(key => {
            if (needToChangeAttrs.includes(key)) {
                if (key === 'fontSize' && component.style[key] === '') return

                // 根据原来的比例获取样式原来的尺寸
                // 再用原来的尺寸 * 现在的比例得出新的尺寸
                component.style[key] = format(getOriginStyle(component.style[key], store.state.drawBoard.canvasStyleData.scale), scale)
            }
        })
    })



    //store.commit('setComponentData', componentData)
    console.log(store._mutations);
    store._mutations['drawBoard/setComponentData'][0](componentData)
    // 更新画布数组后，需要重新设置当前组件，否则在改变比例后，直接拖动圆点改变组件大小不会生效 https://github.com/woai3c/visual-drag-demo/issues/74

    //store.commit('setCurComponent', { component: componentData[store.state.curComponentIndex], index: store.state.curComponentIndex })
    store._mutations['drawBoard/setCurComponent'][0]({ component: componentData[store.state.drawBoard.curComponentIndex], index: store.state.drawBoard.curComponentIndex })
    // store.commit('setCanvasStyle', {
    //     ...store.state.canvasStyleData,
    //     scale,
    // })
    store._mutations['drawBoard/setCanvasStyle'][0]({
        ...store.state.drawBoard.canvasStyleData,
        scale,
    })
}

export function getchangeComponentsSizeWithScale(scale,oldScale, componentData) {
    componentData = deepCopy(componentData)
    componentData.forEach(component => {
        Object.keys(component.style).forEach(key => {
            if (needToChangeAttrs.includes(key)) {
                if (key === 'fontSize' && component.style[key] === '') return

                // 根据原来的比例获取样式原来的尺寸
                // 再用原来的尺寸 * 现在的比例得出新的尺寸
                component.style[key] = format(getOriginStyle(component.style[key], oldScale), scale)
            }
        })
    })
    return componentData
}

const needToChangeAttrs2 = ['width', 'height', 'fontSize']
export function changeComponentSizeWithScale(component) {
    Object.keys(component.style).forEach(key => {
        if (needToChangeAttrs2.includes(key)) {
            if (key === 'fontSize' && component.style[key] === '') return

            component.style[key] = format(component.style[key], store.state.drawBoard.canvasStyleData.scale)
        }
    })
}

function format(value, scale) {
    return multiply(value, divide(parseFloat(scale), 100))
}

function getOriginStyle(value, scale) {
    return divide(value, divide(parseFloat(scale), 100))
}
