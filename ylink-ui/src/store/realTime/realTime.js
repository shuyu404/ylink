import mqttMini from "@/static/js/mqtt.mini";
import store from "..";
import user from "../modules/user";
import { ByteToString } from "../../utils/ylink"
import { Notification } from "element-ui";
import { timeUtils } from "@/utils/timeUtils";
const state = {
    mqtt: null,
    websocket: null,
    mqttMsgBus: {},
    webMsgBus: {},
    options: {
        username: "ss",
        password: "tt",
        keepalive: 60,
        reconnectPeriod: 10000
    }
}

const mutations = {
    connect(state) {
        var client = {}
        if (state.mqtt == null) {
            state.options.clientId = user.state.info.userId
            client = mqttMini.connect(process.env.VUE_APP_MQTT_URL, state.options)
            client.on("connect", e => { store._mutations['realTime/onConnect'][0](client); })
            client.on("disconnect", e => { store._mutations['realTime/disConnect'][0](); })
            client.on("message", (topic, paloy, data) => { store._mutations['realTime/onMessage'][0](data); })
        }
        //连接websocket
        if (state.websocket == null) {
            store._mutations['realTime/ws_connect'][0]();
        }
    },
    onConnect(state, client) {
        console.log("连接成功");
        state.mqtt = client
    },
    disConnect(state) {
        console.log("连接断开");
        state.mqtt = null
    },
    onMessage(state, data) {
        var message = ByteToString(data.payload)
        console.log(message);
        state.mqttMsgBus = JSON.parse(message)
    },
    subscribe(state, sub) {
        if (state.mqtt != null) {
            console.log("添加订阅");
            state.mqtt.subscribe(sub, (error, grantend) => {

            })
        }
    },
    unsubscribe(state, unsub) {
        console.log("取消订阅");
        if (state.mqtt != null) {
            state.mqtt.unsubscribe(unsub, { 'qos': 0 }, res => { })
        }
    },
    ws_connect(state) {

        state.websocket = new WebSocket(process.env.VUE_APP_WEBSOCKET_URL)
        state.websocket.onopen = e => { store._mutations['realTime/ws_onOpen'][0](e); }
        state.websocket.onmessage = e => { store._mutations['realTime/ws_onMessage'][0](e); }
        state.websocket.onclose = e => { store._mutations['realTime/ws_onClose'][0](e); }
        state.websocket.onerror = e => { console.log(e); }
    },
    ws_onOpen(state, evt) {
        console.log("ws连接成功");
        var userInfo = store.state.user.info
        state.websocket.send(JSON.stringify({ type: "info", rootId: userInfo.rootId }))
    },
    ws_onMessage(state, evt) {
        var data = JSON.parse(evt.data)
        console.log(data);
        handlerMessage(data)
        console.log("ws收到消息");
    },
    ws_onClose(state, evt) {
        console.log("断开连接");
    }
}

const actions = {

}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}

function handlerMessage(data) {
    if (data.type == "message") {
        if (data.status == "warn") {
            Notification({
                title: "阈值报警",
                type: 'warning',
                dangerouslyUseHTMLString: true,
                message:"<b>报警设备：</b>"+data.data.deviceName+" <br/>"+
                        "<b>检测数据：</b>"+data.data.dataName+"<br/>"+
                        "<b>当前值：</b>" + data.data.value+"<br/>"+
                        "<b>触发器：</b>" + data.data.ruleTrigger.name + "<br/>" +
                        "<b>报警消息：</b>" + data.data.ruleTrigger.warnMsg + "<br/>"+
                        "<b>触发时间：</b>"+timeUtils.parseTime(data.time)+"<br/>",
                duration: 10000
            })
        } else if(data.status == "common") {
            Notification({
                title: "恢复提醒",
                type: 'success',
                dangerouslyUseHTMLString: true,
                message:"<b>报警设备：</b>"+data.data.deviceName+" <br/>"+
                        "<b>检测数据：</b>"+data.data.dataName+"<br/>"+
                        "<b>当前值：</b>" + data.data.value+"<br/>"+
                        "<b>触发器：</b>" + data.data.ruleTrigger.name + "<br/>" +
                        "<b>报警消息：</b>" + data.data.ruleTrigger.commonMsg + "<br/>"+
                        "<b>触发时间：</b>"+timeUtils.parseTime(data.time)+"<br/>",
                duration: 10000
            })
        }
    }
}
