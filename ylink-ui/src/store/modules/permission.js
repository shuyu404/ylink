import { getRouters } from '@/api/menu'
import { map } from 'core-js/internals/array-iteration'
import router, { constantRoutes,appendRoutes } from '@/router'
import Layout from '@/layout/index'
const permission = {
  state: {
    routes: [],
    addRoutes: [],
    defaultRoutes: [],
    topbarRouters: [],
    sidebarRouters: []
  },
  mutations: {
    SET_ROUTES: (state, routes) => {
      state.addRoutes = routes
      state.routes = constantRoutes.concat(routes)
    },
    SET_DEFAULT_ROUTES: (state, routes) => {
      state.defaultRoutes = constantRoutes.concat(routes)
    },
    SET_TOPBAR_ROUTES: (state, routes) => {
      state.topbarRouters = routes
    },
    SET_SIDEBAR_ROUTERS: (state, routes) => {
      state.sidebarRouters = routes
    }
  },
  actions: {
    // 生成路由
    GenerateRoutes({ commit }) {
      return new Promise(resolve => {
        // 向后端请求路由数据
        getRouters().then(res => {
          const rdata = JSON.parse(JSON.stringify(res.data))

          const rewriteRoutes = filterAsyncRouter(rdata)
          // const rewriteRoutes = filterAsyncRouter(rdata, false, true)
          // const asyncRoutes = filterDynamicRoutes(dynamicRoutes);
          router.addRoutes(rewriteRoutes);
          //追加路由
          router.addRoutes(appendRoutes)
          // commit('SET_ROUTES', rewriteRoutes)
           commit('SET_SIDEBAR_ROUTERS', constantRoutes.concat(rewriteRoutes))
          // commit('SET_DEFAULT_ROUTES', sidebarRoutes)
          // commit('SET_TOPBAR_ROUTES', sidebarRoutes)
          // resolve(rewriteRoutes)
        })
      })
    }
  }
}

//遍历后台传来的我路由字符串，转换为组件对象
function filterAsyncRouter(asyncRouterMap, lastRouter = false, type = false) {
  const routerMap = []
  const routerList=[]

  //对后端传来的数据进行加工
  asyncRouterMap.forEach(router => {
    if(router.component==='Layout'){
      router.component=Layout
    }else {
      router.component=loadView(router.component)
    }

    router.meta={title:router.menuName,icon:router.icon}
    router.name=router.menuName

    if(router.hide==null){
      delete router.hide
    }

    routerMap[router.menuId] = router
  })

  asyncRouterMap.forEach(route=>{
    if (route.parentId === "0") {
      route.children= filterChildren(route, routerMap)
      routerList.push(route)
    }
  })

  return routerList
}


function filterChildren(route, routerMap) {
  const children = []
  routerMap.forEach(e => {
    if (route.menuId === e.parentId) {
      children.push(e)
      e.children=filterChildren(e, routerMap)
      if(e.children.length===0){
        delete e.children
      }
    }
  })
  return children
}

export const loadView = (view) => {
  if (process.env.NODE_ENV === 'development') {

    return (resolve) => require([`@/views${view}`], resolve)

   } else {
  // 使用 import 实现生产环境的路由懒加载
     return () => import('@/views'+view)
   }
}

export default permission
