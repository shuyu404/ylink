import store from "@/store"
import Vue from "vue"
import toast, { $, deepCopy, swap } from "@/views/drawBoard/utils/utils"

const state = {
    // 右击菜单数据
    contextMenuStatus: {
        menuTop: 0,
        menuLeft: 0,
        menuShow: false,
    },
    //当前组件数据
    curComponent: null,
    // 选中区域包含的组件以及区域位移信息
    areaData: {
        style: {
            top: 0,
            left: 0,
            width: 0,
            height: 0,
        },
        components: [],
    },
    //当前画布实体
    editor: null,
    // 画布组件数据
    componentData: [],
    // 点击画布时是否点中组件，主要用于取消选中组件用。
    // 如果没点中组件，并且在画布空白处弹起鼠标，则取消当前组件的选中状态
    isClickComponent: false,
    isInEdiotr: false, // 是否在编辑器中，用于判断复制、粘贴组件时是否生效，如果在编辑器外，则无视这些操作
    curComponentIndex: null,
    //左侧数据列表
    componentLib: [],
    //快照信息
    snapshotData: [],
    editMode: 'edit',
    canvasStyleData: { // 页面全局数据
        width: 1200,
        height: 740,
        scale: 100,
        color: '#000',
        opacity: 1,
        background: '#fff',
        fontSize: 14,
    },
    snapshotIndex: -1,
    //编辑动作属性
    //复制
    copyData: null,
    isCut: false
}


const mutations = {
    showContextMenu(state, { top, left }) {
        state.contextMenuStatus.menuShow = true
        state.contextMenuStatus.menuTop = top
        state.contextMenuStatus.menuLeft = left
    },
    hideContextMenu(state) {
        state.contextMenuStatus.menuShow = false
    },
    setEditor(state, data) {
        state.editor = data
    },
    setAreaData(state, data) {
        state.areaData = data
    },
    setComponentData(state, componentData = []) {
        Vue.set(state, 'componentData', componentData)
    },
    setCurComponent(state, { component, index }) {
        state.curComponent = component
        state.curComponentIndex = index
    },
    setEditMode(state, mode) {
        state.editMode = mode
    },
    addComponent(state, { component, index }) {
        if (index !== undefined) {
            state.componentData.splice(index, 0, component)
        } else {
            state.componentData.push(component)
        }
    },
    setCanvasStyle(state, style) {
        state.canvasStyleData = style
    },
    setComponentLib(state, data) {
        state.componentLib = data
    },
    setInEditorStatus(state, status) {
        state.isInEdiotr = status
    },
    setClickComponentStatus(state, status) {
        state.isClickComponent = status
    },
    recordSnapshot(state) {
        // 添加新的快照
        state.snapshotData[++state.snapshotIndex] = deepCopy(state.componentData)
        // 在 undo 过程中，添加新的快照时，要将它后面的快照清理掉
        if (state.snapshotIndex < state.snapshotData.length - 1) {
            state.snapshotData = state.snapshotData.slice(0, state.snapshotIndex + 1)
        }
    },
    undo(state) {
        if (state.snapshotIndex >= 0) {
            state.snapshotIndex--
            const componentData = deepCopy(state.snapshotData[state.snapshotIndex]) || []
            if (state.curComponent) {
                // 如果当前组件不在 componentData 中，则置空
                const needClean = !componentData.find(component => state.curComponent.id === component.id)

                if (needClean) {
                    store._mutations['drawBoard/setCurComponent'][0]({
                        component: null,
                        index: null,
                    })
                    // store.commit('setCurComponent', {
                    //     component: null,
                    //     index: null,
                    // })
                }
            }

           // store.commit('setComponentData', componentData)
            store._mutations['drawBoard/setComponentData'][0](componentData)
        }
    },

    redo(state) {
        if (state.snapshotIndex < state.snapshotData.length - 1) {
            state.snapshotIndex++
            store._mutations['drawBoard/setComponentData'][0](deepCopy(state.snapshotData[state.snapshotIndex]))
           // store.commit('setComponentData', deepCopy(state.snapshotData[state.snapshotIndex]))
        }
    },
    setShapeStyle({ curComponent }, { top, left, width, height, rotate }) {
        if (top) curComponent.style.top = Math.round(top)
        if (left) curComponent.style.left = Math.round(left)
        if (width) curComponent.style.width = Math.round(width)
        if (height) curComponent.style.height = Math.round(height)
        if (rotate) curComponent.style.rotate = Math.round(rotate)
    },
    //删除组件
    deleteComponent(state, index) {

        if (index === undefined) {
            index = state.curComponentIndex
        }

        if (index === state.curComponentIndex) {
            state.curComponentIndex = null
            state.curComponent = null
        }

        if (/\d/.test(index)) {
            state.componentData.splice(index, 1)
        }
    },
    //编辑动作
    copy(state) {
        if (!state.curComponent) {
            toast("请选择组件")
        }
        // 如果有剪切的数据，需要先还原
        restorePreCutData(state)
        copyData(state)

        state.isCut = false
        console.log(state.copyData)
    },
    paste(state, isMouse) {
        console.log(state.copyData)
        if (!state.copyData) {
            toast('请选择组件')
            return
        }
        
        const data = state.copyData.data
        if (isMouse) {
            data.style.top = state.contextMenuStatus.menuTop
            data.style.left = state.contextMenuStatus.menuLeft
        } else {
            data.style.top += 10
            data.style.left += 10
        }

        data.id = new Date().getTime()
        console.log();
        store._mutations['drawBoard/addComponent'][0]({ component: deepCopy(data) })
       // store.commit('addComponent', { component: deepCopy(data) })
        if (state.isCut) {
            state.copyData = null
        }
    },
    cut(state) {
        if (!state.curComponent) {
            toast('请选择组件')
            return
        }

        // 如果重复剪切，需要恢复上一次剪切的数据
        restorePreCutData(state)
        copyData(state)
        store._mutations['drawBoard/deleteComponent'][0]()
       // store.commit('deleteComponent')
        state.isCut = true
    },
    //布局调整操作
    upComponent(state) {
        const { componentData, curComponentIndex } = state
        // 上移图层 index，表示元素在数组中越往后
        if (curComponentIndex < componentData.length - 1) {
            swap(componentData, curComponentIndex, curComponentIndex + 1)
            state.curComponentIndex = curComponentIndex + 1
        } else {
            toast('已经到顶了')
        }
    },

    downComponent(state) {
        const { componentData, curComponentIndex } = state
        // 下移图层 index，表示元素在数组中越往前
        if (curComponentIndex > 0) {
            swap(componentData, curComponentIndex, curComponentIndex - 1)
            state.curComponentIndex = curComponentIndex - 1
        } else {
            toast('已经到底了')
        }
    },

    topComponent(state) {
        const { componentData, curComponentIndex, curComponent } = state
        // 置顶
        if (curComponentIndex < componentData.length - 1) {
            componentData.splice(curComponentIndex, 1)
            componentData.push(curComponent)
            state.curComponentIndex = componentData.length - 1
        } else {
            toast('已经到顶了')
        }
    },

    bottomComponent(state) {
        const { componentData, curComponentIndex, curComponent } = state
        // 置底
        if (curComponentIndex > 0) {
            componentData.splice(curComponentIndex, 1)
            componentData.unshift(curComponent)
            state.curComponentIndex = 0
        } else {
            toast('已经到底了')
        }
    },
    lock({ curComponent }) {
        curComponent.isLock = true
    },
    unlock({ curComponent }) {
        curComponent.isLock = false
    }
}

const getters = {
    //根据id获取组件信息
    getComponentFromLib: state => {
        return cid => {
            for (const component of state.componentLib) {
                if (component.cid === cid) {
                    return component
                }
            }
        }
    }
}

const actions = {

}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}

// 恢复上一次剪切的数据
function restorePreCutData(state) {
    if (state.isCut && state.copyData) {
        const data = deepCopy(state.copyData.data)
        const index = state.copyData.index
        store._mutations['drawBoard/addComponent'][0]( { component: data, index })
        if (state.curComponentIndex >= index) {
            // 如果当前组件索引大于等于插入索引，需要加一，因为当前组件往后移了一位
            state.curComponentIndex++
        }
    }
}

function copyData(state) {
    state.copyData = {
        data: deepCopy(state.curComponent),
        index: state.curComponentIndex,
    }
}