
export const timeUtils = {}

timeUtils.parseTime = (data) => {
    var date = new Date(data)
    return date.toLocaleDateString() + " " + date.toLocaleTimeString()
}