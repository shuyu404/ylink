package com.ylink.common.core.utils.captcha;

import org.springframework.context.annotation.Configuration;

/**
 * 验证码配置
 * 
 * @author ruoyi
 */
@Configuration
//@RefreshScope

public class CaptchaProperties
{
    /**
     * 验证码开关
     */
    private Boolean enabled=true;

    /**
     * 验证码类型（math 数组计算 char 字符）
     */
    private String type="char";

    public Boolean getEnabled()
    {
        return enabled;
    }

    public void setEnabled(Boolean enabled)
    {
        this.enabled = enabled;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }
}
