package com.ylink.common.core.exception.auth;


import com.ylink.common.core.exception.CaptchaException;
import com.ylink.common.core.utils.StringUtils;

/**
 * @BelongsProject: springSecurity
 * @BelongsPackage: com.example.springSecurity.exception
 * @Author: shuyu
 * @CreateTime: 2022-10-31  11:08
 * @Description: 权限校验异常类
 * @Version: 1.0
 */

public class NotPermissionException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    /**
     * 错误码
     */
    private Integer code;

    /**
     * 错误提示
     */
    private String message;
    /**
     * 错误明细，内部调试错误
     * 和 {CommonResult#getDetailMessage()} 一致的设计
     */
    private String detailMessage;

    /**
     * 空构造方法，避免反序列化问题
     */
    public NotPermissionException() {
    }

    public NotPermissionException(String message) {
        this.message = message;
    }

    public NotPermissionException(String message, Integer code) {
        this.message = message;
        this.code = code;
    }

    public String getDetailMessage() {
        return detailMessage;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Integer getCode() {
        return code;
    }

    public NotPermissionException setMessage(String message) {
        this.message = message;
        return this;
    }

    public NotPermissionException setDetailMessage(String detailMessage) {
        this.detailMessage = detailMessage;
        return this;
    }
}
