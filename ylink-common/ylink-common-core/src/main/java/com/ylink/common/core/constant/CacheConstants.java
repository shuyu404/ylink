package com.ylink.common.core.constant;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.core.constant
 * @Author: shuyu
 * @CreateTime: 2022-11-01  11:01
 * @Description:
 * @Version: 1.0
 */

public class CacheConstants {

    /**
     * 允许重试次数
     */
    public static final int PASSWORD_MAX_RETRY_COUNT = 5;

    /**
     * 默认锁定十分钟
     */
    public static final Long PASSWORD_LOCK_TIME = 10L;


    /**
     * 登录有效时间，默认30min
     */
    public static final int EXPIRATION = 60;


    /**
     * 登录错误次数缓存键
     */
    public static final String PWD_ERR_CNT_KEY = "pwd_err_cnt:";

    /**
     * token缓存键
     */
    public static final String LOGIN_TOKEN_KEY="login_token_key:";

    /**
     * 验证码缓存
     */
    public static final String CAPTCHA_CODE_KEY = "captcha_code_key";
}
