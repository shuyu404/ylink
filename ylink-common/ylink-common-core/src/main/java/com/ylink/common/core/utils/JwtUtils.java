package com.ylink.common.core.utils;

import com.ylink.common.core.constant.SecurityConstants;
import com.ylink.common.core.constant.TokenConstants;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.core.utils
 * @Author: shuyu
 * @CreateTime: 2022-11-01  12:01
 * @Description:
 * @Version: 1.0
 */
@Slf4j
public class JwtUtils {

    public static String secret = TokenConstants.SECRET;

    /**
     * @param claims 声明载荷
     * @return token
     */
    public static String createToken(Map<String, Object> claims) {
        return Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS256, secret).compact();
    }

    /**
     * 从声明中获取载荷
     *
     * @param token token
     * @return 载荷
     */
    public static Claims parseToken(String token) {
        try {
            return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
        } catch (Exception e) {
            log.error("token解析错误");
            return null;
        }
    }

    public static String getUserKey(Claims claims) {
        return getValue(claims, SecurityConstants.USER_KEY);
    }

    public static String getUserId(Claims claims) {
        return getValue(claims, SecurityConstants.DETAILS_USER_ID);
    }

    public static String getUserName(Claims claims) {
        return getValue(claims, SecurityConstants.DETAILS_USERNAME);
    }


    public static String getUserId(String token) {
        Claims claims = parseToken(token);
        return getUserId(claims);
    }

    public static String getUserKey(String token) {
        Claims claims = parseToken(token);
        return getUserKey(claims);
    }

    public static String getValue(Claims claims, String key) {
        return (String) claims.get(key);
    }

}
