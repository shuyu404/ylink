package com.ylink.common.core.constant;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.core.constant
 * @Author: shuyu
 * @CreateTime: 2022-11-02  20:23
 * @Description:
 * @Version: 1.0
 */

public class CaptchaConstants {
    /**
     * 验证码id
     */
    public static final String UUID="uuid";

    /**
     * 验证码内容
     */
    public static final String CODE="code";

    /**
     * math类型验证码
     */
    public static final String MATH="math";
    /**
     * char类型验证码
     */
    public static final String CHAR="char";
}
