package com.ylink.common.core.constant.kafka;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.core.constant.kafka
 * @Author: shuyu
 * @CreateTime: 2022-12-21  19:25
 * @Description:
 * @Version: 1.0
 */

public class KafkaConstants {
    /**
     * 规则消息
     */
    public final static String TO_RULE_MESSAGE="to_rule_message";

    /**
     * 上下线消息
     */
    public final static String TO_RULE_STATUS="to_rule_status";
}
