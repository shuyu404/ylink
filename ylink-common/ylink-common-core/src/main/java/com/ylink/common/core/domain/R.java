package com.ylink.common.core.domain;

import java.util.HashMap;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.core.domain
 * @Author: shuyu
 * @CreateTime: 2022-10-31  21:28
 * @Description:    R包装类
 * @Version: 1.0
 */

public class R extends HashMap<String,Object> {

    public R(){
        put("code",0);
    }

    public static R ok(String msg){
        R ok = init();
        ok.put("msg",msg);
        return ok;
    }

    public static R ok(Object obj){
        R ok = init();
        ok.put("data",obj);
        return ok;
    }

    public static R error(String msg){
        R error = error();
        error.put("msg",msg);
        return error;
    }
    public static R error(int code,String msg){
        R error = error(msg);
        error.put("code",code);
        return error;
    }

    public static R ok(){
        return init();
    }

    public static R ok(int code,String msg){
        R init = init();
        init.put("code",code);
        init.put("msg",msg);
        return init;
    }

    public static R error(){
        R init = init();
        init.put("code",500);
        return init;
    }

    public static R init(){
        return new R();
    }
    @Override
    public  R put(String key,Object value){
        super.put(key,value);
        return this;
    }

}
