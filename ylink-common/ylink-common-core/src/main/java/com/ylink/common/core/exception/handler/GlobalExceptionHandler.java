package com.ylink.common.core.exception.handler;

import com.ylink.common.core.domain.R;
import com.ylink.common.core.exception.CaptchaException;
import com.ylink.common.core.exception.ServiceException;
import com.ylink.common.core.exception.auth.NotPermissionException;
import com.ylink.common.core.exception.security.TokenException;
import com.ylink.common.core.utils.ServletUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletResponse;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.core.exception.handler
 * @Author: shuyu
 * @CreateTime: 2022-11-01  22:02
 * @Description:
 * @Version: 1.0
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ServiceException.class)
    public R handlerServiceException(ServiceException e){
        return R.error(e.getMessage());
    }

    @ExceptionHandler(CaptchaException.class)
    public void handlerCaptchaException(CaptchaException e, HttpServletResponse response){
        ServletUtils.response(HttpStatus.OK,e.getMessage(),response);
    }

    @ExceptionHandler(TokenException.class)
    public void handlerTokenException(TokenException e, HttpServletResponse response){
        ServletUtils.response(HttpStatus.OK,e.getMessage(),response);
    }


    @ExceptionHandler(NotPermissionException.class)
    public R handlerNotPermissionException(NotPermissionException e){
        return R.error(403,e.getMessage());
    }
}
