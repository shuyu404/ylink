package com.ylink.common.core.constant;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.core.constant
 * @Author: shuyu
 * @CreateTime: 2022-10-31  21:16
 * @Description:
 * @Version: 1.0
 */

public class Constants {

    public static final int SUCCESS=200;

    public static final int FAIL=500;

    public static final String LOGIN_FAIL="error";


    public static final String UTF8 = "UTF-8";

    /**
     * 验证码过期时间
     */
    public static final Long CAPTCHA_EXPIRATION = 2L;
}
