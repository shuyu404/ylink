package com.ylink.common.core.enums;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.core.enums
 * @Author: shuyu
 * @CreateTime: 2022-11-09  11:42
 * @Description:    数据类型枚举
 * @Version: 1.0
 */

public enum DataType {
    /**/
    BOOLEAN(0),SHORT(1),INT32(2),INT64(3),FLOAT(4),DOUBLE(5);

    private final int type;

    DataType(int type){
        this.type=type;
    }

    public static DataType getType(int i){
        return getDataType(i);
    }

    private static DataType getDataType(int type){
       switch (type){
           case 0:
               return BOOLEAN;
           case 1:
               return SHORT;
           case 2:
               return INT32;
           case 3:
               return INT64;
           case 4:
               return FLOAT;
           case 5:
               return DOUBLE;
           default:
               throw new IllegalArgumentException("data类型错误");
       }
    }

    public int getDataTypeSize() {
        switch(this) {
            case BOOLEAN:
                return 1;
            case SHORT:
                return 2;
            case INT32:
            case FLOAT:
                return 4;
            case INT64:
            case DOUBLE:
                return 8;
            default:
                throw new IllegalArgumentException("不支持的数据类型"+this.toString());
        }
    }

}
