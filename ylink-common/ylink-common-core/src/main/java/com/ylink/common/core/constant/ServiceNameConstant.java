package com.ylink.common.core.constant;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.core.constant
 * @Author: shuyu
 * @CreateTime: 2022-10-31  21:48
 * @Description:
 * @Version: 1.0
 */

public class ServiceNameConstant {

    public static final String SYSTEM_SERVICE = "ylink-system";

    public static final String AUTH_SERVICE = "ylink-auth";

    public static final String DEVICE_SERVICE="ylink-device";

    public static final String BROKER_SERVICE="ylink-broker";
}
