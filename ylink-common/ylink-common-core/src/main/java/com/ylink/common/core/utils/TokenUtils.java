package com.ylink.common.core.utils;

import com.ylink.common.core.constant.SecurityConstants;
import com.ylink.common.core.constant.TokenConstants;
import io.jsonwebtoken.Claims;
import org.springframework.http.server.reactive.ServerHttpRequest;

import javax.servlet.http.HttpServletRequest;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.core.utils
 * @Author: shuyu
 * @CreateTime: 2022-11-01  14:41
 * @Description:
 * @Version: 1.0
 */

public class TokenUtils {

    /**
     * 从http头获取token
     * @param request http请求
     * @return token
     */
    public static String getToken(ServerHttpRequest request){
        return request.getHeaders().getFirst(TokenConstants.AUTHENTICATION);
    }

    public static String getToken(HttpServletRequest request){
        return request.getHeader(TokenConstants.AUTHENTICATION);
    }


    public static String getUserKey(Claims claims){
        return claims.get(SecurityConstants.USER_KEY,String.class);
    }

    public static String getUserName(Claims claims){
        return claims.get(SecurityConstants.DETAILS_USERNAME,String.class);
    }

    public static Claims parseToken(String token){
        try {
            Claims claims = JwtUtils.parseToken(token);
            return claims;
        }catch (Exception e){
            return null;
        }
    }

}
