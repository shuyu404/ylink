package com.ylink.common.core.exception.auth;

/**
 * @BelongsProject: springSecurity
 * @BelongsPackage: com.example.springSecurity.exception
 * @Author: shuyu
 * @CreateTime: 2022-10-31  10:34
 * @Description:    未登录异常
 * @Version: 1.0
 */

public class NotLoginException extends RuntimeException{
    private static final long serialVersionUID = 1L;
    public NotLoginException(){
        super("未登录异常");
    }
    public NotLoginException(String msg){
        super(msg);
    }
}
