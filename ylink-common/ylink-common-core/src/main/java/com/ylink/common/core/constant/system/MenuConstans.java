package com.ylink.common.core.constant.system;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.core.constant
 * @Author: shuyu
 * @CreateTime: 2022-11-05  15:25
 * @Description:
 * @Version: 1.0
 */

public class MenuConstans {

    public static final String VIEW="view";

    public static final String PERMISSION="permission";
}
