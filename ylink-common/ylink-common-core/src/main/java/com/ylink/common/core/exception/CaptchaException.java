package com.ylink.common.core.exception;

/**
 * 验证码错误异常类
 *
 * @author ruoyi
 */
public class CaptchaException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    /**
     * 错误码
     */
    private Integer code;

    /**
     * 错误提示
     */
    private String message;
    /**
     * 错误明细，内部调试错误
     * 和 {CommonResult#getDetailMessage()} 一致的设计
     */
    private String detailMessage;

    /**
     * 空构造方法，避免反序列化问题
     */
    public CaptchaException() {
    }

    public CaptchaException(String message) {
        this.message = message;
    }

    public CaptchaException(String message, Integer code) {
        this.message = message;
        this.code = code;
    }

    public String getDetailMessage() {
        return detailMessage;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Integer getCode() {
        return code;
    }

    public CaptchaException setMessage(String message) {
        this.message = message;
        return this;
    }

    public CaptchaException setDetailMessage(String detailMessage) {
        this.detailMessage = detailMessage;
        return this;
    }
}
