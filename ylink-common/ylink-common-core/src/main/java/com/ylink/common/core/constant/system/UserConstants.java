package com.ylink.common.core.constant.system;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.core.constant
 * @Author: shuyu
 * @CreateTime: 2022-11-01  10:30
 * @Description:
 * @Version: 1.0
 */

public class UserConstants {

    public static final int PASSWORD_MIN_LENGTH = 5;

    public static final int PASSWORD_MAX_LENGTH = 16;

    public static final int USERNAME_MIN_LENGTH = 5;

    public static final int USERNAME_MAX_LENGTH = 16;

    public static final String OK = "0";

    public static final String DISABLE = "1";

    public static final String DELETED = "2";


    public static final String AVATAR="https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fpic4.zhimg.com%2Fv2-3c7a1d5872f950feae89c3577baff287_b.jpg&refer=http%3A%2F%2Fpic4.zhimg.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1669968215&t=8f20f9273bdc0027d263088e902b22d9";
}
