package com.ylink.common.core.exception.security;

import com.ylink.common.core.exception.ServiceException;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.core.exception.security
 * @Author: shuyu
 * @CreateTime: 2022-11-03  10:46
 * @Description:
 * @Version: 1.0
 */

public class TokenException extends RuntimeException{
    private static final long serialVersionUID = 1L;
    /**
     * 错误码
     */
    private Integer code;

    /**
     * 错误提示
     */
    private String message;
    /**
     * 错误明细，内部调试错误
     * 和 {CommonResult#getDetailMessage()} 一致的设计
     */
    private String detailMessage;

    /**
     * 空构造方法，避免反序列化问题
     */
    public TokenException() {
    }

    public TokenException(String message) {
        this.message = message;
    }

    public TokenException(String message, Integer code) {
        this.message = message;
        this.code = code;
    }

    public String getDetailMessage() {
        return detailMessage;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Integer getCode() {
        return code;
    }

    public TokenException setMessage(String message) {
        this.message = message;
        return this;
    }

    public TokenException setDetailMessage(String detailMessage) {
        this.detailMessage = detailMessage;
        return this;
    }
}
