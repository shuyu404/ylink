package com.ylink.common.core.web.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.nio.file.OpenOption;
import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.vo
 * @Author: shuyu
 * @CreateTime: 2022-11-05  11:18
 * @Description:    前端下拉菜单类型
 * @Version: 1.0
 */
@Data
@AllArgsConstructor
public class SelectOption {
    /**
     * 菜单标签
     */
    String label;

    /**
     * 菜单数值
     */
    String value;

}
