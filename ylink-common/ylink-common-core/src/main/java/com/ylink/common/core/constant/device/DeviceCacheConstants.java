package com.ylink.common.core.constant.device;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.core.constant.device
 * @Author: shuyu
 * @CreateTime: 2022-11-12  15:57
 * @Description:    设备缓存类
 * @Version: 1.0
 */

public class DeviceCacheConstants {

    public static final String DEVICE_STATUS="device:status:";
    public static final String DEVICE_WARN="device:warn:";
}
