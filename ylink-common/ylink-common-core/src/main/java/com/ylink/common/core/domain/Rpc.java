package com.ylink.common.core.domain;

import com.ylink.common.core.constant.Constants;

import java.io.Serializable;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.core.domain
 * @Author: shuyu
 * @CreateTime: 2022-10-31  21:14
 * @Description: rpc远程访问包装类
 * @Version: 1.0
 */

public class Rpc<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 成功
     */
    public static final int SUCCESS = Constants.SUCCESS;

    /**
     * 失败
     */
    public static final int FAIL = Constants.FAIL;

    private int code;

    private String msg;

    private T data;


    public static <T> Rpc<T> ok() {
        return restRusult(null, SUCCESS, null);
    }

    public static <T> Rpc<T> ok(T data) {
        return restRusult(data, SUCCESS, null);
    }

    public static <T> Rpc<T> ok(T data, String msg) {
        return restRusult(data, SUCCESS, msg);
    }
    public static <T> Rpc<T> ok( String msg) {
        return restRusult(null, SUCCESS, msg);
    }

    public static <T> Rpc<T> fail() {
        return restRusult(null, FAIL, null);
    }

    public static <T> Rpc<T> fail(T data) {
        return restRusult(data, FAIL, null);
    }
    public static <T> Rpc<T> fail(String msg) {
        return restRusult(null, FAIL, msg);
    }

    public static <T> Rpc<T> fail(T data, String msg) {
        return restRusult(data, FAIL, msg);
    }


    private static <T> Rpc<T> restRusult(T data, int code, String msg) {
        Rpc<T> rpc = new Rpc<>();
        rpc.setCode(code);
        rpc.setData(data);
        rpc.setMsg(msg);
        return rpc;
    }

    public static <T> Boolean isSuccess(Rpc<T> rpc) {
        return rpc.getCode() == SUCCESS;
    }

    public static <T> Boolean isFail(Rpc<T> rpc){
        return !isSuccess(rpc);
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public T getData() {
        return data;
    }
}
