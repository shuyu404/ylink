package com.ylink.common.core.utils;

import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.core.utils
 * @Author: shuyu
 * @CreateTime: 2022-11-01  10:22
 * @Description:
 * @Version: 1.0
 */

public class StringUtils extends org.apache.commons.lang3.StringUtils {

    private static final String NULL_STR = "";
    /** 下划线 */
    private static final char SEPARATOR = '_';

    public static boolean isNull(Object o) {
        return o == null;
    }

    public static boolean isNotNull(Object o){
        return !isNull(o);
    }

    public static boolean isEmpty(String str) {
        return isNull(str) || NULL_STR.equals(str.trim());
    }

    public static <T> boolean isEmpty(List<T> strs) {
        return isNull(strs) || strs.isEmpty();
    }

    /**
     * 只需要有一个可以匹配上
     *
     * @param str  指定字符串
     * @param strs 匹配字符串
     * @return 匹配结果
     */
    public static boolean matches(String str, List<String> strs) {
        if (isEmpty(str) || isEmpty(strs)) {
            return false;
        }
        for (String s : strs) {
            if (s.equals(str)) {
                return true;
            }
        }
        return false;
    }


    @SuppressWarnings("unchecked")
    public static <T> T cast(Object obj) {
        return (T) obj;
    }

    /**
     * 驼峰转下划线命名
     */
    public static String toUnderScoreCase(String str)
    {
        if (str == null)
        {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        // 前置字符是否大写
        boolean preCharIsUpperCase = true;
        // 当前字符是否大写
        boolean curreCharIsUpperCase = true;
        // 下一字符是否大写
        boolean nexteCharIsUpperCase = true;
        for (int i = 0; i < str.length(); i++)
        {
            char c = str.charAt(i);
            if (i > 0)
            {
                preCharIsUpperCase = Character.isUpperCase(str.charAt(i - 1));
            }
            else
            {
                preCharIsUpperCase = false;
            }

            curreCharIsUpperCase = Character.isUpperCase(c);

            if (i < (str.length() - 1))
            {
                nexteCharIsUpperCase = Character.isUpperCase(str.charAt(i + 1));
            }

            if (preCharIsUpperCase && curreCharIsUpperCase && !nexteCharIsUpperCase)
            {
                sb.append(SEPARATOR);
            }
            else if ((i != 0 && !preCharIsUpperCase) && curreCharIsUpperCase)
            {
                sb.append(SEPARATOR);
            }
            sb.append(Character.toLowerCase(c));
        }

        return sb.toString();
    }
}
