package com.ylink.common.core.constant.device;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.core.constant.device
 * @Author: shuyu
 * @CreateTime: 2022-11-08  21:48
 * @Description:
 * @Version: 1.0
 */

public class DeviceConstants {

    /**
     * 在线字段
     */
    public static final String ONLINE = "online";

    /**
     * 离线字段
     */
    public static final String OFFLINE = "offline";

    /**
     * 数据最后更新时间
     */
    public static final String LAST_UPDATE_TIME="last_update_time";
    /**
     * json协议识别字段
     */
    public static final String JSON="JSON";

    /**
     * modbus协议识别字段
     */
    public static final String MODBUS="Modbus";

    /**
     * 用户类型识别字段
     */
    public static final String USER="user";

    /**
     * 设备类型识别字段
     */
    public static final String DEVICE="device";

    /**json类型数据的载体*/
    public static final String PARAMS ="params";

    /**
     * 数据存储根
     */
    public static final String ROOT="root";

    /**
     * 设备状态
     */
    public static final String STATUS="status";

    /** 设备报警状态*/
    public static final String WARN="warn";

    /** 设备正常状态*/
    public static final String COMMON="common";
}
