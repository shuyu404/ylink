package com.ylink.common.core.constant;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.core.constant
 * @Author: shuyu
 * @CreateTime: 2022-11-01  12:02
 * @Description:
 * @Version: 1.0
 */

public class TokenConstants {
    /**
     * 令牌自定义标识
     */
    public static final String AUTHENTICATION = "Authorization";

    /**
     * 令牌密匙
     */
    public static final String SECRET="shuyu";
}
