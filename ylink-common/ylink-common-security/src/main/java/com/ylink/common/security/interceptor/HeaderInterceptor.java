package com.ylink.common.security.interceptor;

import com.ylink.common.core.constant.SecurityConstants;
import com.ylink.common.core.utils.ServletUtils;
import com.ylink.common.core.utils.StringUtils;
import com.ylink.common.security.auth.AuthUtils;
import com.ylink.common.security.config.InterceptorConfig;
import com.ylink.common.security.context.SecurityContextHolder;
import com.ylink.common.security.utils.SecurityUtils;
import com.ylink.system.api.model.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.AsyncHandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.security.interceptor
 * @Author: shuyu
 * @CreateTime: 2022-11-02  22:05
 * @Description: 头部拦截器 权限信息存入securityContext
 * @Version: 1.0
 */
@Slf4j
public class HeaderInterceptor implements AsyncHandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if (!(handler instanceof HandlerMethod)) {
            return true;
        }


        SecurityContextHolder.setUserId(ServletUtils.getHeader(request, SecurityConstants.DETAILS_USER_ID));
        SecurityContextHolder.setUserKey(ServletUtils.getHeader(request, SecurityConstants.USER_KEY));
        SecurityContextHolder.setUserName(ServletUtils.getHeader(request, SecurityConstants.DETAILS_USERNAME));
        String token = SecurityUtils.getToken();

        if (StringUtils.isEmpty(token)&&InterceptorConfig.enable) {
            log.error("令牌为空");
            return false;
        }

        LoginUser loginUser = AuthUtils.getLoginUser(token);

        if (StringUtils.isNull(loginUser)&&InterceptorConfig.enable) {
            log.error("用户为空");
            return false;
        }

        SecurityContextHolder.set(SecurityConstants.LOGIN_USER, loginUser);

        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        SecurityContextHolder.remove();
    }
}
