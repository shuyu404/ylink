package com.ylink.common.security.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.security.config
 * @Author: shuyu
 * @CreateTime: 2022-11-03  17:07
 * @Description:
 * @Version: 1.0
 */
@Configuration
public class InterceptorConfig {

    public static boolean enable;

    @Value("${interceptor.enable:true}")
    public void setEnable(boolean b){
            InterceptorConfig.enable=b;
    }

}
