package com.ylink.common.security.service;

import com.ylink.common.core.constant.CacheConstants;
import com.ylink.common.core.exception.CaptchaException;
import com.ylink.common.core.utils.StringUtils;
import com.ylink.common.redis.service.RedisService;
import com.ylink.common.security.utils.SpringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.security.service
 * @Author: shuyu
 * @CreateTime: 2022-11-02  20:30
 * @Description:
 * @Version: 1.0
 */
@Component
public class CaptchaValiDataService {
    @Autowired
    RedisService redisService;

    public void checkCaptcha(String uuid, String code) {
        if (StringUtils.isEmpty(code)) {
            throw new CaptchaException("验证码不能为空");
        }
        if (StringUtils.isEmpty(uuid)) {
            throw new CaptchaException("验证码过期");
        }
        String verifyKey = CacheConstants.CAPTCHA_CODE_KEY + uuid;
        String captcha = redisService.getCacheObject(verifyKey);
        redisService.deleteObject(verifyKey);
        if (!code.equals(captcha)) {
            throw new CaptchaException("验证码错误");
        }
    }
}
