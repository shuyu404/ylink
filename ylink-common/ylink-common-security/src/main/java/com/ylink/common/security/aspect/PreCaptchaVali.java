package com.ylink.common.security.aspect;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.ylink.common.core.constant.CaptchaConstants;
import com.ylink.common.core.exception.CaptchaException;
import com.ylink.common.core.exception.ServiceException;
import com.ylink.common.core.utils.StringUtils;
import com.ylink.common.security.service.CaptchaValiDataService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.security.aspect
 * @Author: shuyu
 * @CreateTime: 2022-11-02  19:18
 * @Description: 验证码注解操作
 * @Version: 1.0
 */
@Aspect
@Component
public class PreCaptchaVali {

    @Autowired
    CaptchaValiDataService  captchaValiDataService;

    public PreCaptchaVali(){

    }

    /**
     * 自定义注解签名
     */
    public static final String POINTCUT_SIGN="@annotation(com.ylink.common.security.annoation.CaptchaVali)";


    /**
     * 声明签名
     */
    @Pointcut(POINTCUT_SIGN)
    public void poincut(){}

    @Around("poincut()")
    public Object around(ProceedingJoinPoint joinPoint)throws Throwable{

        if (joinPoint.getArgs().length>0) {
            String string = JSON.toJSONString(joinPoint.getArgs()[0]);
            JSONObject jsonObject = JSON.parseObject(string);
            String uuid = jsonObject.getString(CaptchaConstants.UUID);
            String code = jsonObject.getString(CaptchaConstants.CODE);
            if(StringUtils.isEmpty(uuid)||StringUtils.isEmpty(code)){
                throw new CaptchaException("验证码为空");
            }
            try {
                captchaValiDataService.checkCaptcha(uuid,code);
            }catch (CaptchaException e){
                throw new ServiceException(e.getMessage());
            }
        }else {
            throw new CaptchaException("注解使用错误");
        }
        return joinPoint.proceed();
    }
}
