package com.ylink.common.security.annoation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author shuyu
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD,ElementType.TYPE})
public @interface RequiresPermissions {

    /**
     *  需要校验码
     */
    String[] value() default {};

    /**
     * 校验模式 AND、OR 默认AND
     */
    Logical logical() default Logical.AND;
}
