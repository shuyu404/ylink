package com.ylink.common.security.context;

import com.alibaba.ttl.TransmittableThreadLocal;
import com.ylink.common.core.constant.SecurityConstants;
import com.ylink.common.core.utils.StringUtils;
import com.ylink.system.api.model.LoginUser;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @BelongsProject: springSecurity
 * @BelongsPackage: com.example.springSecurity.context
 * @Author: shuyu
 * @CreateTime: 2022-10-30  21:36
 * @Description:
 * @Version: 1.0
 */

public class SecurityContextHolder {
    private static final TransmittableThreadLocal<Map<String, Object>> THREAD_LOCAL = new TransmittableThreadLocal<>();

    public static void set(String key, Object value) {
        Map<String, Object> localMap = getLocalMap();
        localMap.put(key, value);
    }

    public static String get(String key) {
        return getLocalMap().get(key).toString();
    }


    public static <T> T get(String key, Class<T> clazz) {
        Map<String, Object> localMap = getLocalMap();
        return StringUtils.cast(localMap.getOrDefault(key, null));
    }


    public static Map<String, Object> getLocalMap() {
        Map<String, Object> map = THREAD_LOCAL.get();
        if (map == null) {
            map = new ConcurrentHashMap<>();
            THREAD_LOCAL.set(map);
        }
        return map;
    }

    public static void setUserId(String userId){set(SecurityConstants.DETAILS_USER_ID,userId);}
    public static void setUserName(String userName){set(SecurityConstants.DETAILS_USERNAME,userName);}
    public static void setUserKey(String userKey){set(SecurityConstants.USER_KEY,userKey);}

    public static void remove() {
        THREAD_LOCAL.remove();
    }

    public static void setPermission(String permission) {
        set(SecurityConstants.ROLE_PERMISSION,permission);
    }

    public static String getUserkey(){
        return get(SecurityConstants.USER_KEY);
    }

    public static String getUserId(){return get(SecurityConstants.DETAILS_USER_ID);}

    public static LoginUser getLoginUser() {
        return get(SecurityConstants.LOGIN_USER,LoginUser.class);
    }
}
