package com.ylink.common.security.aspect;



import com.ylink.common.security.annoation.RequiresPermissions;
import com.ylink.common.security.auth.AuthUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * @BelongsProject: springSecurity
 * @BelongsPackage: com.example.springSecurity.aspect
 * @Author: shuyu
 * @CreateTime: 2022-10-31  10:51
 * @Description:
 * @Version: 1.0
 */
@Aspect
@Component
public class PreAuthorizeAspect {
    public PreAuthorizeAspect(){

    }

    /**
     * 自定义Aop签名
     */
    public static final String POINTCUT_SIGN="@annotation(com.ylink.common.security.annoation.RequiresPermissions)";

    /**
     * 声明签名
     */
    @Pointcut(POINTCUT_SIGN)
    public void poincut(){}

    @Around("poincut()")
    public Object around(ProceedingJoinPoint joinPoint)throws Throwable{
        //注解鉴权
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();

        checkMethodAnnotation(signature.getMethod());
        try {
            //执行原有逻辑
            return joinPoint.proceed();
        }catch (Throwable e){
            throw e;
        }
    }

    /**
     *
     * 对Method对象进行注解
     */
    private void checkMethodAnnotation(Method method){
        RequiresPermissions requiresPermissions = method.getAnnotation(RequiresPermissions.class);
        if(requiresPermissions!=null){
            AuthUtils.checkPermi(requiresPermissions);
        }
    }
}
