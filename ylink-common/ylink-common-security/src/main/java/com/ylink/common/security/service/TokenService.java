package com.ylink.common.security.service;

import com.ylink.common.core.constant.CacheConstants;
import com.ylink.common.core.constant.SecurityConstants;
import com.ylink.common.core.utils.JwtUtils;
import com.ylink.common.core.utils.ServletUtils;
import com.ylink.common.core.utils.StringUtils;
import com.ylink.common.core.utils.TokenUtils;
import com.ylink.common.core.utils.ip.IpUtils;
import com.ylink.common.core.utils.uuid.IdUtils;
import com.ylink.common.redis.service.RedisService;
import com.ylink.system.api.model.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.security.service
 * @Author: shuyu
 * @CreateTime: 2022-11-01  11:29
 * @Description:
 * @Version: 1.0
 */
@Component
public class TokenService {

    @Autowired
    RedisService redisService;

    private final static long expireTime = CacheConstants.EXPIRATION;

    private final static long MILLIS_MINUTE = 60 * 1000;

    private final static String ACCESS_TOKEN = CacheConstants.LOGIN_TOKEN_KEY;

    public Map<String, Object> createTokenMap(LoginUser loginUser) {
        String token = IdUtils.fastUUID();
        String userId = loginUser.getSysUser().getUserId();
        String userName = loginUser.getSysUser().getUserName();
        loginUser.setToken(token);
        loginUser.setUserid(userId);
        loginUser.setUsername(userName);
        loginUser.setIpaddr(IpUtils.getIpAddr(ServletUtils.getRequest()));


        //进行jwt生成
        Map<String,Object> claimsMap=new HashMap<>(8);
        claimsMap.put(SecurityConstants.USER_KEY,token);
        claimsMap.put(SecurityConstants.DETAILS_USER_ID,userId);
        claimsMap.put(SecurityConstants.DETAILS_USERNAME,userName);

        //返回接口信息
        Map<String,Object> rspMap=new HashMap<>(8);
        token = JwtUtils.createToken(claimsMap);
        loginUser.setToken(token);
        refreshToken(loginUser);
        rspMap.put("access_token", token);
        rspMap.put("expires_in",expireTime);
        return rspMap;
    }

    /**
     *
     * @param loginUser 刷新userLogin token
     */
    public void refreshToken(LoginUser loginUser) {
        loginUser.setLoginTime(System.currentTimeMillis());
        loginUser.setExpireTime(loginUser.getLoginTime() + expireTime * MILLIS_MINUTE);
        String tokenKey = getTokenKey(loginUser.getToken());
        redisService.setCacheObject(tokenKey, loginUser, expireTime, TimeUnit.MINUTES);
    }

    private String getTokenKey(String token) {
        return ACCESS_TOKEN + token;
    }


    public LoginUser getLoginUser(String token) {
        LoginUser loginUser=null;
        try {
            if(StringUtils.isNotEmpty(token)){
                return redisService.getCacheObject(CacheConstants.LOGIN_TOKEN_KEY + token);
            }
        }catch (Exception e){
            throw new RuntimeException(e.getMessage());
        }
        return loginUser;
    }
}
