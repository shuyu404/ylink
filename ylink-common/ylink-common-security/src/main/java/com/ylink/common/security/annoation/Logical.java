package com.ylink.common.security.annoation;

/**
 * @BelongsProject: springSecurity
 * @BelongsPackage: com.example.springSecurity.annotation
 * @Author: shuyu
 * @CreateTime: 2022-10-31  10:56
 * @Description:
 * @Version: 1.0
 */

public enum Logical {
    /**
     * 必须具有所有元素
     */
    AND,
    /**
     * 只需具有其中一个
     */
    OR
}
