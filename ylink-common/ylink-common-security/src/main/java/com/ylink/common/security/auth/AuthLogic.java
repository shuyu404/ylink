package com.ylink.common.security.auth;


import com.ylink.common.core.exception.ServiceException;
import com.ylink.common.core.exception.auth.NotLoginException;
import com.ylink.common.core.exception.auth.NotPermissionException;
import com.ylink.common.core.utils.StringUtils;
import com.ylink.common.security.annoation.Logical;
import com.ylink.common.security.annoation.RequiresPermissions;
import com.ylink.common.security.context.SecurityContextHolder;
import com.ylink.common.security.service.TokenService;
import com.ylink.common.security.utils.SecurityUtils;
import com.ylink.common.security.utils.SpringUtils;
import com.ylink.system.api.model.LoginUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @BelongsProject: springSecurity
 * @BelongsPackage: com.example.springSecurity.auth
 * @Author: shuyu
 * @CreateTime: 2022-10-31  09:52
 * @Description:   token权限验证 逻辑实现类
 * @Version: 1.0
 */

public class AuthLogic {
    private static final Logger LOGGER= LoggerFactory.getLogger(AuthLogic.class);

    /** 所有权限标识 */
    private static final String ALL_PERMISSION = "*:*:*";

    /** 管理员角色权限标识 */
    private static final String SUPER_ADMIN = "admin";

    public TokenService tokenService= SpringUtils.getBean(TokenService.class);

    public LoginUser getLoginUser(){
        String token = SecurityUtils.getToken();
        if(token==null){
            throw new NotLoginException("未提供token");
        }
        LoginUser loginUser = tokenService.getLoginUser(token);
        if (loginUser==null){
            throw new NotLoginException("无效token");
        }
        return loginUser;
    }


    public LoginUser getLoginUser(String token){
        return tokenService.getLoginUser(token);
    }

    /**
     * 根据注解传入参数进行鉴权，如果验证未通过抛出异常 NotPermissionException
     * @param requiresPermissions 权限注解
     */
    public void checkPermi(RequiresPermissions requiresPermissions) {
        SecurityContextHolder.setPermission(StringUtils.join(requiresPermissions.value(),","));
        if(requiresPermissions.logical()== Logical.AND){
            checkPermiAnd(requiresPermissions.value());
        }else {
            checkPermiOr(requiresPermissions.value());
        }
    }



    /**
     * 验证用户是否含有指定权限，必须全部拥有
     *
     * @param permissions 权限列表
     */
    private void checkPermiAnd(String...permissions) {
        Set<String> permiList = getPermiList();
        for (String permission : permissions) {
          if(!hasPermi(permiList,permission)){
              LOGGER.error(permission);
              throw new NotPermissionException("没有访问权限");
          }
        }
    }

    /**
     * 验证用户是否含有指定权限，只需拥有其中一个
     *
     * @param permissions 权限列表
     */
    private void checkPermiOr(String...permissions) {
        Set<String> permiList = getPermiList();
        for (String permission : permissions) {
            if(hasPermi(permiList,permission)){
                return;
            }
        }
        if(permissions.length>0){
            throw new NotPermissionException(Arrays.toString(permissions));
        }
    }

    public boolean hasPermi(Set<String> permissions,String permission){
        return permissions.contains(permission);
    }

    /**
     * 获取当前权限列表
     * @return  权限列表
     */
    public Set<String> getPermiList(){
        try {
            LoginUser loginUser = getLoginUser();
            return loginUser.getPermissions();
        }catch (Exception e){
            return new HashSet<>();
        }
    }

}
