package com.ylink.common.security.auth;

import com.ylink.common.security.annoation.RequiresPermissions;
import com.ylink.system.api.model.LoginUser;

/**
 * @BelongsProject: springSecurity
 * @BelongsPackage: com.example.springSecurity.utils
 * @Author: shuyu
 * @CreateTime: 2022-10-31  09:51
 * @Description:
 * @Version: 1.0
 */

public class AuthUtils {
    public static AuthLogic authLogic=new AuthLogic();

    public static LoginUser getLoginUser(){
        return authLogic.getLoginUser();
    }

    /**
     * 通过token获取loginUser
     * @param token
     * @return
     */
    public static LoginUser getLoginUser(String token){
        return authLogic.getLoginUser(token);
    }

    /**
     * 根据注解传入参数鉴权，如果校验不通过则抛出异常NotPermissionException
     * @param requiresPermissions 权限注解
     */
    public static void checkPermi(RequiresPermissions requiresPermissions) {
        authLogic.checkPermi(requiresPermissions);
    }
}
