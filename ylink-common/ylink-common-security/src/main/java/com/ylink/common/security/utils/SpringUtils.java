package com.ylink.common.security.utils;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

/**
 * @BelongsProject: springSecurity
 * @BelongsPackage: com.example.springSecurity.utils
 * @Author: shuyu
 * @CreateTime: 2022-10-31  10:28
 * @Description:    spring工具类
 * @Version: 1.0
 */
@Component
public class SpringUtils implements BeanFactoryPostProcessor {

    /**
     *  spring 应用上下文
     */
    private static ConfigurableListableBeanFactory beanFactory;

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        SpringUtils.beanFactory = beanFactory;
    }

    /**
     * 通过springBean工厂来获取实体
     * @param clazz 类型
     * @param <T>
     * @return  返回相同类型
     */
    public static <T> T getBean(Class<T> clazz){
        return beanFactory.getBean(clazz);
    }


}
