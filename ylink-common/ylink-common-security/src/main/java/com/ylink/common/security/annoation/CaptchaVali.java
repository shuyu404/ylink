package com.ylink.common.security.annoation;


import java.lang.annotation.*;

/**
 * @author shuyu
 * 验证码注解
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface CaptchaVali {

}
