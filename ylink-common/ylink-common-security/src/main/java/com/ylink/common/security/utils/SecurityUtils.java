package com.ylink.common.security.utils;

import com.ylink.common.core.constant.TokenConstants;
import com.ylink.common.core.utils.ServletUtils;
import com.ylink.common.security.context.SecurityContextHolder;
import com.ylink.system.api.entity.SysUser;
import com.ylink.system.api.model.LoginUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.common.security.utils
 * @Author: shuyu
 * @CreateTime: 2022-11-01  11:18
 * @Description:
 * @Version: 1.0
 */
@Slf4j
public class SecurityUtils {


    public static boolean matchesPassword(String rawPassword,String encodePassword){
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        return passwordEncoder.matches(rawPassword,encodePassword);
    }

    public static String getToken(){
        try {
            return ServletUtils.getRequest().getHeader(TokenConstants.AUTHENTICATION);
        }catch (Exception e){
            log.error("SecurityUtils 获取token异常");
        }
        return null;
    }

    /**
     *
     * @return 用户id
     */
    public static String getUserId() {
        return SecurityContextHolder.getUserId();
    }

    /**
     *
     * @return 登录信息
     */
    public static LoginUser getLoginUser(){
        return SecurityContextHolder.getLoginUser();
    }

    /**
     * 获取用户
     */
    public static SysUser getSysUser(){
        return getLoginUser().getSysUser();
    }
}
