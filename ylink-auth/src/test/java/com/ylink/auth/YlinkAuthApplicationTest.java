package com.ylink.auth;

import com.alibaba.fastjson2.JSON;
import com.ylink.system.api.RemoteUserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.auth
 * @Author: shuyu
 * @CreateTime: 2022-11-01  09:53
 * @Description:
 * @Version: 1.0
 */
@SpringBootTest
public class YlinkAuthApplicationTest {

    @Autowired
    RemoteUserService remoteUserService;



    @Test
    void test(){
        System.out.println(JSON.toJSONString(remoteUserService.info("admin")) );
    }

    @Test
    void test2(){
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        System.out.println(passwordEncoder.encode("111111"));
    }
}
