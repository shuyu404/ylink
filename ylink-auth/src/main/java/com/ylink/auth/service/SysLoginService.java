package com.ylink.auth.service;

import com.ylink.common.core.constant.system.UserConstants;
import com.ylink.common.core.domain.Rpc;
import com.ylink.common.core.enums.UserStatus;
import com.ylink.common.core.exception.ServiceException;
import com.ylink.common.core.utils.StringUtils;
import com.ylink.system.api.RemoteUserService;
import com.ylink.system.api.entity.SysUser;
import com.ylink.system.api.model.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.auth.service
 * @Author: shuyu
 * @CreateTime: 2022-11-01  09:41
 * @Description:
 * @Version: 1.0
 */
@Component
public class SysLoginService {

    @Autowired
    RemoteUserService remoteUserService;

    @Autowired
    SysPasswordService passwordService;

    public LoginUser login(String username, String password){

        if (StringUtils.isAnyBlank(username,password)) {
            throw new ServiceException("账号或密码错误");
        }

        if(password.length()< UserConstants.PASSWORD_MIN_LENGTH||password.length()>UserConstants.PASSWORD_MAX_LENGTH){
            throw new ServiceException("密码不在指定范围内");
        }
        if(username.length()<UserConstants.USERNAME_MIN_LENGTH||username.length()>UserConstants.USERNAME_MAX_LENGTH){
            throw new ServiceException("用户名不在指定范围内");
        }

        //查询用户信息
        Rpc<LoginUser> loginUserRpc = remoteUserService.info(username);

        if(loginUserRpc==null){
            throw new ServiceException("用户不存在");
        }

        if (Rpc.FAIL==loginUserRpc.getCode()) {
            throw new ServiceException(loginUserRpc.getMsg());
        }
        LoginUser loginUser = loginUserRpc.getData();
        SysUser sysUser = loginUser.getSysUser();

        if(UserStatus.DELETED.getCode().equals(sysUser.getDelFlag())){
            throw new ServiceException("对不起用户已被删除");
        }
        if(UserStatus.DISABLE.getCode().equals(sysUser.getStatus())){
            throw new ServiceException("对不起用户已被停用");
        }

        passwordService.validate(sysUser,password);

        return  loginUser;
    }

}
