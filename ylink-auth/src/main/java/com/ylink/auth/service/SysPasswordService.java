package com.ylink.auth.service;

import com.ylink.common.core.constant.CacheConstants;
import com.ylink.common.core.exception.ServiceException;
import com.ylink.common.redis.service.RedisService;
import com.ylink.common.security.utils.SecurityUtils;
import com.ylink.system.api.entity.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.auth.service
 * @Author: shuyu
 * @CreateTime: 2022-11-01  10:49
 * @Description:
 * @Version: 1.0
 */
@Component
public class SysPasswordService {
    @Autowired
    private RedisService redisService;

    private int maxRetryCount = CacheConstants.PASSWORD_MAX_RETRY_COUNT;

    private Long lockTime = CacheConstants.PASSWORD_LOCK_TIME;

    private String getCacheKey(String username) {
        return CacheConstants.PWD_ERR_CNT_KEY + username;
    }

    public void validate(SysUser sysUser, String password) {
        String userName = sysUser.getUserName();

        Integer retryCount = redisService.getCacheObject(getCacheKey(userName));
        if (retryCount == null) {
            retryCount = 0;
        }

        //检验账号密码输入错误次数
        if (retryCount > maxRetryCount) {
            String errMsg = String.format("密码输入错误%s次，账户锁定%s分钟", maxRetryCount, lockTime);
            throw new ServiceException(errMsg);
        }

        //匹配账号密码
        if (!matches(sysUser, password)) {
            throw new ServiceException("用户不存在/账号密码错误");
        } else {
            clearLoginRecordCache(userName);
        }
    }

    public boolean matches(SysUser sysUser, String rawPassword) {
        return SecurityUtils.matchesPassword(rawPassword, sysUser.getPassword());
    }

    public void clearLoginRecordCache(String loginName) {
        if (redisService.hasKey(getCacheKey(loginName))) {
            redisService.deleteObject(loginName);
        }
    }


}
