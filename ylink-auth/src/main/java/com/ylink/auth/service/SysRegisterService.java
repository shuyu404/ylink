package com.ylink.auth.service;

import com.ylink.auth.dto.RegisterBody;
import com.ylink.common.core.constant.system.UserConstants;
import com.ylink.common.core.domain.Rpc;
import com.ylink.common.core.exception.ServiceException;
import com.ylink.system.api.RemoteUserService;
import com.ylink.system.api.model.LoginUser;
import com.ylink.system.api.model.RegisterUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.auth.service
 * @Author: shuyu
 * @CreateTime: 2022-11-01  22:13
 * @Description:
 * @Version: 1.0
 */
@Component
public class SysRegisterService {

    @Autowired
    RemoteUserService remoteUserService;

   public boolean  register(RegisterBody registerBody){
       String password = registerBody.getPassword();
       String username = registerBody.getUsername();

       if(username.length()< UserConstants.USERNAME_MIN_LENGTH||username.length()>UserConstants.USERNAME_MAX_LENGTH){
           throw new ServiceException("用户名不在限定范围1-16");
       }
       if(password.length()<UserConstants.PASSWORD_MIN_LENGTH||password.length()>UserConstants.PASSWORD_MAX_LENGTH){
           throw new ServiceException("密码不在限定范围1-16");
       }
       Rpc<LoginUser> info = remoteUserService.info(username);
       LoginUser data = info.getData();
       if (data!=null&&data.getSysUser()!=null){
           throw new ServiceException("用户已存在");
       }
       RegisterUser registerUser = new RegisterUser();
       registerUser.setUsername(registerBody.getUsername());
       registerUser.setPassword(registerUser.getPassword());
       Rpc<Boolean> booleanRpc = remoteUserService.registerUser(registerUser);

       return booleanRpc.getData();
   }



}
