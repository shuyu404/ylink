package com.ylink.auth.dto;

import lombok.Data;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.auth.dto
 * @Author: shuyu
 * @CreateTime: 2022-11-01  09:39
 * @Description:    登录包装
 * @Version: 1.0
 */
@Data
public class LoginBody {
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;

    /**
     * 验证码id
     */
    private String uuid;

    /**
     * 验证码内容
     */
    private String code;
}
