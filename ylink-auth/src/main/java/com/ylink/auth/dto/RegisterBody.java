package com.ylink.auth.dto;

/**
 * 用户注册对象
 *
 * @author ruoyi
 */
public class RegisterBody extends LoginBody {

    /**
     * 用户名
     */
    String username;

    /**
     * 用户密码
     */
    String password;
}
