package com.ylink.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.auth
 * @Author: shuyu
 * @CreateTime: 2022-10-31  19:34
 * @Description:
 * @Version: 1.0
 */
@EnableFeignClients(basePackages = {"com.ylink.system.api"})
@SpringBootApplication(scanBasePackages = {"com.ylink.auth","com.ylink.common"})
public class YlinkAuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(YlinkAuthApplication.class,args);
    }
}
