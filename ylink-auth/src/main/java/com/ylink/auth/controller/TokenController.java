package com.ylink.auth.controller;

import com.ylink.auth.dto.LoginBody;
import com.ylink.auth.dto.RegisterBody;
import com.ylink.auth.service.SysLoginService;
import com.ylink.auth.service.SysRegisterService;
import com.ylink.common.core.constant.CacheConstants;
import com.ylink.common.core.constant.SecurityConstants;
import com.ylink.common.core.domain.R;
import com.ylink.common.core.utils.StringUtils;
import com.ylink.common.core.utils.TokenUtils;
import com.ylink.common.redis.service.RedisService;
import com.ylink.common.security.annoation.CaptchaVali;
import com.ylink.common.security.service.TokenService;
import com.ylink.system.api.model.LoginUser;
import com.ylink.system.api.vo.ResultUser;
import com.ylink.system.api.vo.UserVo;
import io.jsonwebtoken.Claims;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.auth.controller
 * @Author: shuyu
 * @CreateTime: 2022-11-01  12:11
 * @Description:
 * @Version: 1.0
 */
@RestController
public class TokenController {
    @Autowired
    SysLoginService loginService;

    @Autowired
    SysRegisterService registerService;

    @Autowired
    TokenService tokenService;

    @Autowired
    RedisService redisService;

    @CaptchaVali
    @PostMapping("login")
    public R login(@RequestBody LoginBody loginBody) {
        LoginUser login = loginService.login(loginBody.getUsername(), loginBody.getPassword());
        Map<String, Object> tokenMap = tokenService.createTokenMap(login);
        return R.ok().put("data", tokenMap);
    }

    @PostMapping("register")
    public R register(@RequestBody RegisterBody registerBody) {
        boolean register = registerService.register(registerBody);
        if (register) {
            return R.ok("注册成功");
        } else {
            return R.error("注册失败");
        }
    }

    @GetMapping("user/info")
    public R getUserInfo(@Param("token") String token) {
        if (StringUtils.isEmpty(token)) {
            return R.error(401, "令牌为空请重新登录");
        }
        Claims claims = TokenUtils.parseToken(token);
        if (claims == null) {
            return R.error(401, "令牌失效");
        }

        LoginUser loginUser = redisService.getCacheObject(CacheConstants.LOGIN_TOKEN_KEY + token);
        ResultUser resultUser = new ResultUser();
        UserVo userVo = new UserVo();
        BeanUtils.copyProperties(loginUser,resultUser);
        BeanUtils.copyProperties(loginUser.getSysUser(),userVo);
        resultUser.setUserVo(userVo);
        return R.ok().put("data",resultUser);
    }

    @PostMapping("user/logout")
    public R logOut(HttpServletRequest request){

        String token = request.getHeader(SecurityConstants.AUTHORIZATION_HEADER);
        Claims claims = TokenUtils.parseToken(token);
        String userKey = TokenUtils.getUserKey(claims);
        redisService.deleteObject(CacheConstants.LOGIN_TOKEN_KEY+userKey);

        return R.ok();
    }

}
