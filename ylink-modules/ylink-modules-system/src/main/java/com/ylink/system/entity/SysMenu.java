package com.ylink.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.Collection;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.entity
 * @Author: shuyu
 * @CreateTime: 2022-10-31  20:09
 * @Description:
 * @Version: 1.0
 */
@Data
public class SysMenu {

    /**菜单Id*/
    String menuId;

    /**菜单名称*/
    String menuName;

    /**父级id*/
    String parentId;

    /**排序*/
    Integer orderNum;

    /**路径*/
    String path;

    /**菜单类型*/
    String menuType;

    /**菜单状态*/
    String status;

    /**所需权限*/
    String perms;

    /**图标*/
    String icon;

    /**是否隐藏*/
    String hidden;

    /**
     * 布局
     */
    String component;

    @TableField(exist = false)
    List<SysMenu> children;
}
