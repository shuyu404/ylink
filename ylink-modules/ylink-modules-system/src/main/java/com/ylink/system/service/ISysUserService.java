package com.ylink.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ylink.system.api.entity.SysUser;
import com.ylink.system.api.model.RegisterUser;

import java.util.List;

/**
 * @author shuyu
 */
public interface ISysUserService extends IService<SysUser> {

    /**
     *  获取用户信息
     * @param username  用户账号
     * @return  用户
     */
    SysUser getUser(String username);

    /**
     * 注册
     * @param registerUser  注册用户
     * @return  注册成功
     */
    boolean register(RegisterUser registerUser);

    /**
     * 获取用户列表
     * @param rootId    根id
     * @return  用户列表
     */
    List<SysUser> getUserList(String rootId);

    /**
     * 添加用户
     * @param sysUser   用户信息
     * @return  是否添加成功
     */
    boolean addUser(SysUser sysUser);

    /**
     * 根据用户id删除用户
     * @param userId
     */
    boolean deleteById(String userId,String rootId);

    /**
     * 通过id获取用户信息
     * @param userId
     * @return
     */
    SysUser getUserById(String userId);

    /**
     * 更新用户信息
     * @param sysUser
     * @return
     */
    void updateUser(SysUser sysUser);

    /**
     * 根据角色Id获取用户列表
     * @param roleId    角色ID
     * @return  用户列表
     */
    List<SysUser> getUserListByRoleId(String roleId);
}
