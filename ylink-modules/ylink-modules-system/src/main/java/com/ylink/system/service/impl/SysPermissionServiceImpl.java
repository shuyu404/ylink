package com.ylink.system.service.impl;

import com.ylink.system.api.entity.SysUser;
import com.ylink.system.entity.SysMenu;
import com.ylink.system.service.ISysMenuService;
import com.ylink.system.service.ISysPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.service.impl
 * @Author: shuyu
 * @CreateTime: 2022-10-31  22:57
 * @Description:
 * @Version: 1.0
 */
@Service
public class SysPermissionServiceImpl implements ISysPermissionService {

    @Autowired
    ISysMenuService sysMenuService;


    @Override
    public Set<String> getMenuPermission(SysUser sysUser) {
        Set<String> permissions = new HashSet<>();

        if (!StringUtils.hasText(sysUser.getRoleId())) {
            return permissions;
        }
        List<SysMenu> menuList = sysMenuService.getListByRole(sysUser.getRoleId());

        for (SysMenu sysMenu : menuList) {
            permissions.add(sysMenu.getPerms());
        }

        return permissions;
    }
}
