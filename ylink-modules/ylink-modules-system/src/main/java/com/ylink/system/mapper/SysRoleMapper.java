package com.ylink.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ylink.system.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {
}
