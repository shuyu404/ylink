package com.ylink.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ylink.common.core.web.domain.BaseEntity;
import lombok.Data;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.entity
 * @Author: shuyu
 * @CreateTime: 2022-11-03  17:24
 * @Description:    角色实体类
 * @Version: 1.0
 */
@Data
@TableName("sys_role")
public class SysRole extends BaseEntity {

    /**
     * 角色id
     */
    @TableId
    String roleId;

    /**
     * 角色名称
     */
    String roleName;

    /**
     * 角色排序
     */
    Integer roleSort;

    /**
     * 角色状态
     */
    String status;

    /**
     * 删除标识
     */
    String delFlag;


    /**
     * 根id
     */
    String rootId;

    @TableField(exist = false)
    HashMap<String,Object> params;

    @TableField(exist = false)
    List<String> menus;

    @Override
    public String toString() {
        return "SysRole{" +
                "createBy='" + createBy + '\'' +
                ", createTime=" + createTime +
                ", updateBy='" + updateBy + '\'' +
                ", updateTime=" + updateTime +
                ", remark='" + remark + '\'' +
                ", roleId='" + roleId + '\'' +
                ", roleName='" + roleName + '\'' +
                ", roleSort=" + roleSort +
                ", status='" + status + '\'' +
                ", delFlag='" + delFlag + '\'' +
                ", rootId='" + rootId + '\'' +
                ", params=" + params +
                ", menus=" + menus +
                '}';
    }
}
