package com.ylink.system.controller;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.ylink.common.core.constant.system.UserConstants;
import com.ylink.common.core.domain.R;
import com.ylink.common.core.web.controller.BaseController;
import com.ylink.common.core.web.page.TableDataInfo;
import com.ylink.common.core.web.vo.SelectOption;
import com.ylink.common.security.annoation.RequiresPermissions;
import com.ylink.common.security.utils.SecurityUtils;
import com.ylink.system.api.entity.SysUser;
import com.ylink.system.entity.SysRole;
import com.ylink.system.service.ISysRoleMenuService;
import com.ylink.system.service.ISysRoleService;
import com.ylink.system.service.ISysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.controller
 * @Author: shuyu
 * @CreateTime: 2022-11-03  17:33
 * @Description:
 * @Version: 1.0
 */
@Api(tags = "角色接口")
@RestController
@RequestMapping("role")
public class SysRoleController extends BaseController {

    @Autowired
    private ISysRoleService roleService;
    @Autowired
    private ISysRoleMenuService roleMenuService;
    @Autowired
    private ISysUserService sysUserService;

    @ApiOperation("角色列表")
    @GetMapping("/list")
    public TableDataInfo getList(SysRole role) {
        startPage();
        String rootId = SecurityUtils.getSysUser().getRootId();
        List<SysRole> list = roleService.getList(rootId);
        return getDataTable(list);
    }

    @ApiOperation("所有角色选项列表")
    @GetMapping("/listAll")
    public R getListAll(){
        String rootId = SecurityUtils.getSysUser().getRootId();
        List<SelectOption> options=new ArrayList<>();
        for (SysRole sysRole : roleService.getList(rootId)) {
            options.add(new SelectOption(sysRole.getRoleName(),sysRole.getRoleId()));
        }
        return R.ok().put("data",options);
    }


    @ApiOperation("添加角色")
    @RequiresPermissions("system:role:post")
    @PostMapping("/add")
    @Transactional
    public R addRole(@RequestBody SysRole role) {
        String idStr = IdWorker.getIdStr();
        SysUser sysUser = SecurityUtils.getSysUser();
        role.setRoleId(idStr);
        role.setRootId(sysUser.getRootId());
        role.setCreateBy(sysUser.getUserId());
        roleService.add(role);
        roleMenuService.updateList(role.getRoleId(), role.getMenus());
        return R.ok();
    }

    @ApiOperation("更新角色信息")
    @RequiresPermissions("system:role:put")
    @PutMapping("/update")
    public R update(@RequestBody SysRole role) {
        SysUser sysUser = SecurityUtils.getSysUser();
        role.setUpdateBy(sysUser.getUserId());
        role.setUpdateTime(new Date());
        roleService.updateRole(role);
        roleMenuService.updateList(role.getRoleId(), role.getMenus());
        return R.ok();
    }

    @ApiOperation("改变角色状态")
    @RequiresPermissions("system:role:put")
    @PutMapping("/changeStatus")
    public R changeStatus(@RequestBody SysRole role) {
        if (UserConstants.OK.equals(role.getStatus())||UserConstants.DISABLE.equals(role.getStatus())) {
            roleService.updateRole(role);
        }
        return R.ok().put("data", role);
    }

    @ApiOperation("获取角色详细信息")
    @GetMapping("/info/{roleId}")
    public R getInfo(@PathVariable("roleId") String roleId) {
        SysRole sysRole = roleService.getInfoBydId(roleId);
        List<String> menus = roleMenuService.getMenuIds(roleId);
        sysRole.setMenus(menus);
        return R.ok().put("data", sysRole);
    }

    @ApiOperation("删除角色")
    @RequiresPermissions("system:role:delete")
    @DeleteMapping("delete/{roleId}")
    @Transactional
    public R delete(@PathVariable("roleId") String roleId) {
        List<SysUser> userListByRoleId = sysUserService.getUserListByRoleId(roleId);
        if(userListByRoleId.size()==0){
            roleService.deleteByRoleId(roleId);
            roleMenuService.deleteList(roleId);
            return R.ok();
        }else{
            return R.error("角色存在绑定用户无法删除");
        }
    }

}
