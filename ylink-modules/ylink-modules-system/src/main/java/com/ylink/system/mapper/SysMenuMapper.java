package com.ylink.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ylink.system.entity.SysMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.mapper
 * @Author: shuyu
 * @CreateTime: 2022-10-31  22:44
 * @Description:
 * @Version: 1.0
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {
    /**
     * 从roleId中获取sysmenu列表
     * @param roleId 角色id
     * @return  菜单列表
     */
    @Select("SELECT sys_menu.* FROM sys_role_menu,sys_menu WHERE sys_menu.menu_id=sys_role_menu.menu_id AND sys_role_menu.role_id=#{roleId} ORDER BY sys_menu.order_num")
    List<SysMenu> getListByRole(String roleId);

    @Select("SELECT sys_menu.* FROM sys_role_menu,sys_menu WHERE sys_menu.menu_id=sys_role_menu.menu_id AND sys_menu.menu_type='view' AND sys_role_menu.role_id=#{roleId} ORDER BY sys_menu.order_num")
    List<SysMenu> getListView(String roleId);

    @Select("SELECT sys_menu.* FROM sys_role_menu,sys_menu WHERE sys_menu.menu_id=sys_role_menu.menu_id AND sys_menu.menu_type='permission' AND sys_role_menu.role_id=#{roleId} ORDER BY sys_menu.order_num")
    List<SysMenu> getListPermission(String roleId);
}
