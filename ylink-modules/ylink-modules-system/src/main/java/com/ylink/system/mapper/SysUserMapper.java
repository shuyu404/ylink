package com.ylink.system.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ylink.system.api.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author shuyu
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {
    void update(QueryWrapper<SysUser> user_id);
}
