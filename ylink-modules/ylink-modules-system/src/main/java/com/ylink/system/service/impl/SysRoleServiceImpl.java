package com.ylink.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ylink.system.entity.SysRole;
import com.ylink.system.mapper.SysRoleMapper;
import com.ylink.system.service.ISysRoleService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.service.impl
 * @Author: shuyu
 * @CreateTime: 2022-11-03  17:26
 * @Description:    角色
 * @Version: 1.0
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

    @Override
    public List<SysRole> getList(String rootId) {
        return baseMapper.selectList(new QueryWrapper<SysRole>().eq("root_id", rootId).orderByAsc("role_sort"));
    }

    @Override
    public void add(SysRole sysRole) {
        sysRole.setCreateTime(new Date());
        baseMapper.insert(sysRole);
    }

    @Override
    public void deleteByRoleId(String roleId) {
        baseMapper.deleteById(roleId);
    }

    @Override
    public SysRole getInfoBydId(String roleId) {
        SysRole sysRole = baseMapper.selectOne(new QueryWrapper<SysRole>().eq("role_id", roleId));
        return sysRole;
    }

    @Override
    public void updateRole(SysRole sysRole) {
        baseMapper.updateById(sysRole);
    }


}
