package com.ylink.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ylink.common.core.constant.system.UserConstants;
import com.ylink.common.core.enums.UserStatus;
import com.ylink.system.api.model.RegisterUser;
import com.ylink.system.service.ISysUserService;
import com.ylink.system.api.entity.SysUser;
import com.ylink.system.mapper.SysUserMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.Service.impl
 * @Author: shuyu
 * @CreateTime: 2022-10-31  20:36
 * @Description:    用户实现类
 * @Version: 1.0
 */

@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

    @Override
    public SysUser getUser(String username) {
        return baseMapper.selectOne(new QueryWrapper<SysUser>().eq("user_name", username));
    }

    @Override
    public boolean register(RegisterUser registerUser) {
        SysUser sysUser = new SysUser();
        String id = IdWorker.getIdStr();
        sysUser.setUserName(registerUser.getUsername());
        sysUser.setPassword(registerUser.getPassword());
        sysUser.setStatus(UserStatus.OK.getCode());
        sysUser.setRootId(id);
        sysUser.setUserId(id);
        int insert = baseMapper.insert(initUser(sysUser));

        return insert>0;
    }

    @Override
    public List<SysUser> getUserList(String rootId) {
        List<SysUser> sysUsers = baseMapper.selectList(new QueryWrapper<SysUser>().eq("root_id", rootId));
        return sysUsers.stream().filter(sysUser ->
             !sysUser.getUserId().equals(sysUser.getRootId())
        ).collect(Collectors.toList());
    }

    @Override
    public boolean addUser(SysUser sysUser) {
        sysUser.setUserId(IdWorker.getIdStr());
        int insert = baseMapper.insert(initUser(sysUser));
        return insert>0;
    }

    @Override
    public boolean deleteById(String userId,String rootId) {
        int user_id = baseMapper.delete(new QueryWrapper<SysUser>().eq("user_id", userId).eq("root_id",rootId));
        return user_id>0;
    }

    @Override
    public SysUser getUserById(String userId) {
        SysUser sysUser = baseMapper.selectOne(new QueryWrapper<SysUser>().eq("user_id", userId));
        return sysUser;
    }

    @Override
    public void updateUser(SysUser sysUser) {
        baseMapper.updateById(sysUser);
    }

    @Override
    public List<SysUser> getUserListByRoleId(String roleId) {
        return baseMapper.selectList(new QueryWrapper<SysUser>().eq("roleId", roleId));
    }


    private SysUser initUser(SysUser sysUser){
        sysUser.setCreateTime(new Date(System.currentTimeMillis()));
        sysUser.setNickName(sysUser.getUserName());
        sysUser.setAvatar(UserConstants.AVATAR);
        sysUser.setPassword( new BCryptPasswordEncoder().encode(sysUser.getPassword()));
        sysUser.setDelFlag(UserStatus.OK.getCode());
        return sysUser;
    }
}
