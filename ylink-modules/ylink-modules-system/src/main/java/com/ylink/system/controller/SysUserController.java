package com.ylink.system.controller;

import com.ylink.common.core.constant.system.UserConstants;
import com.ylink.common.core.domain.R;
import com.ylink.common.core.domain.Rpc;
import com.ylink.common.core.exception.ServiceException;
import com.ylink.common.core.utils.StringUtils;
import com.ylink.common.core.web.controller.BaseController;
import com.ylink.common.core.web.page.TableDataInfo;
import com.ylink.common.security.annoation.RequiresPermissions;
import com.ylink.common.security.utils.SecurityUtils;
import com.ylink.system.api.model.LoginUser;
import com.ylink.system.api.model.RegisterUser;
import com.ylink.system.service.ISysPermissionService;
import com.ylink.system.service.ISysUserService;
import com.ylink.system.api.entity.SysUser;
import com.ylink.system.vo.SysUserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.controller
 * @Author: shuyu
 * @CreateTime: 2022-10-31  19:37
 * @Description:
 * @Version: 1.0
 */
@Api(tags = "用户")
@RestController
@RequestMapping("/user")
public class SysUserController extends BaseController {
    @Autowired
    ISysUserService sysUserService;
    @Autowired
    ISysPermissionService sysPermissionService;

    @ApiOperation("用户登录信息")
    @GetMapping("/info/{username}")
    public Rpc<LoginUser> info(@PathVariable("username") String username) {
        SysUser sysUser = sysUserService.getUser(username);
        if (sysUser == null) {
            return Rpc.fail("用户名或密码错误");
        }
        Set<String> permissions = sysPermissionService.getMenuPermission(sysUser);
        LoginUser loginUser = new LoginUser();
        loginUser.setPermissions(permissions);
        loginUser.setSysUser(sysUser);
        return Rpc.ok(loginUser);
    }

    @ApiOperation("用户注册")
    @PostMapping("/regist")
    public Rpc<Boolean> registerUser(@RequestBody RegisterUser registerUser) {
        boolean success = sysUserService.register(registerUser);
        return Rpc.ok(success);
    }

    @ApiOperation("获取用户列表")
    @GetMapping("/list")
    public TableDataInfo getList() {
        startPage();
        String rootId = SecurityUtils.getSysUser().getRootId();
        List<SysUser> sysUsers = sysUserService.getUserList(rootId);
        return getDataTable(sysUsers);
    }

    @ApiOperation("添加用户")
    @RequiresPermissions("system:user:post")
    @PostMapping("/add")
    public R addUser(@RequestBody SysUser sysUser) {
        String rootId = SecurityUtils.getSysUser().getRootId();
        String userName = sysUser.getUserName();
        String password = sysUser.getPassword();
        if (StringUtils.isEmpty(userName) || StringUtils.isEmpty(password)) {
            throw new ServiceException("账号/密码输入不能为空");
        }
        if (userName.length() < UserConstants.USERNAME_MIN_LENGTH || userName.length() > UserConstants.USERNAME_MAX_LENGTH) {
            throw new ServiceException("账号长度不在范围内");
        }
        if (password.length() < UserConstants.PASSWORD_MIN_LENGTH || password.length() > UserConstants.PASSWORD_MAX_LENGTH) {
            throw new ServiceException("密码长度不在范围内");
        }
        SysUser user = sysUserService.getUser(userName);
        if (user != null) {
            throw new ServiceException("用户已存在");
        }
        sysUser.setRootId(rootId);
        if (sysUserService.addUser(sysUser)) {
            return R.ok(200, "添加成功");
        } else {
            return R.error("添加失败");
        }
    }

    @ApiOperation("删除用户")
    @RequiresPermissions("system:user:delete")
    @DeleteMapping("/delete/{userId}")
    public R deleteUser(@PathVariable("userId") String userId) {
        String rootId = SecurityUtils.getSysUser().getRootId();
        boolean b = sysUserService.deleteById(userId, rootId);
        if (b) {
            return R.ok();
        } else {
            return R.error("删除用户失败");
        }
    }

    @ApiOperation("获取用户信息")
    @GetMapping("/userInfo/{userId}")
    public R getUserInfo(@PathVariable("userId") String userId) {
        SysUser sysUser = sysUserService.getUserById(userId);
        SysUserVo sysUserVo = new SysUserVo();
        if (StringUtils.isNotNull(sysUser)) {
            BeanUtils.copyProperties(sysUser, sysUserVo);
        }
        return R.ok().put("data", sysUserVo);
    }

    @ApiOperation("更新用户信息")
    @RequiresPermissions("system:user:put")
    @PutMapping("/update")
    public R updateUser(@RequestBody SysUser sysUser) {
        sysUserService.updateUser(sysUser);
        return R.ok();
    }

    @ApiOperation("修改用户状态")
    @RequiresPermissions("system:user:put")
    @PutMapping("/changeStatus")
    public R changeStatus(@RequestBody SysUser sysUser) {
        sysUserService.updateUser(sysUser);
        return R.ok();
    }

    @ApiOperation("获取此角色用户")
    @GetMapping("/listByRole/{roleId}")
    public TableDataInfo getRoleList(@PathVariable("roleId") String roleId) {
        startPage();
        List<SysUser> list = sysUserService.getUserListByRoleId(roleId);
        return getDataTable(list);
    }


}
