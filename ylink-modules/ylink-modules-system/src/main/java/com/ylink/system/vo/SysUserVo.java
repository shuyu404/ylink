package com.ylink.system.vo;

import lombok.Data;

import java.sql.Date;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.vo
 * @Author: shuyu
 * @CreateTime: 2022-11-05  14:11
 * @Description:
 * @Version: 1.0
 */
@Data
public class SysUserVo {

    /** 用户ID */
    String userId;

    /** 根ID */
    String rootId;

    /** 用户账号 */
    String userName;


    /** 用户邮箱 */
    String email;

    /** 手机号码 */
    String phoneNumber;


    /** 帐号状态（0正常 1停用） */
    String status;


    /** 角色ID */
    String roleId;

    /**备注 */
    String remark;
}
