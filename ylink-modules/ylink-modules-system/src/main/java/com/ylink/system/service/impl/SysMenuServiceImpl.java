package com.ylink.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ylink.system.service.ISysMenuService;
import com.ylink.system.entity.SysMenu;
import com.ylink.system.mapper.SysMenuMapper;
import com.ylink.system.vo.TreeSelect;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.Service.impl
 * @Author: shuyu
 * @CreateTime: 2022-10-31  22:45
 * @Description:    菜单实现类
 * @Version: 1.0
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {


    @Override
    public List<SysMenu> getListByRole(String roleId) {
        return baseMapper.getListByRole(roleId);
    }



    @Override
    public TreeSelect buildMenuTreeSelect(List<SysMenu> menus) {
        TreeSelect root = new TreeSelect("0", "root");
        return  root.buildTree(root,menus);
    }

    @Override
    public List<SysMenu> buildMenuTree(List<SysMenu> menus) {
        List<SysMenu> returnList = new ArrayList<SysMenu>();
        List<Long> tempList = new ArrayList<Long>();
        return menus;
    }

    @Override
    public void updateMenuList(String roleId, List<String> menusId) {

    }

    @Override
    public List<SysMenu> getViewListByRole(String roleId) {
        return baseMapper.getListView(roleId);
    }

    @Override
    public List<SysMenu> getPermissions(String roleId) {
        return baseMapper.getListPermission(roleId);
    }


}
