package com.ylink.system.controller;

import com.ylink.common.core.constant.system.UserConstants;
import com.ylink.common.core.domain.R;
import com.ylink.common.core.utils.StringUtils;
import com.ylink.common.security.utils.SecurityUtils;
import com.ylink.system.entity.SysMenu;
import com.ylink.system.entity.SysRole;
import com.ylink.system.service.ISysMenuService;
import com.ylink.system.service.ISysRoleService;
import com.ylink.system.vo.TreeSelect;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.controller
 * @Author: shuyu
 * @CreateTime: 2022-11-02  21:46
 * @Description:
 * @Version: 1.0
 */
@Api(tags = "菜单")
@RestController
@RequestMapping("menu")
public class SysMenuController {
    @Autowired
    ISysMenuService menuService;
    @Autowired
    ISysRoleService roleService;

    @ApiOperation("获取权限菜单列表")
    @GetMapping("getRouters")
    public R getRouters() {
        String roleId = SecurityUtils.getLoginUser().getSysUser().getRoleId();
        SysRole role = roleService.getInfoBydId(roleId);
        List<SysMenu> listByRole = new ArrayList<>();
        if (!(StringUtils.isNull(role) || UserConstants.DISABLE.equals(role.getStatus()))) {
            listByRole = menuService.getViewListByRole(roleId);
        }
        return R.ok().put("data", listByRole);
    }

    @ApiOperation("权限树接口")
    @GetMapping("roleMenuTreeselect/{roleId}")
    public R getRoleMenuTreeselect(@PathVariable("roleId") String roleId) {
        List<SysMenu> listByRole = menuService.getListByRole(roleId);
        TreeSelect treeSelect = menuService.buildMenuTreeSelect(listByRole);
        return R.ok().put("data", treeSelect);
    }

    @ApiOperation("获取下拉菜单列表")
    @GetMapping("treeselect")
    public R getTreeselect() {
        return getRoleMenuTreeselect("1");
    }
}
