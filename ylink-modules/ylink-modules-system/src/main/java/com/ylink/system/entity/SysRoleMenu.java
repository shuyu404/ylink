package com.ylink.system.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.entity
 * @Author: shuyu
 * @CreateTime: 2022-11-04  17:20
 * @Description: 权限映射表
 * @Version: 1.0
 */
@Data
@AllArgsConstructor
@TableName("sys_role_menu")
public class SysRoleMenu {

    @TableId
    String roleId;

    String menuId;
}
