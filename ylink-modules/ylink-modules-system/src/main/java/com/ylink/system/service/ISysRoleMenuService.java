package com.ylink.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ylink.system.entity.SysRoleMenu;

import java.util.List;

public interface ISysRoleMenuService extends IService<SysRoleMenu> {

    /**
     * 更新角色权限列表
     * @param roleId    角色id
     * @param menus     权限信息
     */
    void updateList(String roleId, List<String> menus);

    /**
     * 删除角色
     * @param roleId    角色id
     */
    void deleteList(String roleId);

    /**
     * 根据角色id获取映射表列表
     * @param roleId
     * @return
     */
    List<SysRoleMenu> getRoleMenuList(String roleId);

    /**
     * 根据角色id获取权限id
     * @param roleId
     * @return
     */
    List<String> getMenuIds(String roleId);
}
