package com.ylink.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: PACKAGE_NAME
 * @Author: shuyu
 * @CreateTime: 2022-10-31  16:25
 * @Description:
 * @Version: 1.0
 */
@EnableFeignClients
@SpringBootApplication(scanBasePackages = {"com.ylink.system.*","com.ylink.common.security","com.ylink.common.redis","com.ylink.common.core"})
public class YlinkSystemApplication {
    public static void main(String[] args) {
        SpringApplication.run(YlinkSystemApplication.class,args);
    }
}
