package com.ylink.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ylink.system.entity.SysRoleMenu;
import com.ylink.system.mapper.SysRoleMenuMapper;
import com.ylink.system.service.ISysRoleMenuService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.service.impl
 * @Author: shuyu
 * @CreateTime: 2022-11-04  17:23
 * @Description:
 * @Version: 1.0
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements ISysRoleMenuService {

    @Override
    public void updateList(String roleId, List<String> menus) {
        baseMapper.deleteById(roleId);
        for (String menu : menus) {
            SysRoleMenu sysRoleMenu = new SysRoleMenu(roleId, menu);
            baseMapper.insert(sysRoleMenu);
        }
    }

    @Override
    public void deleteList(String roleId) {
        baseMapper.deleteById(roleId);
    }

    @Override
    public List<SysRoleMenu> getRoleMenuList(String roleId) {
        List<SysRoleMenu> roleMenus = baseMapper.selectList(new QueryWrapper<SysRoleMenu>().eq("role_id", roleId));
        return roleMenus;
    }

    @Override
    public List<String> getMenuIds(String roleId) {
        List<SysRoleMenu> roleMenuList = getRoleMenuList(roleId);
        List<String> menuIds=new ArrayList<>();
        for (SysRoleMenu sysRoleMenu : roleMenuList) {
            menuIds.add(sysRoleMenu.getMenuId());
        }
        return menuIds;
    }
}
