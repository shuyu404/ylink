package com.ylink.system.service;

import com.ylink.system.api.entity.SysUser;

import java.util.Set;
/**
 * @author shuyu
 */
public interface ISysPermissionService {

    /**
     * 获取权限列表
     * @param sysUser   用户
     * @return  权限列表
     */
    public Set<String> getMenuPermission(SysUser sysUser);

}
