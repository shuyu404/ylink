package com.ylink.system.config;

import com.ylink.common.security.interceptor.HeaderInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.config
 * @Author: shuyu
 * @CreateTime: 2022-11-10  10:31
 * @Description:
 * @Version: 1.0
 */

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    /** 不需要拦截地址 */
    public static final String[] excludeUrls = { "/user/info/{username}"};

    @Override
    public void addInterceptors(InterceptorRegistry registry)
    {

        registry.addInterceptor(getHeaderInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns(excludeUrls)
                .order(-10);
    }


    public HeaderInterceptor getHeaderInterceptor()
    {
        return new HeaderInterceptor();
    }
}
