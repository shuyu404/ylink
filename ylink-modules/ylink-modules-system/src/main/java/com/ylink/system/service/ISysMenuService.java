package com.ylink.system.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.ylink.system.entity.SysMenu;
import com.ylink.system.vo.TreeSelect;

import java.util.List;

/**
 * @author shuyu
 */
public interface ISysMenuService extends IService<SysMenu> {

    /**
     * 通过角色获取菜单
     * @param roleId    角色id
     * @return  菜单
     */
    List<SysMenu> getListByRole(String roleId);

    /**
     * 构建树结构
     * @param menus 菜单列表
     * @return  树结构列表
     */
   TreeSelect buildMenuTreeSelect(List<SysMenu> menus);

    /**
     * 构建前端所需要下拉树结构
     * @param menus 菜单列表
     * @return 下拉树结构列表
     */
    List<SysMenu> buildMenuTree(List<SysMenu> menus);

    /**
     * 更新权限列表
     */
    void updateMenuList(String roleId,List<String> menusId);

    /**获取视图列表 */
    List<SysMenu> getViewListByRole(String roleId);

    /**
     * 获取权限列表
     */
    List<SysMenu> getPermissions(String roleId);
}
