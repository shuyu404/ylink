package com.ylink.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ylink.system.entity.SysRoleMenu;
import org.apache.ibatis.annotations.Mapper;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.system.mapper
 * @Author: shuyu
 * @CreateTime: 2022-11-04  17:21
 * @Description:
 * @Version: 1.0
 */
@Mapper
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {
}
