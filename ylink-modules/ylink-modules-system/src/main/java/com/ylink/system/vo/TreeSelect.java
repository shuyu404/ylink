package com.ylink.system.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ylink.system.entity.SysMenu;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Treeselect树结构实体类
 * 
 * @author ruoyi
 */
@Data
public class TreeSelect implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 节点ID */
    private String id;

    /** 节点名称 */
    private String label;

    /** 子节点 */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<TreeSelect> children;

    public TreeSelect()
    {

    }
    public TreeSelect(String id,String label)
    {
        this.id=id;
        this.label=label;
    }


    public TreeSelect(SysMenu menu)
    {
        this.id = menu.getMenuId();
        this.label = menu.getMenuName();
        this.children = menu.getChildren().stream().map(TreeSelect::new).collect(Collectors.toList());
    }

    public TreeSelect buildTree(TreeSelect parent,List<SysMenu> menus){

        parent.children=new ArrayList<>();
        for (SysMenu sysMenu : menus) {
            if(sysMenu.getMenuId().equals(sysMenu.getParentId())){
                throw new RuntimeException("树结构建造错误，出现循环依赖");
            }
            if (sysMenu.getParentId().equals(parent.id) ) {
                TreeSelect childerTree = new TreeSelect(sysMenu.getMenuId(), sysMenu.getMenuName());
           //     menus.remove(sysMenu);
                TreeSelect tree = buildTree(childerTree, menus);
                parent.children.add(tree);
            }
        }
        return parent;
    }



}
