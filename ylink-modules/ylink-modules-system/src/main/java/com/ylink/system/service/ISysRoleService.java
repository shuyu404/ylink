package com.ylink.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ylink.system.entity.SysRole;

import java.util.List;

/**
 * @author shuyu
 */
public interface ISysRoleService extends IService<SysRole> {

    /**
     *
     * @param rootId 根id
     * @return 角色列表
     */
    List<SysRole> getList(String rootId);

    /**
     * 添加角色信息
     * @param sysRole   角色
     */
    void add(SysRole sysRole);

    void deleteByRoleId(String roleId);

    SysRole getInfoBydId(String roleId);


    void updateRole(SysRole sysRole);
}
