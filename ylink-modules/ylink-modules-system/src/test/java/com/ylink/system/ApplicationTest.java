package com.ylink.system;

import com.ylink.system.entity.SysMenu;
import com.ylink.system.service.ISysMenuService;
import com.ylink.system.service.ISysRoleService;
import com.ylink.system.service.ISysUserService;
import com.ylink.system.vo.TreeSelect;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: PACKAGE_NAME
 * @Author: shuyu
 * @CreateTime: 2022-10-31  20:45
 * @Description:
 * @Version: 1.0
 */
@SpringBootTest
public class ApplicationTest {

    @Autowired
    ISysUserService sysUserService;

    @Autowired
    ISysMenuService sysMenuService;

    @Autowired
    ISysRoleService sysRoleService;

    @Test
    void Test() {
        System.out.println(sysUserService.getUser("admin"));
    }

    @Test
    void Test2() {
        System.out.println(sysMenuService.getListByRole("1"));
    }

    @Test
    void Test3() {
        List<SysMenu> listByRole = sysMenuService.getListByRole("1");
        TreeSelect root = new TreeSelect("0", "root");
        System.out.println(root.buildTree(root,listByRole));

    }


    class Tree {
        String id;
        String label;
        List<Tree> children;

        Tree(String id, String label) {
            this.id = id;
            this.label = label;
        }

        Tree buildTree(Tree parent,List<SysMenu> sysMenus){
            parent.children=new ArrayList<>();
            for (SysMenu sysMenu : sysMenus) {
                if (sysMenu.getParentId().equals(parent.id) ) {
                    Tree childerTree = new Tree(sysMenu.getMenuId(), sysMenu.getMenuName());
                    Tree tree = buildTree(childerTree, sysMenus);
                    parent.children.add(tree);
                }
            }

            return parent;
        }

        @Override
        public String toString() {
            return "Tree{" +
                    "id='" + id + '\'' +
                    ", label='" + label + '\'' +
                    ", children=" + children.toString() +
                    '}';
        }
    }
}
