package com.ylink.broker.redis;

import com.ylink.common.core.constant.device.DeviceCacheConstants;
import com.ylink.common.core.constant.device.DeviceConstants;
import com.ylink.common.redis.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.broker.redis
 * @Author: shuyu
 * @CreateTime: 2022-11-12  16:02
 * @Description: 更新redis中设备的状态信息
 * @Version: 1.0
 */
@Component
public class UpdateDeviceStatus {
    private static RedisService redisService;

    @Autowired
    void setRedisService(RedisService redisService) {
        UpdateDeviceStatus.redisService = redisService;
    }

    /**
     * 在线触发缓存
     * @param clientId  客户端id
     */
    public static void online(String clientId){
        Map<String, Object> map = getMap();
        map.put(DeviceConstants.STATUS,DeviceConstants.ONLINE);
        redisService.setCacheObject(DeviceCacheConstants.DEVICE_STATUS+clientId,map);
    }

    /**
     * 离线触发缓存
     * @param clientId 客户端id
     */
    public static void offline(String clientId){
        Map<String, Object> map = getMap();
        map.put(DeviceConstants.STATUS,DeviceConstants.OFFLINE);
        redisService.setCacheObject(DeviceCacheConstants.DEVICE_STATUS+clientId,map);
    }

    private static Map<String,Object> getMap(){
        Map<String,Object> map=new HashMap<>();
        map.put(DeviceConstants.LAST_UPDATE_TIME,new Date());
        return map;
    }


}
