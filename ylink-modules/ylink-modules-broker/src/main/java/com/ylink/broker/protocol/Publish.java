package com.ylink.broker.protocol;


import com.ylink.broker.cache.entity.SessionStore;
import com.ylink.broker.cache.service.ChannelService;
import com.ylink.broker.cache.service.SessionStoreService;
import com.ylink.broker.cache.service.SubscribeStoreService;
import com.ylink.broker.constants.SessionConstant;
import com.ylink.broker.handler.MessageHandler;
import com.ylink.common.core.constant.device.DeviceConstants;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.Channel;
import io.netty.channel.ChannelId;
import io.netty.handler.codec.mqtt.*;
import io.netty.util.AttributeKey;
import org.springframework.stereotype.Component;

import java.util.Set;


/**
 * @BelongsProject: ylinkMqtt
 * @BelongsPackage: com.example.ylinkmqtt.protocol
 * @Author: shuyu
 * @CreateTime: 2022-11-07  14:51
 * @Description: 发布消息处理类
 * @Version: 1.0
 */
@Component
public class Publish {

    public void publish(Channel channel, MqttPublishMessage message) {
        String topicName = message.variableHeader().topicName();

        Set<String> clientIds = SubscribeStoreService.searchClientId(topicName);
        if (clientIds != null && clientIds.size() > 0) {
            for (String clientId : clientIds) {
                ChannelId channelId = SessionStoreService.getSessionStore(clientId).getChannelId();
                MqttPublishMessage copy = message.copy();
                ChannelService.getChannel(channelId).writeAndFlush(copy);
            }
        }
        publishReq(channel, message);

        String clientId = (String) channel.attr(AttributeKey.valueOf(SessionConstant.CLIENT_ID)).get();

        //判断数据来自device而不是客户端
        SessionStore sessionStore = SessionStoreService.getSessionStore(clientId);
        if (DeviceConstants.DEVICE.equals(sessionStore.getType())) {
            //此处可以做异步，或者发送到消息中间件

            if (sessionStore.getPubString().equals(message.variableHeader().topicName())) {
                MessageHandler.handler(channel, message);

            }
        }
    }

    /**
     * 直接发送数据
     * @param rootId 根id
     * @param deviceId  设备id
     * @param buf    传输数据
     */
    public static void sendMsg(String rootId, String deviceId, ByteBuf buf) {
        String topicName = String.format("%s/%s/sub", rootId, deviceId);
        Set<String> clientIds = SubscribeStoreService.searchClientId(topicName);
        System.out.println(topicName);
        MqttPublishMessage mqttPublishMessage = new MqttPublishMessage(new MqttFixedHeader(MqttMessageType.PUBLISH, false, MqttQoS.AT_MOST_ONCE, false, 0),
                new MqttPublishVariableHeader(topicName,0), buf);

        if(clientIds!=null&&clientIds.size()>0){
            for (String clientId : clientIds) {
                ChannelId channelId = SessionStoreService.getSessionStore(clientId).getChannelId();
                MqttPublishMessage copy = mqttPublishMessage.copy();
                ChannelService.getChannel(channelId).writeAndFlush(copy);
            }
        }
    }

    private void publishReq(Channel channel, MqttPublishMessage message) {
        MqttQoS mqttQoS = message.fixedHeader().qosLevel();

        if (MqttQoS.AT_LEAST_ONCE == mqttQoS) {
            MqttMessage message1 = MqttMessageFactory.newMessage(new MqttFixedHeader(MqttMessageType.PUBACK, false, MqttQoS.AT_MOST_ONCE, false, 0),
                    null, null);
            sendPublishMessage(channel, message1);
        }
        if (MqttQoS.EXACTLY_ONCE == mqttQoS) {
            MqttMessage message1 = MqttMessageFactory.newMessage(new MqttFixedHeader(MqttMessageType.PUBREC, false, MqttQoS.AT_MOST_ONCE, false, 0),
                    null, null);
            sendPublishMessage(channel, message1);
        }

    }

    private void sendPublishMessage(Channel channel, MqttMessage message) {
        channel.writeAndFlush(message);
    }


}
