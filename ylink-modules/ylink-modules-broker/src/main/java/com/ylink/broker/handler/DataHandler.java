package com.ylink.broker.handler;

import com.alibaba.fastjson2.JSON;
import com.fasterxml.jackson.databind.ser.std.StringSerializer;
import com.ylink.broker.entity.DeviceDataSave;
import com.ylink.broker.kafka.KafkaProducer;
import com.ylink.common.core.constant.device.DeviceConstants;
import com.ylink.common.core.constant.kafka.KafkaConstants;
import nonapi.io.github.classgraph.json.JSONSerializer;
import org.apache.iotdb.rpc.IoTDBConnectionException;
import org.apache.iotdb.rpc.StatementExecutionException;
import org.apache.iotdb.session.pool.SessionPool;
import org.apache.iotdb.tsfile.file.metadata.enums.TSDataType;
import org.apache.kafka.common.protocol.ObjectSerializationCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.broker.handler
 * @Author: shuyu
 * @CreateTime: 2022-11-10  19:57
 * @Description: 数据处理中心类
 * @Version: 1.0
 */
@Component
public class DataHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataHandler.class);

    private static SessionPool sessionPool;


    @Autowired
    public void setSessionPool(SessionPool sessionPool) {
        DataHandler.sessionPool = sessionPool;
    }

    static TSDataType getTSDataType(int type) {
        switch (type) {
            case 0:
            case 1:
            case 2:
                return TSDataType.INT32;
            case 3:
                return TSDataType.INT64;
            case 4:
                return TSDataType.FLOAT;
            case 5:
                return TSDataType.DOUBLE;
            default:
                throw new RuntimeException("非法格式");
        }
    }


    static void saveData(DeviceDataSave deviceDataSave) {
        String deviceId = String.format("%s.%s.%s", DeviceConstants.ROOT, deviceDataSave.getRootId(), deviceDataSave.getClientId());

        try {
            sessionPool.insertRecord(deviceId, deviceDataSave.getTime(), deviceDataSave.getNames(), deviceDataSave.getTypes(), deviceDataSave.getValues());
        } catch (IoTDBConnectionException | StatementExecutionException e) {
            LOGGER.error("数据保存错误 错误原因：" + e.getMessage());
        }
        //发送设备消息到kafka
        KafkaProducer.sendDeviceMessage(deviceDataSave);
    }

}
