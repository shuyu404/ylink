package com.ylink.broker.cache.entity;

import com.ylink.device.api.entity.ModelData;
import io.netty.channel.Channel;
import io.netty.channel.ChannelId;
import io.netty.handler.codec.mqtt.MqttPublishMessage;
import lombok.Data;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * @BelongsProject: ylinkMqtt
 * @BelongsPackage: com.example.ylinkmqtt.cache.entity
 * @Author: shuyu
 * @CreateTime: 2022-11-06  15:21
 * @Description: 会话存储
 * @Version: 1.0
 */
@Data
public class SessionStore implements Serializable {
    private static final long serialVersionUID = -1L;

    private String rootId;

    private String clientId;

    private String deviceName;

    private ChannelId channelId;

    private String pubString;

    private String dataType;

    private String type;

    private Set<String> topics;

    private int expire;


    private List<ModelData> modelDataList;

    private MqttPublishMessage willMassage;

}
