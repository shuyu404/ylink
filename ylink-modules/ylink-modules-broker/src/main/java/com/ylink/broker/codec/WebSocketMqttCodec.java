package com.ylink.broker.codec;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageCodec;
import io.netty.handler.codec.http.websocketx.BinaryWebSocketFrame;

import java.util.List;

/**
 * @BelongsProject: ZQ_Cloud
 * @BelongsPackage: com.zqwl.mqttbroker.websocket.handler
 * @Author: shuyu
 * @CreateTime: 2022-07-20  19:07
 * @Description:
 * @Version: 1.0
 */

public class WebSocketMqttCodec extends MessageToMessageCodec<BinaryWebSocketFrame, ByteBuf> {

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) {
        list.add(new BinaryWebSocketFrame(byteBuf.retain()));
    }

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, BinaryWebSocketFrame binaryWebSocketFrame, List<Object> list) {
        list.add(binaryWebSocketFrame.retain().content());
    }
}
