package com.ylink.broker.cache.service;

import com.ylink.broker.cache.entity.SessionStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * @BelongsProject: ylinkMqtt
 * @BelongsPackage: com.example.ylinkmqtt.cache.service.impl
 * @Author: shuyu
 * @CreateTime: 2022-11-06  15:36
 * @Description:
 * @Version: 1.0
 */

public class SubscribeStoreService {
    private static final Logger LOGGER= LoggerFactory.getLogger(SubscribeStoreService.class);
    private static final Map<String, ConcurrentSkipListSet<String>> SUB_MAP = new ConcurrentHashMap<>();


    public static void put(String topic, String clientId) {
        SessionStore sessionStore = SessionStoreService.getSessionStore(clientId);
        if (sessionStore != null) {
            if (!SUB_MAP.containsKey(topic)) {
                SUB_MAP.put(topic, new ConcurrentSkipListSet<>());
            }
            SUB_MAP.get(topic).add(clientId);
            if (sessionStore.getTopics() == null) {
                sessionStore.setTopics(new HashSet<>());
            }
            sessionStore.getTopics().add(topic);
            LOGGER.info(String.format("当前TOPIC：%s",SUB_MAP));
        }
    }


    public static void remove(String topicFilter, String clientId) {
        if (SUB_MAP.containsKey(topicFilter)) {
            SUB_MAP.get(topicFilter).remove(clientId);
            if(SUB_MAP.get(topicFilter).size()==0){
                SUB_MAP.remove(topicFilter);
            }
        }
        SessionStore sessionStore = SessionStoreService.getSessionStore(clientId);
        if(sessionStore !=null){
            if(sessionStore.getTopics()!=null){
                sessionStore.getTopics().remove(topicFilter);
            }
        }
        LOGGER.info(String.format("当前TOPIC：%s",SUB_MAP));
    }


    public static void removeForClient(String clientId) {
        SessionStore sessionStore = SessionStoreService.getSessionStore(clientId);
        if (sessionStore !=null&&sessionStore.getTopics()!=null) {
            for (String topic : sessionStore.getTopics()) {
                remove(topic,clientId);
            }
        }
    }


    public static Set<String> searchClientId(String topic) {
        return SUB_MAP.get(topic);
    }
}
