package com.ylink.broker.entity;

import lombok.Data;
import org.apache.iotdb.tsfile.file.metadata.enums.TSDataType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.broker.entity
 * @Author: shuyu
 * @CreateTime: 2022-11-10  19:48
 * @Description:
 * @Version: 1.0
 */
@Data
public class DeviceDataSave {
    /**
     * 根id+ clientId
     */
    String rootId;
    /**
     * 设备名
     */
    String deviceName;

    String clientId;

    /**
     * 数据名称列表
     */
    List<String> dataNames;

    /**
     * 数据path列表
     */
    List<String> names;

    /**
     * 数组列表
     */
    List<Object> values;

    /**
     * 数据列表
     */
    List<TSDataType> types;

    /**
     * 时间
     */
    Long time;

    public DeviceDataSave() {
    }

    public DeviceDataSave(String rootId,String clientId) {
        this.rootId=rootId;
        this.clientId=clientId;
        this.names=new ArrayList<>();
        this.values=new ArrayList<>();
        this.types=new ArrayList<>();
        this.dataNames=new ArrayList<>();
        this.time=System.currentTimeMillis();
    }

}
