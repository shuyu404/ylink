package com.ylink.broker;

import com.ylink.broker.codec.WebSocketMqttCodec;
import com.ylink.broker.config.BrokerConfig;
import com.ylink.broker.handler.BrokerHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpContentCompressor;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.codec.mqtt.MqttDecoder;
import io.netty.handler.codec.mqtt.MqttEncoder;
import io.netty.handler.timeout.IdleStateHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.broker
 * @Author: shuyu
 * @CreateTime: 2022-11-09  19:38
 * @Description: 服务启动类
 * @Version: 1.0
 */
@Component
public class BootStrap {

    @Autowired
    public BootStrap(BrokerHandler brokerHandler) {
        this.brokerHandler = brokerHandler;
    }

    private EventLoopGroup bossGroup;
    private EventLoopGroup workGroup;
    private final BrokerHandler brokerHandler;



    @PostConstruct
    public void start() {
        bossGroup = new NioEventLoopGroup();
        workGroup = new NioEventLoopGroup();
        try {
            mqttStart();
          //  webSocketStart();
            System.out.println("broker启动成功");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * mqtt启动
     */
    private void mqttStart() throws InterruptedException {
        ServerBootstrap serverBootstrap = new ServerBootstrap();
        serverBootstrap.group(bossGroup, workGroup).channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        ChannelPipeline pipeline = socketChannel.pipeline();
                        //添加心跳检测
                        pipeline.addFirst("idle", new IdleStateHandler(0, 0, BrokerConfig.OUT_TIME));
                        pipeline.addLast("decoder", new MqttDecoder());
                        pipeline.addLast("encoder", MqttEncoder.INSTANCE);
                        //进行Mqtt处理
                        pipeline.addLast("borker", brokerHandler);
                    }
                }).option(ChannelOption.SO_KEEPALIVE, BrokerConfig.IS_KEEP_ALIVE);
        serverBootstrap.bind(BrokerConfig.PORT).sync();
    }

    /**
     * webSocket启动
     */
    private void webSocketStart() throws InterruptedException {
        ServerBootstrap serverBootstrap = new ServerBootstrap();
        serverBootstrap.group(bossGroup, workGroup).channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        ChannelPipeline pipeline = socketChannel.pipeline();
                        //添加心跳检测
                        pipeline.addFirst("idle", new IdleStateHandler(0, 0, BrokerConfig.OUT_TIME));
                        // 将请求和应答消息编码或解码为HTTP消息
                        pipeline.addLast("http-codec", new HttpServerCodec());
                        // 将HTTP消息的多个部分合成一条完整的HTTP消息
                        pipeline.addLast("aggregator", new HttpObjectAggregator(1048576));
                        // 将HTTP消息进行压缩编码
                        pipeline.addLast("compressor ", new HttpContentCompressor());
                        pipeline.addLast("protocol", new WebSocketServerProtocolHandler(BrokerConfig.WS_PATH, "mqtt,mqttv3.1,mqttv3.1.1", true, 65536));
                        //对websocket进行解码
                        pipeline.addLast("mqttWebSocket", new WebSocketMqttCodec());
                        pipeline.addLast("decoder", new MqttDecoder());
                        pipeline.addLast("encoder", MqttEncoder.INSTANCE);
                        //进行Mqtt处理
                        pipeline.addLast("borker", brokerHandler);
                    }
                }).option(ChannelOption.SO_KEEPALIVE, BrokerConfig.IS_KEEP_ALIVE);
        serverBootstrap.bind(BrokerConfig.WS_PORT).sync();
    }
}
