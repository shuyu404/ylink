package com.ylink.broker.protocol;


import com.ylink.broker.cache.service.SessionStoreService;
import com.ylink.broker.constants.SessionConstant;
import io.netty.channel.Channel;
import io.netty.handler.codec.mqtt.*;
import io.netty.util.AttributeKey;
import org.springframework.stereotype.Component;

/**
 * @BelongsProject: ylinkMqtt
 * @BelongsPackage: com.example.ylinkmqtt.protocol
 * @Author: shuyu
 * @CreateTime: 2022-11-07  14:19
 * @Description:
 * @Version: 1.0
 */
@Component
public class PingReq {

    public void pingReq(Channel channel, MqttMessage message) {
        String clientId = (String) channel.attr(AttributeKey.valueOf(SessionConstant.CLIENT_ID)).get();

        if (SessionStoreService.hasSession(clientId)) {

            MqttMessage pingMessage = MqttMessageFactory.newMessage(new MqttFixedHeader(MqttMessageType.PINGRESP, false, MqttQoS.AT_MOST_ONCE, false, 0),
                    null, null);

            channel.writeAndFlush(pingMessage);
        } else {
            channel.close();
        }

    }
}
