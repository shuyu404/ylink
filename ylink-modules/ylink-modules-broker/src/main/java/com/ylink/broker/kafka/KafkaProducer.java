package com.ylink.broker.kafka;

import com.alibaba.fastjson2.JSON;
import com.ylink.broker.entity.DeviceDataSave;
import com.ylink.common.core.constant.kafka.KafkaConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.broker.kafka
 * @Author: shuyu
 * @CreateTime: 2022-12-22  10:07
 * @Description:
 * @Version: 1.0
 */
@Service
public class KafkaProducer {
    static KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    void setKafkaTemplate(KafkaTemplate<String, String> kafka) {
        kafkaTemplate = kafka;
    }

    public static void sendDeviceMessage(DeviceDataSave deviceDataSave){
        //发生解析完成数据到kafka
        kafkaTemplate.send(KafkaConstants.TO_RULE_MESSAGE, JSON.toJSONString(deviceDataSave));
    }

    public static void sendDeviceStatus(){
        kafkaTemplate.send(KafkaConstants.TO_RULE_STATUS,"");
    }

}
