package com.ylink.broker.constants;

/**
 * @BelongsProject: ylinkMqtt
 * @BelongsPackage: com.example.ylinkmqtt.constants
 * @Author: shuyu
 * @CreateTime: 2022-11-06  13:48
 * @Description:
 * @Version: 1.0
 */

public class SessionConstant {

    public static final String IDLE="idle";

    public static final String CLIENT_ID="clientId";
}
