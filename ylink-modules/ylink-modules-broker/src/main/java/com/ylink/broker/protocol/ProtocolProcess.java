package com.ylink.broker.protocol;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @BelongsProject: ylinkMqtt
 * @BelongsPackage: com.example.ylinkmqtt.protocol
 * @Author: shuyu
 * @CreateTime: 2022-11-06  14:45
 * @Description:
 * @Version: 1.0
 */
@Component
public class ProtocolProcess {
    @Autowired
    Connect connect;

    @Autowired
    Subscribe subscribe;

    @Autowired
    UnSubscribe unSubscribe;

    @Autowired
    DisConnect disConnectl;

    @Autowired
    PingReq pingReq;

    @Autowired
    Publish publish;

    public Connect connect() {
        return connect;
    }

    public Subscribe subscribe() {
        return subscribe;
    }

    public UnSubscribe unSubscribe() {
        return unSubscribe;
    }


    public DisConnect disConnectl() {
        return disConnectl;
    }

    public PingReq pingReq() {
        return pingReq;
    }

    public Publish publish(){
        return publish;
    }
}
