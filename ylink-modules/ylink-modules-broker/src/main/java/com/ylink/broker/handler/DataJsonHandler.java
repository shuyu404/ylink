package com.ylink.broker.handler;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.ylink.broker.cache.entity.SessionStore;
import com.ylink.broker.entity.DeviceDataSave;
import com.ylink.common.core.constant.device.DeviceConstants;
import com.ylink.common.core.enums.DataType;
import com.ylink.common.core.utils.StringUtils;
import com.ylink.device.api.entity.ModelData;
import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.mqtt.MqttPublishMessage;

import java.nio.charset.Charset;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.broker.handler
 * @Author: shuyu
 * @CreateTime: 2022-11-10  14:41
 * @Description:
 * @Version: 1.0
 */

public class DataJsonHandler extends DataHandler {

    public static void handler(MqttPublishMessage message, SessionStore sessionStore) {
        List<ModelData> modelDataList = sessionStore.getModelDataList();
        if (modelDataList.size() == 0) {
            return;
        }

        ByteBuf payload = message.payload();
        CharSequence charSequence = payload.readCharSequence(payload.readableBytes(), Charset.defaultCharset());
        String payloadStr = charSequence.subSequence(0, charSequence.length()).toString();

        JSONObject jsonPayload = JSON.parseObject(payloadStr);
        JSONObject jsonParams = jsonPayload.getJSONObject(DeviceConstants.PARAMS);


        if (StringUtils.isNotNull(jsonParams)) {

            //初始化存储数据体
            DeviceDataSave deviceDataSave = new DeviceDataSave(sessionStore.getRootId(),sessionStore.getClientId());

            deviceDataSave.setDeviceName(sessionStore.getDeviceName());
            for (ModelData modelData : modelDataList) {
                Object data = getData(jsonParams, modelData);
                if(data!=null){
                    deviceDataSave.getDataNames().add(modelData.getName());
                    deviceDataSave.getNames().add(modelData.getDataId());
                    deviceDataSave.getTypes().add(getTSDataType(modelData.getType()));
                    deviceDataSave.getValues().add(data);
                }
            }
            //存储数据
            saveData(deviceDataSave);

        }

    }


    static Object getData(JSONObject jsonObject, ModelData modelData) {

        switch (DataType.getType(modelData.getType())) {
            case BOOLEAN:
            case SHORT:
            case INT32:
                return jsonObject.getInteger(modelData.getFlag());
            case INT64:
                return jsonObject.getLong(modelData.getFlag());
            case FLOAT:
                return jsonObject.getFloat(modelData.getFlag());
            case DOUBLE:
                return jsonObject.getDouble(modelData.getFlag());
            default:
                throw new RuntimeException("非法的数据类型");
        }
    }
}
