package com.ylink.broker.config;

/**
 * @BelongsProject: ylinkMqtt
 * @BelongsPackage: com.example.ylinkmqtt.config
 * @Author: shuyu
 * @CreateTime: 2022-11-06  13:46
 * @Description:
 * @Version: 1.0
 */

public class BrokerConfig {

    /**
     * 地址
     */
    public static  String HOST="127.0.0.1";

    /**
     * broker id
     */
    public static String BROKER_ID="v1_";

    /**
     * 端口号
     */
    public static  int PORT=1883;

    /**
     * websocket 端口
     */
    public static int WS_PORT=8083;

    public static String WS_PATH="/mqtt";

    /**
     * 默认超时时间 单位秒 实际应根据设备连接情况来决定
     */
    public static int OUT_TIME=300;

    /**
     * 鉴权配置默认关闭
     */
    public static boolean isAuth=false;

    public static boolean IS_KEEP_ALIVE=true;

}
