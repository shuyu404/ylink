package com.ylink.broker.handler;

import com.ylink.broker.cache.entity.SessionStore;
import com.ylink.broker.cache.service.SessionStoreService;
import com.ylink.broker.constants.SessionConstant;
import com.ylink.common.core.constant.device.DeviceConstants;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.Channel;
import io.netty.handler.codec.mqtt.MqttPublishMessage;
import io.netty.util.AttributeKey;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * @BelongsProject: ylinkMqtt
 * @BelongsPackage: com.example.ylinkmqtt.handler
 * @Author: shuyu
 * @CreateTime: 2022-11-08  15:10
 * @Description:
 * @Version: 1.0
 */

public class MessageHandler {
    public static void handler(Channel channel, MqttPublishMessage message) {


        String clientId = (String) channel.attr(AttributeKey.valueOf(SessionConstant.CLIENT_ID)).get();
        SessionStore sessionStore = SessionStoreService.getSessionStore(clientId);
        if (DeviceConstants.JSON.equals(sessionStore.getDataType())) {
            DataJsonHandler.handler(message,sessionStore);
        } else if (DeviceConstants.MODBUS.equals(sessionStore.getDataType())) {
            DataModbusHandler.handler(message,sessionStore);
        }
    }
}