package com.ylink.broker.handler;

import com.ylink.broker.cache.entity.SessionStore;
import com.ylink.broker.entity.DeviceDataSave;
import com.ylink.common.core.enums.DataType;
import com.ylink.device.api.entity.ModelData;
import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.mqtt.MqttPublishMessage;

import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.broker.handler
 * @Author: shuyu
 * @CreateTime: 2022-11-10  14:42
 * @Description:
 * @Version: 1.0
 */

public class DataModbusHandler extends DataHandler {
    static byte[] type = {3};


    public static void handler(MqttPublishMessage message, SessionStore sessionStore) {

        byte[] bytes = new byte[1];

        ByteBuf payload = message.payload();
        //  System.out.println(ByteBufUtil.hexDump(payload));

        //获取modbus类型
        payload.getBytes(1, bytes);

        List<ModelData> modelDataList = sessionStore.getModelDataList();
        if (bytes[0] == type[0] && modelDataList.size() > 0) {
            //初始化存储数据体
            DeviceDataSave deviceDataSave = new DeviceDataSave(sessionStore.getRootId(), sessionStore.getClientId());

            deviceDataSave.setDeviceName(sessionStore.getDeviceName());

            for (ModelData modelData : modelDataList) {
                deviceDataSave.getDataNames().add(modelData.getName());
                deviceDataSave.getNames().add(modelData.getDataId());
                deviceDataSave.getTypes().add(getTSDataType(modelData.getType()));
                deviceDataSave.getValues().add(getData(payload, modelData));
            }
            //存储数据
            saveData(deviceDataSave);
        }

    }

    /**
     * 数据解析
     *
     * @param byteBuf   字节数据
     * @param modelData 解析规则
     * @return 解析后数据
     */
    static Object getData(ByteBuf byteBuf, ModelData modelData) {

        int readIndex = Integer.parseInt(modelData.getFlag()) + 3;
        switch (DataType.getType(modelData.getType())) {
            case BOOLEAN:
                return byteBuf.getBoolean(readIndex)?1:0;
            case SHORT:
                return (int) byteBuf.getShort(readIndex);
            case INT32:
                return byteBuf.getInt(readIndex);
            case INT64:
                return byteBuf.getLong(readIndex);
            case DOUBLE:
                return byteBuf.getDouble(readIndex);
            default:
                throw new RuntimeException("不合法的数据类型");
        }
    }
}
