package com.ylink.broker.auth;

import org.springframework.stereotype.Service;

/**
 * @BelongsProject: ylinkMqtt
 * @BelongsPackage: com.example.ylinkmqtt.auth
 * @Author: shuyu
 * @CreateTime: 2022-11-06  16:47
 * @Description:
 * @Version: 1.0
 */
@Service
public interface IAuthService {

    boolean valiAuth(String username, String password);
}
