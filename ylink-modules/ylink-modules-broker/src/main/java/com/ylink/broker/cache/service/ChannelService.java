package com.ylink.broker.cache.service;

import com.ylink.broker.cache.entity.SessionStore;
import com.ylink.broker.constants.SessionConstant;
import io.netty.channel.Channel;
import io.netty.channel.ChannelId;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.AttributeKey;
import io.netty.util.concurrent.GlobalEventExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @BelongsProject: ylinkMqtt
 * @BelongsPackage: com.example.ylinkmqtt.cache.service.impl
 * @Author: shuyu
 * @CreateTime: 2022-11-06  21:24
 * @Description:
 * @Version: 1.0
 */
@Component
public class ChannelService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChannelService.class);

    private static ChannelGroup channelGroup;


    @PostConstruct
    void init() {
        channelGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    }

    public static void addChannel(Channel channel) {
        channelGroup.add(channel);
        LOGGER.info(String.format("通道增加当前数量：%d", channelGroup.size()));
    }

    public static void channelInit(Channel channel, SessionStore sessionStore) {
        SessionStore hasSessionStore = SessionStoreService.getSessionStore(sessionStore.getClientId());
        if (hasSessionStore != null) {
            channelGroup.find(hasSessionStore.getChannelId()).close();
            SessionStoreService.remove(hasSessionStore.getClientId());
        }
        channel.attr(AttributeKey.valueOf(SessionConstant.CLIENT_ID)).set(sessionStore.getClientId());
        SessionStoreService.put(sessionStore.getClientId(), sessionStore);
    }

    public static Channel getChannel(ChannelId channelId) {
        return channelGroup.find(channelId);
    }

    public static void remove(Channel channel) {
        channelGroup.remove(channel);
        String clientId = (String) channel.attr(AttributeKey.valueOf(SessionConstant.CLIENT_ID)).get();
        System.out.println(clientId);
        if (channel.id().equals(SessionStoreService.getSessionStore(clientId).getChannelId())) {
            SessionStoreService.remove(clientId);
        }
        LOGGER.info(String.format("通道减少当前数量：%d", channelGroup.size()));
    }



}
