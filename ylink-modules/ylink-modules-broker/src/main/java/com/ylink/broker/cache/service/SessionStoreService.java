package com.ylink.broker.cache.service;

import com.ylink.broker.cache.entity.SessionStore;
import com.ylink.broker.redis.UpdateDeviceStatus;
import com.ylink.common.core.constant.device.DeviceConstants;
import com.ylink.device.api.RemoteModelDataService;
import com.ylink.device.api.domain.DeviceVo;
import com.ylink.device.api.entity.Device;
import com.ylink.device.api.entity.DeviceModel;
import com.ylink.device.api.entity.ModelData;
import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @BelongsProject: ylinkMqtt
 * @BelongsPackage: com.example.ylinkmqtt.cache.service.impl
 * @Author: shuyu
 * @CreateTime: 2022-11-06  15:27
 * @Description:
 * @Version: 1.0
 */
@Component
public class SessionStoreService {

    private static RemoteModelDataService remoteModelDataService;

    @Autowired
    void setRemoteModelDataService(RemoteModelDataService service) {
        SessionStoreService.remoteModelDataService = service;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(ChannelService.class);

    private static final Map<String, SessionStore> SESSION_STORE_MAP = new ConcurrentHashMap<>();

    public static void put(String clientId, SessionStore sessionStore) {
        SESSION_STORE_MAP.put(clientId, sessionStore);
        //更新缓存
        UpdateDeviceStatus.online(clientId);
        LOGGER.info(String.format("session增加当前session为：%d", SESSION_STORE_MAP.size()));
    }


    public static SessionStore getSessionStore(String clientId) {
        return SESSION_STORE_MAP.get(clientId);
    }


    public static void remove(String clientId) {
        SubscribeStoreService.removeForClient(clientId);
        SESSION_STORE_MAP.remove(clientId);
        //更新缓存内容
        UpdateDeviceStatus.offline(clientId);

        LOGGER.info(String.format("session减少当前session为：%d", SESSION_STORE_MAP.size()));
    }

    public static boolean hasSession(String clientId) {
        return getSessionStore(clientId) != null;
    }


    /**session初始化*/
    public static void initSession(Channel channel, SessionStore sessionStore) {
        DeviceVo data = remoteModelDataService.getDeviceVo(sessionStore.getClientId()).getData();

        Device device = data.getDevice();
        if(device==null){
            channel.close();
            return;
        }

        DeviceModel deviceModel = data.getDeviceModel();
        List<ModelData> modelDataList = data.getModelDataList();
        if (sessionStore.getClientId().startsWith(DeviceConstants.USER)) {
            sessionStore.setType(DeviceConstants.USER);
        }else {
            sessionStore.setType(DeviceConstants.DEVICE);
        }
        sessionStore.setDeviceName(device.getName());
        sessionStore.setRootId(device.getRootId());
        sessionStore.setDataType(deviceModel.getType());
        sessionStore.setChannelId(channel.id());
        sessionStore.setModelDataList(modelDataList);
        sessionStore.setPubString(device.getPub());
    }

}
