package com.ylink.broker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.broker
 * @Author: shuyu
 * @CreateTime: 2022-11-09  19:37
 * @Description:
 * @Version: 1.0
 */
@EnableFeignClients(basePackages = {"com.ylink.device.api"})
@SpringBootApplication(scanBasePackages = {"com.ylink.broker","com.ylink.device.api","com.ylink.common.redis"})
public class YlinkBrokerApplication {
    public static void main(String[] args) {
        SpringApplication.run(YlinkBrokerApplication.class,args);
    }
}
