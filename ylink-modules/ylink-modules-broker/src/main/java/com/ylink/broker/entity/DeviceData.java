package com.ylink.broker.entity;

import lombok.Data;

import java.util.Date;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.broker.entity
 * @Author: shuyu
 * @CreateTime: 2022-11-10  19:48
 * @Description:
 * @Version: 1.0
 */
@Data
public class DeviceData {
    String dataId;

    String dataName;

    Object value;

    Date time;
}
