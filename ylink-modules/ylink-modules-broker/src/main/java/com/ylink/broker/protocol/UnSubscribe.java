package com.ylink.broker.protocol;

import com.ylink.broker.cache.service.SubscribeStoreService;
import com.ylink.broker.constants.SessionConstant;
import io.netty.channel.Channel;
import io.netty.handler.codec.mqtt.*;
import io.netty.util.AttributeKey;
import org.springframework.stereotype.Component;

/**
 * @BelongsProject: ylinkMqtt
 * @BelongsPackage: com.example.ylinkmqtt.protocol
 * @Author: shuyu
 * @CreateTime: 2022-11-06  22:08
 * @Description:
 * @Version: 1.0
 */
@Component
public class UnSubscribe {

    public void unSubscribe(Channel channel, MqttUnsubscribeMessage message){
        String clientId = (String) channel.attr(AttributeKey.valueOf(SessionConstant.CLIENT_ID)).get();
        for (String topic : message.payload().topics()) {
            SubscribeStoreService.remove(topic,clientId);
        }

        MqttUnsubAckMessage unsubAckMessage =(MqttUnsubAckMessage) MqttMessageFactory.newMessage(
                new MqttFixedHeader(MqttMessageType.UNSUBACK, false, MqttQoS.AT_MOST_ONCE, false, 0),
                MqttMessageIdVariableHeader.from(message.variableHeader().messageId()),
                new MqttUnsubAckPayload());
        channel.writeAndFlush(unsubAckMessage);
    }
}
