package com.ylink.broker.controller;

import com.ylink.broker.api.entity.SendControllerData;
import com.ylink.broker.protocol.Publish;
import com.ylink.common.core.domain.R;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.buffer.PooledByteBufAllocator;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.broker.controller
 * @Author: shuyu
 * @CreateTime: 2022-12-17  17:35
 * @Description:
 * @Version: 1.0
 */
@Api(tags = "设备模型")
@RestController
@RequestMapping("/sendMsg")
public class SendMsgController {
    static String HEX="Hex";
    static String STR="Str";

    @PostMapping("/send")
    public void send(@RequestBody SendControllerData controllerData){
        ByteBuf buf=null;
        String data = controllerData.getData();
        if (HEX.equals(controllerData.getType())) {
            byte[] bytes = ByteBufUtil.decodeHexDump(data);
            buf = PooledByteBufAllocator.DEFAULT.directBuffer();
            buf.writeBytes(bytes);

        }else if (STR.equals(controllerData.getType())){
            byte[] bytes = data.getBytes(StandardCharsets.UTF_8);
            buf = PooledByteBufAllocator.DEFAULT.directBuffer();
            buf.writeBytes(bytes);
        }

        if(buf!=null){
            Publish.sendMsg(controllerData.getRootId(), controllerData.getDeviceId(), buf);
        }

    }
}
