package com.ylink.broker;

import com.ylink.device.api.RemoteModelDataService;
import com.ylink.device.api.domain.DeviceVo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.broker
 * @Author: shuyu
 * @CreateTime: 2022-11-09  21:51
 * @Description:
 * @Version: 1.0
 */
@SpringBootTest
public class ApplicationTest {

    @Autowired
    RemoteModelDataService remoteModelDataService;

    @Test
    void test() {

        DeviceVo data = remoteModelDataService.getDeviceVo("1590280810370981890").getData();
        System.out.println(data);
    }

    @Autowired
    KafkaTemplate<String,String> kafkaTemplate;

    @Test
    void TestKafka(){
        System.out.println(kafkaTemplate);
    }
}
