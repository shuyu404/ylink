package com.ylink.device.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ylink.device.api.entity.ModelData;
import com.ylink.device.mapper.ModelDataMapper;
import com.ylink.device.service.IModelDataService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.service.impl
 * @Author: shuyu
 * @CreateTime: 2022-11-09  14:36
 * @Description:    模型数据类实现
 * @Version: 1.0
 */
@Service
public class ModelDataServiceImpl extends ServiceImpl<ModelDataMapper, ModelData> implements IModelDataService {
    @Override
    public boolean add(ModelData modelData) {
        modelData.setDataId(IdWorker.getIdStr());
        modelData.setCreateTime(new Date());
        return baseMapper.insert(modelData) > 0;
    }

    @Override
    public boolean delete(String dataId) {
        return baseMapper.deleteById(dataId) > 0;
    }

    @Override
    public List<ModelData> getListByMdoelId(String modelId) {
        return baseMapper.selectList(new QueryWrapper<ModelData>().eq("model_id", modelId));
    }

    @Override
    public ModelData getModelDataInfo(String modelDataId) {
        return this.baseMapper.selectById(modelDataId);
    }

    @Override
    public boolean updateInfo(ModelData modelData) {
        return this.baseMapper.updateById(modelData) > 0;
    }
}
