package com.ylink.device.controller;

import com.ylink.common.core.constant.device.DeviceConstants;
import com.ylink.common.core.domain.R;
import com.ylink.common.core.utils.StringUtils;
import com.ylink.common.core.web.controller.BaseController;
import com.ylink.common.core.web.page.TableDataInfo;
import com.ylink.common.core.web.vo.SelectOption;
import com.ylink.common.security.utils.SecurityUtils;
import com.ylink.device.Redis.UpdateDeviceStatus;
import com.ylink.device.api.entity.Device;
import com.ylink.device.api.entity.ModelData;
import com.ylink.device.service.IModelDataService;
import com.ylink.device.service.impl.DeviceServiceImpl;
import com.ylink.device.service.impl.ModelDataServiceImpl;
import com.ylink.device.service.iotdb.IotdbDDLService;
import com.ylink.system.api.entity.SysUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.controller
 * @Author: shuyu
 * @CreateTime: 2022-11-08  17:06
 * @Description: 设备
 * @Version: 1.0
 */
@Api(tags = "设备 ")
@RestController
@RequestMapping("device")
public class DeviceController extends BaseController {

    @Autowired
    DeviceServiceImpl deviceService;
    @Autowired
    IotdbDDLService ddlService;
    @Autowired
    IModelDataService modelDataService;

    @ApiOperation("添加设备")
    @PostMapping("/add")
    public R add(@RequestBody Device device) {
        SysUser sysUser = SecurityUtils.getSysUser();
        device.setRootId(sysUser.getRootId());
        device.setCreateBy(sysUser.getUserId());
        if (deviceService.add(device)) {
            return R.ok();
        } else {
            return R.error();
        }
    }

    @ApiOperation("获取选择列表")
    @GetMapping("/selectList")
    public R selectList() {
        String rootId = SecurityUtils.getSysUser().getRootId();
        List<Device> list = deviceService.getList(rootId);
        List<SelectOption> selectOptions = new ArrayList<>();
        list.forEach(e -> {
            selectOptions.add(new SelectOption(e.getName(), e.getClientId()));
        });
        return R.ok().put("data", selectOptions);
    }

    @ApiOperation("获取设备在线报警数量")
    @GetMapping("/deviceStatusCount")
    public R getDeviceStatusCount() {
        int onlineCount=0,warnCount=0;
        String rootId = SecurityUtils.getSysUser().getRootId();
        List<Device> list = deviceService.getList(rootId);
        list.forEach(UpdateDeviceStatus::updateStatus);
        for (Device device : list) {
            if (DeviceConstants.ONLINE.equals(device.getStatus())) {
                onlineCount++;
            }
            if (DeviceConstants.WARN.equals(device.getWarnStatus())) {
                warnCount++;
            }
        }
        Map<String,Object> map=new HashMap<>();
        map.put("allCount",list.size());
        map.put("onlineCount",onlineCount);
        map.put("warnCount",warnCount);
        return R.ok().put("data",map);
    }

    @ApiOperation("设备列表")
    @GetMapping("/list")
    public TableDataInfo getList() {
        String rootId = SecurityUtils.getSysUser().getRootId();
        startPage();
        List<Device> list = deviceService.getList(rootId);
        list.forEach(UpdateDeviceStatus::updateStatus);
        return getDataTable(list);
    }

    @ApiOperation("更新设备")
    @PutMapping("/update")
    public R update(@RequestBody Device device) {
        if (deviceService.updateData(device)) {
            return R.ok();
        } else {
            return R.error();
        }
    }


    @ApiOperation("删除设备")
    @DeleteMapping("/delete/{clientId}")
    public R delete(@PathVariable("clientId") String clientId) {
        String rootId = SecurityUtils.getSysUser().getRootId();
        if (deviceService.deleteData(clientId)) {
            ddlService.deleteDevice(rootId, clientId);
            return R.ok();
        } else {
            return R.error();
        }
    }

    @ApiOperation("获取模型的设备列表")
    @GetMapping("/listByModel/{modelId}")
    public TableDataInfo getListByModel(@PathVariable("modelId") String modelId) {
        startPage();
        List<Device> list = deviceService.getListByModel(modelId);
        return getDataTable(list);
    }

    @ApiOperation("获取设备详细信息")
    @GetMapping("/info/{clientId}")
    public R getInfo(@PathVariable("clientId") String clientId) {
        Device info = deviceService.getInfo(clientId);
        UpdateDeviceStatus.updateStatus(info);
        return R.ok().put("data", info);
    }

    @ApiOperation("通过设备id获取模型下拉列表")
    @GetMapping("/modelSelectList/{deviceId}")
    public R getModelList(@PathVariable("deviceId") String deviceId) {
        List<SelectOption> selectOptions = new ArrayList<>();
        Device info = deviceService.getInfo(deviceId);
        if (StringUtils.isNotNull(info)) {
            List<ModelData> listByMdoelId = modelDataService.getListByMdoelId(info.getModelId());
            if (StringUtils.isNotNull(listByMdoelId)) {
                listByMdoelId.forEach(e -> {
                    selectOptions.add(new SelectOption(e.getName(), e.getDataId()));
                });
            }
        }
        return R.ok().put("data", selectOptions);
    }
}

