package com.ylink.device.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ylink.device.api.entity.DeviceModel;
import com.ylink.device.mapper.DeviceModelMapper;
import com.ylink.device.service.IDeviceModelService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.service.impl
 * @Author: shuyu
 * @CreateTime: 2022-11-08  17:15
 * @Description:
 * @Version: 1.0
 */
@Service
public class DeviceModelServiceImpl extends ServiceImpl<DeviceModelMapper, DeviceModel> implements IDeviceModelService {

    @Override
    public boolean add(DeviceModel deviceModel) {
        deviceModel.setModelId(IdWorker.getIdStr());
        deviceModel.setCreateTime(new Date());
        return baseMapper.insert(deviceModel) > 0;
    }

    @Override
    public List<DeviceModel> getList(String rootId) {
        return baseMapper.selectList(new QueryWrapper<DeviceModel>().eq("root_id", rootId));
    }

    @Override
    public boolean updateDate(DeviceModel deviceModel) {
        deviceModel.setUpdateTime(new Date());
        return baseMapper.updateById(deviceModel) > 0;
    }

    @Override
    public boolean deleteData(String modelId) {
        return baseMapper.deleteById(modelId) > 0;
    }

    @Override
    public DeviceModel getInfo(String modelId) {
        return baseMapper.selectById(modelId);
    }
}
