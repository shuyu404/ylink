package com.ylink.device.Redis;

import com.ylink.common.core.constant.device.DeviceCacheConstants;
import com.ylink.common.core.constant.device.DeviceConstants;
import com.ylink.common.core.utils.StringUtils;
import com.ylink.common.redis.service.RedisService;
import com.ylink.device.api.entity.Device;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.Redis
 * @Author: shuyu
 * @CreateTime: 2022-11-12  16:35
 * @Description:
 * @Version: 1.0
 */
@Component
public class UpdateDeviceStatus {

    private static RedisService redisService;

    private static RedisTemplate<String, Object> redisTemplate;

    @Autowired
    public void setRedisService(RedisService redisService) {
        UpdateDeviceStatus.redisService = redisService;
    }


    public static void updateStatus(Device device) {
        Map<String, Object> cacheMap = redisService.getCacheObject(DeviceCacheConstants.DEVICE_STATUS + device.getClientId());
        Collection<String> keys = redisService.keys(DeviceCacheConstants.DEVICE_WARN + device.getClientId()+":*");
        if (StringUtils.isNotNull(cacheMap)) {
            device.setStatus((String) cacheMap.get(DeviceConstants.STATUS));
            device.setLastUpdateTime((Date) cacheMap.get(DeviceConstants.LAST_UPDATE_TIME));
            if (keys.size() > 0) {
                device.setWarnStatus(DeviceConstants.WARN);
            } else {
                device.setWarnStatus(DeviceConstants.COMMON);
            }
        }
    }
}
