package com.ylink.device.mapper.scene;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ylink.device.entity.scene.SceneDevice;
import org.apache.ibatis.annotations.Mapper;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.mapper.scene
 * @Author: shuyu
 * @CreateTime: 2022-12-18  11:53
 * @Description:
 * @Version: 1.0
 */
@Mapper
public interface SceneDeviceMapper extends BaseMapper<SceneDevice> {

}
