package com.ylink.device.controller.scene;

import com.ylink.common.core.domain.R;
import com.ylink.common.core.web.controller.BaseController;
import com.ylink.common.core.web.page.TableDataInfo;
import com.ylink.common.core.web.vo.SelectOption;
import com.ylink.common.security.utils.SecurityUtils;
import com.ylink.device.entity.scene.Scene;
import com.ylink.device.entity.scene.SceneConfig;
import com.ylink.device.service.scene.SceneConfigService;
import com.ylink.system.api.entity.SysUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.controller.scene
 * @Author: shuyu
 * @CreateTime: 2022-12-18  16:29
 * @Description:
 * @Version: 1.0
 */
@Api(tags = "组态设计")
@RestController
@RequestMapping("sceneConfig")
public class SceneConfigController extends BaseController {
    @Autowired
    SceneConfigService  configService;

    @ApiOperation("获取组态列表")
    @GetMapping("/list")
    public TableDataInfo getList(){
        String rootId = SecurityUtils.getSysUser().getRootId();
        startPage();
        List<SceneConfig> list = configService.getList(rootId);
        return getDataTable(list);
    }

    @ApiOperation("组态选择列表")
    @GetMapping("/selectList")
    public R selectList(){
        String rootId = SecurityUtils.getSysUser().getRootId();
        List<SceneConfig> list = configService.getList(rootId);
        List<SelectOption> selectOptions=new ArrayList<>();
        list.forEach(e->{
            selectOptions.add(new SelectOption(e.getName(), e.getId()));
        });
        return R.ok().put("data",selectOptions);
    }

    @ApiOperation("添加组态")
    @PostMapping("/add")
    public R add(@RequestBody SceneConfig scene){
        SysUser sysUser = SecurityUtils.getSysUser();
        String rootId = sysUser.getRootId();
        scene.setRootId(rootId);
        scene.setCreateBy(sysUser.getRootId());
        boolean add = configService.add(scene);
        if(add){
            return R.ok();
        }else {
            return R.error();
        }
    }

    @ApiOperation("删除组态")
    @DeleteMapping("/delete/{id}")
    public R delete(@PathVariable("id")String id){
      boolean b=  configService.delete(id);
        if(b){
            return R.ok();
        }else {
            return R.error();
        }
    }

}
