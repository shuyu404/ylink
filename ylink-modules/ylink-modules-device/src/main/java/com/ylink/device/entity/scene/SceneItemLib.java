package com.ylink.device.entity.scene;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.entity.scene
 * @Author: shuyu
 * @CreateTime: 2022-12-20  09:46
 * @Description:    组件库类
 * @Version: 1.0
 */
@Data
public class SceneItemLib {
    @TableId
    String cid;

    /**
     * 组件名称
     */
    String component;

    /**
     * 标签
     */
    String label;

    /**
     * 默认值
     */
    String propValue;

    /**
     * 样式
     */
    String style;

    /**
     * 是否锁定
     */
    Boolean isLock;

    /**
     * 类型id
     */
    String classId;

}
