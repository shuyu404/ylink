package com.ylink.device.service.scene;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ylink.device.entity.scene.Scene;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Alie
 */
public interface SceneService extends IService<Scene> {

    /**
     * 获取场景列表
     * @param rootId 根id
     * @return  场景列表
     */
    List<Scene> getList(String rootId);

    /**
     * 添加
     * @param scene 场景数据
     * @return  执行结果
     */
    boolean add(Scene scene);

    /**
     * 删除
     * @param rootId 根id
     * @param sceneId   场景id
     * @return  执行结果
     */
    boolean delete(String rootId, String sceneId);

    /**
     * 更新
     * @param scene 场景数据
     */
    void update(Scene scene);
}
