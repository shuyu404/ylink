package com.ylink.device.service.scene.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ylink.device.entity.scene.SceneItemLib;
import com.ylink.device.mapper.scene.SceneItemLibMapper;
import com.ylink.device.service.scene.SceneItemLibService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.service.scene.impl
 * @Author: shuyu
 * @CreateTime: 2022-12-20  10:10
 * @Description:
 * @Version: 1.0
 */
@Service
public class SceneItemLibServiceImpl extends ServiceImpl<SceneItemLibMapper, SceneItemLib> implements SceneItemLibService {
    @Override
    public List<SceneItemLib> getList(String classId) {
        return this.baseMapper.selectList(new QueryWrapper<SceneItemLib>().eq("class_id", classId));
    }
}
