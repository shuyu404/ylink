package com.ylink.device.mapper.scene;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ylink.device.entity.scene.SceneItemLib;
import org.apache.ibatis.annotations.Mapper;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.mapper.scene
 * @Author: shuyu
 * @CreateTime: 2022-12-20  09:56
 * @Description:
 * @Version: 1.0
 */
@Mapper
public interface SceneItemLibMapper extends BaseMapper<SceneItemLib> {
}
