package com.ylink.device.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ylink.common.core.utils.StringUtils;
import com.ylink.common.core.utils.uuid.UUID;
import com.ylink.device.api.entity.Device;
import com.ylink.device.mapper.DeviceMapper;
import com.ylink.device.service.IDeviceService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.service.impl
 * @Author: shuyu
 * @CreateTime: 2022-11-08  17:13
 * @Description: 设备服务实现类
 * @Version: 1.0
 */
@Service
public class DeviceServiceImpl extends ServiceImpl<DeviceMapper, Device> implements IDeviceService {
    @Override
    public boolean add(Device device) {
        device.setClientId(IdWorker.getIdStr());
        device.setUsername(UUID.fastUUID().toString());
        device.setPassword(UUID.fastUUID().toString());
        device.setPub(String.format("%s/%s/pub",device.getRootId(),device.getClientId()));
        device.setSub(String.format("%s/%s/sub",device.getRootId(),device.getClientId()));
        device.setCreateTime(new Date());
        return baseMapper.insert(device) > 0;
    }

    @Override
    public List<Device> getList(String rootId) {
        return baseMapper.selectList(new QueryWrapper<Device>().eq("root_id", rootId));
    }

    @Override
    public Device getInfo(String clientId) {
        return baseMapper.selectById(clientId);
    }

    @Override
    public boolean updateData(Device device) {
        if (StringUtils.isNotEmpty(device.getClientId())) {
            device.setUpdateTime(new Date());
            return baseMapper.updateById(device) > 0;
        }
        return false;
    }

    @Override
    public boolean deleteData(String clientId) {
        if (StringUtils.isNotEmpty(clientId)) {
            return baseMapper.deleteById(clientId) > 0;
        }
        return false;
    }

    @Override
    public List<Device> getListByModel(String modelId) {
        return baseMapper.selectList(new QueryWrapper<Device>().eq("model_id", modelId));
    }
}
