package com.ylink.device.controller;

import com.ylink.common.core.domain.R;
import com.ylink.common.core.domain.Rpc;
import com.ylink.common.core.enums.DataType;
import com.ylink.common.core.utils.StringUtils;
import com.ylink.common.core.web.vo.SelectOption;
import com.ylink.common.security.utils.SecurityUtils;
import com.ylink.device.api.domain.DeviceVo;
import com.ylink.device.api.entity.Device;
import com.ylink.device.api.entity.DeviceModel;
import com.ylink.device.api.entity.ModelData;
import com.ylink.device.service.IDeviceModelService;
import com.ylink.device.service.IDeviceService;
import com.ylink.device.service.IModelDataService;
import com.ylink.device.service.iotdb.IotdbDDLService;
import com.ylink.device.vo.ModelDataVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.controller
 * @Author: shuyu
 * @CreateTime: 2022-11-09  12:05
 * @Description:
 * @Version: 1.0
 */
@Api(tags = "数据类型")
@RestController
@RequestMapping("modelData")
public class ModelDataController {

    @Autowired
    IModelDataService modelDataService;
    @Autowired
    IDeviceService deviceService;
    @Autowired
    IDeviceModelService deviceModelService;

    @Autowired
    IotdbDDLService iotdbDDLService;

    @ApiOperation("获取数据类型")
    @GetMapping("types")
    public R getDataTypes() {
        List<SelectOption> options = new ArrayList<>();
        for (DataType value : DataType.values()) {
            options.add(new SelectOption(value.name(), String.valueOf(value.ordinal())));
        }
        return R.ok().put("data", options);
    }

    @ApiOperation("获取模型数据列表")
    @GetMapping("/list/{modelId}")
    public R getMdoelDataList(@PathVariable("modelId") String modelId) {
        List<ModelData> listByMdoelId = modelDataService.getListByMdoelId(modelId);
        List<ModelDataVo> list = new ArrayList<>();

        listByMdoelId.forEach(modelData -> {
            ModelDataVo modelDataVo = new ModelDataVo();
            BeanUtils.copyProperties(modelData, modelDataVo);
            modelDataVo.setType(DataType.getType(modelData.getType()).name());
            list.add(modelDataVo);
        });
        return R.ok().put("data", list);
    }

    @ApiOperation("获取模型数据项详细数据")
    @GetMapping("/info/{modelDataId}")
    public R getModelDataInfo(@PathVariable("modelDataId") String modelDataId){
      ModelData modelData= modelDataService.getModelDataInfo(modelDataId);
      return R.ok().put("data",modelData);
    }

    @ApiOperation("更新模型数据项数据")
    @PutMapping("/update")
    public R update(@RequestBody ModelData modelData){
        String userId = SecurityUtils.getSysUser().getUserId();
        modelData.setType(null);
        modelData.setUpdateBy(userId);
        modelData.setUpdateTime(new Date());
        boolean b= modelDataService.updateInfo(modelData);
        if (b){
            return R.ok();
        }else {
            return R.error();
        }
    }

    @ApiOperation("根据设备id获取数据列表")
    @GetMapping("/deviceVo/{clientId}")
    public Rpc<DeviceVo> getDeviceVo(@PathVariable("clientId") String clientId) {
        Device info = deviceService.getInfo(clientId);
        DeviceVo deviceVo = new DeviceVo();
        if (StringUtils.isNotNull(info)) {
            List<ModelData> listByModelId = modelDataService.getListByMdoelId(info.getModelId());
            DeviceModel deviceModel = deviceModelService.getInfo(info.getModelId());
            deviceVo.setDevice(info);
            deviceVo.setModelDataList(listByModelId);
            deviceVo.setDeviceModel(deviceModel);
            return Rpc.ok(deviceVo);
        }
        return Rpc.ok(deviceVo);
    }

    @ApiOperation("添加模型数据")
    @PostMapping("/add")
    public R add(@RequestBody ModelData modelData) {
        String userId = SecurityUtils.getSysUser().getUserId();
        modelData.setCreateBy(userId);
        System.out.println(modelData);
        if (modelDataService.add(modelData)) {
            return R.ok();
        } else {
            return R.error();
        }
    }

    @ApiOperation("删除模型数据")
    @DeleteMapping("/delete/{dataId}")
    public R delete(@PathVariable("dataId") String dataId) {
        String rootId = SecurityUtils.getSysUser().getRootId();
        if (modelDataService.delete(dataId)) {
            iotdbDDLService.deleteTimeSeries(rootId,dataId);
            return R.ok();
        } else {
            return R.error();
        }
    }

}
