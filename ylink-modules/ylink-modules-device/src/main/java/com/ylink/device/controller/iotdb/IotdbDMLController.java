package com.ylink.device.controller.iotdb;

import com.ylink.common.core.domain.R;
import com.ylink.common.security.utils.SecurityUtils;
import com.ylink.device.entity.iotdb.DataLast;
import com.ylink.device.entity.iotdb.DataSearchVo;
import com.ylink.device.entity.iotdb.DataSeries;
import com.ylink.device.service.iotdb.IotdbDMLService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.controller.iotdb
 * @Author: shuyu
 * @CreateTime: 2022-11-11  16:15
 * @Description: 数据操作类
 * @Version: 1.0
 */
@Api(tags = "iotdb数据操作接口")
@RestController
@RequestMapping("dataDML")
public class IotdbDMLController extends BaseIotdbController{

    @Autowired
    IotdbDMLService dmlService;

    @ApiOperation("获取数据序列")
    @GetMapping("dataSeries")
    public R getDataSeries(){
        DataSearchVo dataSearchVo = new DataSearchVo();
        dataSearchVo.getServletInit();
        dataSearchVo.setRootId(SecurityUtils.getSysUser().getRootId());
        System.out.println(dataSearchVo);
        DataSeries dataSeries = dmlService.getDataSeries(dataSearchVo);;
        return R.ok().put("data",dataSeries);
    }

    @ApiOperation("获取最近数据")
    @GetMapping("getLast/{clientId}")
    public R getLast(@PathVariable("clientId")String clientId){
        String rootId = SecurityUtils.getSysUser().getRootId();
        List<DataLast> lastData = dmlService.getLastData(rootId, clientId);
        return R.ok().put("data",lastData);
    }

    @ApiOperation("获取数据点数量")
    @GetMapping("/dataCount")
    public R getCountAll(){
        String rootId = SecurityUtils.getSysUser().getRootId();
        String path = String.format("root.%s", rootId);
        long count = dmlService.getCount(path);
        return R.ok().put("count",count);
    }

    @ApiOperation("获取报警数量数据序列")
    @GetMapping("/dataSeriesWarn")
    public R getDataSeriesWarn(){
        String rootId = SecurityUtils.getSysUser().getRootId();
        DataSearchVo dataSearchVo = getDataSearchVo(rootId);
        dataSearchVo.setDataId("warn");
        dataSearchVo.setDeviceId("*");
        DataSeries seriesSumCount = dmlService.getSeriesSumCount(dataSearchVo);
        return R.ok().put("data",seriesSumCount);
    }

    @ApiOperation("获取数据数量序列")
    @GetMapping("/dataCountSeries")
    public R getDataCountSeries(){
        String rootId = SecurityUtils.getSysUser().getRootId();
        DataSearchVo dataSearchVo = getDataSearchVo(rootId);
        dataSearchVo.setDataId("*");
        dataSearchVo.setDeviceId("*");
        DataSeries seriesSumCount = dmlService.getSeriesCount(dataSearchVo);
        return R.ok().put("data",seriesSumCount);
    }
}
