package com.ylink.device.service.scene;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ylink.device.entity.scene.Scene;
import com.ylink.device.entity.scene.SceneConfig;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.service.scene
 * @Author: shuyu
 * @CreateTime: 2022-12-18  11:58
 * @Description:
 * @Version: 1.0
 */

public interface SceneConfigService extends IService<SceneConfig> {

    /**
     * 获取列表
     * @param rootId 根id
     * @return  列表
     */
    List<SceneConfig> getList(String rootId);

    /**
     * 添加
     * @param sceneConfig 组态数据
     * @return 是否执行成功
     */
    boolean add(SceneConfig sceneConfig);

    boolean delete(String id);
}
