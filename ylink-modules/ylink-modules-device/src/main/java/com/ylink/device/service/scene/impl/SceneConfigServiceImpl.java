package com.ylink.device.service.scene.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ylink.device.entity.scene.SceneConfig;
import com.ylink.device.mapper.scene.SceneConfigMapper;
import com.ylink.device.service.scene.SceneConfigService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.service.scene.impl
 * @Author: shuyu
 * @CreateTime: 2022-12-18  12:57
 * @Description:
 * @Version: 1.0
 */
@Service
public class SceneConfigServiceImpl extends ServiceImpl<SceneConfigMapper, SceneConfig> implements SceneConfigService {
    @Override
    public List<SceneConfig> getList(String rootId) {
        return this.baseMapper.selectList(new QueryWrapper<SceneConfig>().eq("root_id", rootId));
    }

    @Override
    public boolean add(SceneConfig sceneConfig) {
        sceneConfig.setId(IdWorker.getIdStr());
        sceneConfig.setCreateTime(new Date());
        return this.baseMapper.insert(sceneConfig) > 0;
    }

    @Override
    public boolean delete(String id) {
        return this.baseMapper.deleteById(id) > 0;
    }
}
