package com.ylink.device.service.scene;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ylink.device.entity.scene.SceneItemClass;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Alie
 */

public interface SceneItemClassService extends IService<SceneItemClass> {

    /**获取全部组件类型
     * @return 组件类型列表
     */
    List<SceneItemClass> getAllList();
}
