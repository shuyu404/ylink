package com.ylink.device.mapper.scene;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ylink.device.entity.scene.Scene;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

/**
 * @author Alie
 */
@Mapper
public interface SceneMapper extends BaseMapper<Scene> {
}
