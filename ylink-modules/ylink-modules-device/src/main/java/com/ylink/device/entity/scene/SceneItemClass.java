package com.ylink.device.entity.scene;

import lombok.Data;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.entity.scene
 * @Author: shuyu
 * @CreateTime: 2022-12-20  09:45
 * @Description:
 * @Version: 1.0
 */
@Data
public class SceneItemClass {
    /**
     * 类型id
     */
    String id;

    /**
     * 类型名称
     */
    String name;
}
