package com.ylink.device.service.scene.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ylink.device.entity.scene.SceneItemData;
import com.ylink.device.mapper.scene.SceneItemDataMapper;
import com.ylink.device.service.scene.SceneItemDataService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.service.scene.impl
 * @Author: shuyu
 * @CreateTime: 2022-12-20  10:08
 * @Description:
 * @Version: 1.0
 */
@Service
public class SceneItemDataServiceImpl extends ServiceImpl<SceneItemDataMapper,SceneItemData> implements SceneItemDataService {
    @Override
    public void deleteBoard(String configId) {
        this.baseMapper.delete(new QueryWrapper<SceneItemData>().eq("config_id",configId));
    }

    @Override
    public boolean saveBoard(List<SceneItemData> list) {
        int i= list.stream().mapToInt(sceneItemData -> this.baseMapper.insert(sceneItemData)).sum();
        return i==list.size();
    }

    @Override
    public List<SceneItemData> getBoard(String configId) {
        return this.baseMapper.selectList(new QueryWrapper<SceneItemData>().eq("config_id", configId));
    }
}
