package com.ylink.device.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ylink.device.api.entity.DeviceModel;

import java.util.List;

/**
 * @author shuyu
 */
public interface IDeviceModelService extends IService<DeviceModel> {

    /**
     *
     * 添加数据模型
     * @param deviceModel 数据模型
     * @return 是否执行成功
     */
    boolean add(DeviceModel deviceModel);

    /**
     * 获取模型列表
     * @param rootId    根id
     * @return 模型列表
     */
    List<DeviceModel> getList(String rootId);

    /**
     *
     * 修改数据模型
     * @param deviceModel 数据模型
     * @return 是否执行成功
     */
    boolean updateDate(DeviceModel deviceModel);


    /**
     * 删除模型
     * @param modelId   要删除的id
     * @return  是否执行成功
     */
    boolean deleteData(String modelId);

    /**
     * 获取模型详细新信息
     * @param modelId 模型id
     * @return  模型数据
     */
    DeviceModel getInfo(String modelId);
}
