package com.ylink.device.controller.scene;

import com.ylink.common.core.domain.R;
import com.ylink.common.core.web.controller.BaseController;
import com.ylink.common.core.web.page.TableDataInfo;
import com.ylink.common.core.web.vo.SelectOption;
import com.ylink.device.entity.scene.SceneConfig;
import com.ylink.device.entity.scene.SceneDevice;
import com.ylink.device.service.impl.DeviceServiceImpl;
import com.ylink.device.service.scene.SceneConfigService;
import com.ylink.device.service.scene.SceneDeviceService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.controller.scene
 * @Author: shuyu
 * @CreateTime: 2022-12-18  14:52
 * @Description:
 * @Version: 1.0
 */
@Api(tags = "设备模型")
@RestController
@RequestMapping("sceneDevice")
public class SceneDeviceController extends BaseController {
    @Autowired
    SceneDeviceService deviceService;
    @Autowired
    DeviceServiceImpl deviceServiceImpl;
    @Autowired
    SceneConfigService configService;
    @GetMapping("/list/{sceneId}")
    public TableDataInfo getList(@PathVariable("sceneId") String sceneId) {
        startPage();
        List<SceneDevice> list = deviceService.getList(sceneId);
        list.forEach(e->{
            e.setDeviceName(deviceServiceImpl.getInfo(e.getDeviceId()).getName());
        });
        return getDataTable(list);
    }

    @GetMapping("/selectList/{configId}")
    public R getSelectList(@PathVariable("configId")String configId){
        String sceneId = configService.getById(configId).getSceneId();
        List<SceneDevice> list = deviceService.getList(sceneId);
        List<SelectOption> selectOptions=new ArrayList<>();
        list.forEach(e->{
            selectOptions.add(new SelectOption(e.getName(),e.getDeviceId()));
        });
        return R.ok().put("data",selectOptions);
    }

    @PostMapping("/add")
    public R add(@RequestBody SceneDevice sceneDevice){
        boolean b = deviceService.add(sceneDevice);
        if (b){
            return R.ok("执行成功");
        }else {
            return R.error("执行失败");
        }
    }

    @PutMapping("/update")
    public R update(@RequestBody SceneDevice sceneDevice){
       boolean b=deviceService.update(sceneDevice);
        if (b){
            return R.ok("执行成功");
        }else {
            return R.error("执行失败");
        }
    }

    @DeleteMapping("/delete/{id}")
    public R delete(@PathVariable("id")String id){
       boolean b= deviceService.delete(id);
        if (b){
            return R.ok("执行成功");
        }else {
            return R.error("执行失败");
        }
    }
}
