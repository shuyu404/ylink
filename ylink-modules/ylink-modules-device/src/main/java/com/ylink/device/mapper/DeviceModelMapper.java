package com.ylink.device.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ylink.device.api.entity.DeviceModel;
import org.apache.ibatis.annotations.Mapper;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.mapper
 * @Author: shuyu
 * @CreateTime: 2022-11-08  17:07
 * @Description:
 * @Version: 1.0
 */
@Mapper
public interface DeviceModelMapper extends BaseMapper<DeviceModel> {
}
