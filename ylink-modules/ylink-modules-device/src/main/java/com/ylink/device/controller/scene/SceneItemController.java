package com.ylink.device.controller.scene;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.ylink.common.core.domain.R;
import com.ylink.common.core.web.page.TableDataInfo;
import com.ylink.device.entity.scene.SceneDevice;
import com.ylink.device.entity.scene.SceneItemClass;
import com.ylink.device.entity.scene.SceneItemData;
import com.ylink.device.entity.scene.SceneItemLib;
import com.ylink.device.service.scene.SceneItemClassService;
import com.ylink.device.service.scene.SceneItemDataService;
import com.ylink.device.service.scene.SceneItemLibService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.controller.scene
 * @Author: shuyu
 * @CreateTime: 2022-12-20  10:13
 * @Description:
 * @Version: 1.0
 */
@Api(tags = "组态插件")
@RestController
@RequestMapping("sceneItem")
public class SceneItemController {
    @Autowired
    SceneItemClassService classService;
    @Autowired
    SceneItemDataService dataService;
    @Autowired
    SceneItemLibService libService;

    @ApiOperation("获取全部类型")
    @GetMapping("/class")
    public R getList() {
        List<SceneItemClass> list = classService.getAllList();
        return R.ok().put("data", list);
    }

    @ApiOperation("根据类型获取组件")
    @GetMapping("/lib/{classId}")
    public R getLib(@PathVariable("classId") String classId) {
        List<SceneItemLib> list = libService.getList(classId);
        return R.ok().put("data", list);
    }

    @ApiOperation("取得画布")
    @GetMapping("/getBoard/{configId}")
    public R getBorad(@PathVariable("configId")String configId){
        List<SceneItemData> list= dataService.getBoard(configId);
        return R.ok().put("data",list);
    }

    @ApiOperation("保存画布")
    @PostMapping("/saveBoard/{configId}")
    public R saveBoard(@PathVariable("configId") String configId, @RequestBody List<SceneItemData> list) {
        dataService.deleteBoard(configId);
        list.forEach(e -> {
            e.setId(IdWorker.getIdStr());
            e.setConfigId(configId);
        });
        boolean b = dataService.saveBoard(list);
        if (b) {
            return R.ok();
        } else {
            return R.error();
        }

    }

}
