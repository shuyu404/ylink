package com.ylink.device.controller.iotdb;

import com.ylink.device.service.iotdb.IotdbDDLService;
import io.swagger.annotations.ApiModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.controller.iotdb
 * @Author: shuyu
 * @CreateTime: 2022-11-11  16:14
 * @Description:    数据定义类
 * @Version: 1.0
 */
@ApiModel("iotdb数据定义接口")
@RestController
@RequestMapping("dataDDL")
public class IotdbDDLController {
    @Autowired
    IotdbDDLService iotdbDDLServcie;

}
