package com.ylink.device.service.scene.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ylink.device.entity.scene.SceneDevice;
import com.ylink.device.mapper.scene.SceneDeviceMapper;
import com.ylink.device.service.scene.SceneDeviceService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.service.scene.impl
 * @Author: shuyu
 * @CreateTime: 2022-12-18  12:58
 * @Description:
 * @Version: 1.0
 */
@Service
public class SceneDeviceServiceImpl extends ServiceImpl<SceneDeviceMapper, SceneDevice> implements SceneDeviceService {
    @Override
    public List<SceneDevice> getList(String sceneId) {
        return this.baseMapper.selectList(new QueryWrapper<SceneDevice>().eq("scene_id", sceneId));
    }

    @Override
    public boolean add(SceneDevice sceneDevice) {
        sceneDevice.setCreateTime(new Date());
        return this.baseMapper.insert(sceneDevice)>0;
    }

    @Override
    public boolean update(SceneDevice sceneDevice) {
        return this.updateById(sceneDevice);
    }

    @Override
    public boolean delete(String id) {
        return this.baseMapper.deleteById(id) > 0;
    }
}
