package com.ylink.device.entity.iotdb;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.entity.iotdb
 * @Author: shuyu
 * @CreateTime: 2022-11-11  22:24
 * @Description:
 * @Version: 1.0
 */
@Data
public class DataLast {
    String dataId;

    Object value;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    Date time;

    String type;
}
