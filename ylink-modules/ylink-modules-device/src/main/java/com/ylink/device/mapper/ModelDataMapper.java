package com.ylink.device.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ylink.device.api.entity.ModelData;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author shuyu
 */
@Mapper
public interface ModelDataMapper extends BaseMapper<ModelData> {

}
