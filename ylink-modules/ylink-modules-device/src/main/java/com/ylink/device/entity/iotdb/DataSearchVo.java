package com.ylink.device.entity.iotdb;

import com.ylink.common.core.utils.ServletUtils;
import com.ylink.common.core.utils.StringUtils;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.entity.iotdb
 * @Author: shuyu
 * @CreateTime: 2022-11-11  17:36
 * @Description:    查询参数类
 * @Version: 1.0
 */
@Data
@ApiModel("查询参数类")
public class DataSearchVo {

    /**
     * 根id
     */
    String rootId;

    /**
     * 设备id
     */
    String deviceId;

    /**
     * 数据id
     */
    String dataId;

    /**
     * 开始时间
     */
    Long start;

    /**
     * 结束时间
     */
    Long end;

    /**
     * 从Servlet中获取参数
     */
    public void getServletInit(){
       this.deviceId=ServletUtils.getParameter("deviceId");
       this.dataId=ServletUtils.getParameter("dataId");
       if(StringUtils.isNotNull(ServletUtils.getParameter("start"))){
           this.start= Long.parseLong(ServletUtils.getParameter("start"));
       }
        if(StringUtils.isNotNull(ServletUtils.getParameter("end"))){
            this.end= Long.parseLong(ServletUtils.getParameter("end"));
        }
    }
}
