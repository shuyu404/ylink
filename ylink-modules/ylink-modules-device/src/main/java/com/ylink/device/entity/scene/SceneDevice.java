package com.ylink.device.entity.scene;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.entity.scene
 * @Author: shuyu
 * @CreateTime: 2022-12-18  11:06
 * @Description:    场景内绑定数据
 * @Version: 1.0
 */
@Data
@TableName("scene_device")
public class SceneDevice {
    String id;

    /**
     * 设备id
     */
    String deviceId;

    /**
     * 场景id
     */
    String sceneId;

    /**
     * 数据源名称
     */
    String name;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    Date createTime;

    @TableField(exist = false)
    String deviceName;
}
