package com.ylink.device.entity.scene;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.entity.scene
 * @Author: shuyu
 * @CreateTime: 2022-12-20  09:50
 * @Description:
 * @Version: 1.0
 */
@Data
public class SceneItemData {
    String id;
    /**
     * 组件id
     */
    String cid;

    /**
     * 组件名称
     */
    String component;

    /**
     * 标签
     */
    String label;

    /**
     * 值
     */
    String propValue;

    /**
     * 样式
     */
    String style;

    /**
     * 锁定
     */
    Boolean isLock;

    /**
     * 类id
     */
    String classId;

    /**
     * 场景id
     */
    String configId;

    /**
     * 事件
     */
    String events;
}
