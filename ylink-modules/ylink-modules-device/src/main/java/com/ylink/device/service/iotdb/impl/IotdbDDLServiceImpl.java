package com.ylink.device.service.iotdb.impl;

import com.ylink.device.service.iotdb.IotdbDDLService;
import org.apache.iotdb.rpc.IoTDBConnectionException;
import org.apache.iotdb.rpc.StatementExecutionException;
import org.apache.iotdb.session.pool.SessionPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.service.iotdb.impl
 * @Author: shuyu
 * @CreateTime: 2022-11-11  16:20
 * @Description:
 * @Version: 1.0
 */
@Service
public class IotdbDDLServiceImpl implements IotdbDDLService {
    private static final Logger LOGGER= LoggerFactory.getLogger(IotdbDDLServiceImpl.class);

    @Autowired
    SessionPool sessionPool;

    @Override
    public void deleteTimeSeries(String rootId, String seriesId) {
        String deleteTimeSeriesSql = String.format("delete timeseries root.%s.*.%s", rootId, seriesId);
        try {
            sessionPool.executeNonQueryStatement(deleteTimeSeriesSql);
        } catch (IoTDBConnectionException | StatementExecutionException e) {
           LOGGER.error("Delete timeseries error Case："+e.getMessage());
        }
    }

    @Override
    public void refreshTtl(String rootId, Long ttl) {
        String refreshTtlSql = String.format("set ttl to root.%s %s", rootId, ttl);
        try {
            sessionPool.executeNonQueryStatement(refreshTtlSql);
        } catch (StatementExecutionException | IoTDBConnectionException e) {
            LOGGER.error("Set TTL error Case："+e.getMessage());
        }
    }

    @Override
    public void deleteDevice(String rootId, String deviceId) {
        String deleteDeviceSql = String.format("delete timeseries root.%s.%s",rootId,deviceId);
        try {
            sessionPool.deleteTimeseries(deleteDeviceSql);
        } catch (IoTDBConnectionException | StatementExecutionException e) {
            LOGGER.error("Delete device error Case："+e.getMessage());
        }
    }

}
