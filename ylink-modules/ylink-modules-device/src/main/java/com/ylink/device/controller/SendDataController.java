package com.ylink.device.controller;

import com.ylink.broker.api.RemoteBrokerService;
import com.ylink.broker.api.entity.SendControllerData;
import com.ylink.common.core.domain.R;
import com.ylink.common.core.utils.StringUtils;
import com.ylink.device.api.entity.Device;
import com.ylink.device.api.entity.ModelData;
import com.ylink.device.service.IDeviceService;
import com.ylink.device.service.IModelDataService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.controller
 * @Author: shuyu
 * @CreateTime: 2022-12-17  16:57
 * @Description:
 * @Version: 1.0
 */
@Api(tags = "下发数据")
@RestController
@RequestMapping("sendData")
public class SendDataController {
    @Autowired
    RemoteBrokerService remoteBrokerService;

    @Autowired
    IModelDataService modelDataService;

    @Autowired
    IDeviceService deviceService;

    @GetMapping("/on/{deviceId}/{dataId}")
    public R sendOn(@PathVariable("deviceId")String deviceId,@PathVariable("dataId")String dataId){

        if(StringUtils.isNotEmpty(deviceId)&&StringUtils.isNotEmpty(dataId)){
            Device info = deviceService.getInfo(deviceId);
            ModelData modelDataInfo = modelDataService.getModelDataInfo(dataId);

            if(StringUtils.isNotEmpty(modelDataInfo.getSendOnData())){
                SendControllerData sendControllerData = new SendControllerData();
                sendControllerData.setRootId(info.getRootId());
                sendControllerData.setType(modelDataInfo.getSendType());
                sendControllerData.setData(modelDataInfo.getSendOnData());
                sendControllerData.setDeviceId(deviceId);
                remoteBrokerService.send(sendControllerData);
                System.out.println(sendControllerData);
            }
        }

        return R.ok();
    }

    @GetMapping("/off/{deviceId}/{dataId}")
    public R sendOff(@PathVariable("deviceId")String deviceId,@PathVariable("dataId")String dataId){
        if(StringUtils.isNotEmpty(deviceId)&&StringUtils.isNotEmpty(dataId)){

            Device info = deviceService.getInfo(deviceId);
            ModelData modelDataInfo = modelDataService.getModelDataInfo(dataId);

            if(StringUtils.isNotEmpty(modelDataInfo.getSendOffData())){
                SendControllerData sendControllerData = new SendControllerData();
                sendControllerData.setRootId(info.getRootId());
                sendControllerData.setType(modelDataInfo.getSendType());
                sendControllerData.setData(modelDataInfo.getSendOffData());
                sendControllerData.setDeviceId(deviceId);
                remoteBrokerService.send(sendControllerData);
                System.out.println(sendControllerData);
            }
        }



        return R.ok();
    }
}
