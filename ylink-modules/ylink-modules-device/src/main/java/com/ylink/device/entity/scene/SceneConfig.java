package com.ylink.device.entity.scene;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ylink.common.core.web.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.entity.scene
 * @Author: shuyu
 * @CreateTime: 2022-12-18  11:08
 * @Description:    组态
 * @Version: 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("scene_config")
public class SceneConfig extends BaseEntity {
    String id;

    /**
     * 场景id
     */
    String sceneId;

    /**
     * 组态名称
     */
    String name;

    /**
     * 根id
     */
    String rootId;

}
