package com.ylink.device.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ylink.device.api.entity.ModelData;

import java.util.List;

/**
 * @author shuyu
 */

public interface IModelDataService extends IService<ModelData> {

    /**
     * 添加模型数据
     * @param modelData 模型数据
     * @return  是否执行成功
     */
    boolean add(ModelData modelData);

    /**
     * 删除模型
     * @param dataId 模型id
     * @return  是否执行成功
     */
    boolean delete(String dataId);

    /**
     * 通过modelId获取 模型 数据列表
     * @param modelId   模型id
     * @return  模型数据列表
     */
    List<ModelData> getListByMdoelId(String modelId);

    /**
     * 通过id获取model详细数据
     * @param modelDataId modelId
     * @return  详细数据
     */
    ModelData getModelDataInfo(String modelDataId);

    /**
     * 更新模型数据
     * @param modelData 新模型数据
     * @return  执行结果
     */
    boolean updateInfo(ModelData modelData);
}
