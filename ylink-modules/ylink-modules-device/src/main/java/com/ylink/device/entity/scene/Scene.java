package com.ylink.device.entity.scene;

import com.baomidou.mybatisplus.annotation.TableName;
import com.ylink.common.core.web.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.entity.scene
 * @Author: shuyu
 * @CreateTime: 2022-12-18  11:03
 * @Description:    场景类型
 * @Version: 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("scene")
public class Scene extends BaseEntity {
    String id;

    /**
     * 场景名称
     */
    String name;

    /**
     * 根id
     */
    String rootId;
}
