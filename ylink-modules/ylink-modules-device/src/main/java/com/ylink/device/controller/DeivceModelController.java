package com.ylink.device.controller;


import com.ylink.common.core.domain.R;
import com.ylink.common.core.web.controller.BaseController;
import com.ylink.common.core.web.page.TableDataInfo;
import com.ylink.common.core.web.vo.SelectOption;
import com.ylink.common.security.utils.SecurityUtils;
import com.ylink.device.api.entity.Device;
import com.ylink.device.api.entity.DeviceModel;
import com.ylink.device.service.IDeviceModelService;
import com.ylink.device.service.IDeviceService;
import com.ylink.device.service.IModelDataService;
import com.ylink.device.service.iotdb.IotdbDDLService;
import com.ylink.system.api.entity.SysUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.controller
 * @Author: shuyu
 * @CreateTime: 2022-11-08  17:07
 * @Description:
 * @Version: 1.0
 */
@Api(tags = "设备模型")
@RestController
@RequestMapping("deviceModel")
public class DeivceModelController extends BaseController {

    @Autowired
    IDeviceModelService deviceModelService;
    @Autowired
    IDeviceService deviceService;
    @Autowired
    IModelDataService modelDataService;

    @ApiOperation("添加设备模型")
    @PostMapping("/add")
    public R add(@RequestBody DeviceModel deviceModel) {
        SysUser sysUser = SecurityUtils.getSysUser();
        deviceModel.setRootId(sysUser.getRootId());
        deviceModel.setCreateBy(sysUser.getUserId());
        if (deviceModelService.add(deviceModel)) {
            return R.ok();
        } else {
            return R.error();
        }
    }

    @ApiOperation("获取模型详情")
    @GetMapping("/info/{modelId}")
    public R getInfo(@PathVariable("modelId")String modelId){
        DeviceModel deviceModel =  deviceModelService.getInfo(modelId);
        return R.ok().put("data",deviceModel);
    }

    @ApiOperation("获取模型列表")
    @GetMapping("/list")
    public TableDataInfo getList() {
        String rootId = SecurityUtils.getSysUser().getRootId();
        startPage();
        List<DeviceModel> list = deviceModelService.getList(rootId);
        return getDataTable(list);
    }

    @ApiOperation("更新模型")
    @PutMapping("/update")
    public R update(@RequestBody DeviceModel deviceModel) {
        String userId = SecurityUtils.getSysUser().getUserId();
        deviceModel.setUpdateBy(userId);
        deviceModel.setType(null);
        if (deviceModelService.updateDate(deviceModel)) {
            return R.ok();
        } else {
            return R.error();
        }
    }

    @ApiOperation("删除模型")
    @DeleteMapping("/delete/{modelId}")
    public R delete(@PathVariable("modelId") String modelId) {
        List<Device> listByModel = deviceService.getListByModel(modelId);
        if(listByModel.size()==0){
            for (DeviceModel deviceModel : deviceModelService.getList(modelId)) {
                modelDataService.delete(deviceModel.getModelId());
            }
            if (deviceModelService.deleteData(modelId)) {
                return R.ok();
            } else {
                return R.error("删除失败");
            }
        }else {
            return R.error("模型存在绑定设备无法删除");
        }
    }

    @ApiOperation("获取设备模型选择列表")
    @GetMapping("/options")
    public R getOptions(){
        String rootId = SecurityUtils.getSysUser().getRootId();
        List<SelectOption> options=new ArrayList<>();
        for (DeviceModel deviceModel : deviceModelService.getList(rootId)) {
            options.add(new SelectOption(deviceModel.getName(),deviceModel.getModelId()));
        }
        return R.ok().put("data",options);
    }
}
