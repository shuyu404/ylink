package com.ylink.device.service.iotdb;

import com.ylink.device.entity.iotdb.DataLast;
import com.ylink.device.entity.iotdb.DataSearchVo;
import com.ylink.device.entity.iotdb.DataSeries;
import lombok.Data;

import java.util.List;

public interface IotdbDMLService {

    /**
     * 获取数量统计
     * @param path  路径
     * @return  数量
     */
    public long getCount(String path);


    /** 获取数量和时间序列关系
     * @return 返回时间序列
     */
    public DataSeries getSeriesCount(DataSearchVo dataSearchVo);

    /**获取相加数量和时间序列关系
     * @param dataSearchVo 查询参数
     * @return 结果
     */
    public DataSeries getSeriesSumCount(DataSearchVo dataSearchVo);


    /**
     * 查询数据序列
     * @param dataSearchVo  查询参数
     * @return  查询结果
     */
    public DataSeries getDataSeries(DataSearchVo dataSearchVo);

    /**
     * 查询最近数据
     * @param rootId 根id
     * @param deviceId  设备id
     * @return  返回数据序列列表
     */
    public List<DataLast> getLastData(String rootId, String deviceId);
}
