package com.ylink.device.entity.iotdb;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.entity.iotdb
 * @Author: shuyu
 * @CreateTime: 2022-11-11  17:28
 * @Description:    数据序列
 * @Version: 1.0
 */
@Data
public class DataSeries {
    /**
     * 数据名称
     */
    String name;

    /**
     * 数据类型
     */
    String dataType;

    /**
     * 数据行
     */
    List<Object[]> rows;
    public DataSeries(){
        this.rows=new ArrayList<>();
    }

    /**
     *
     * @param time  事件戳
     * @param value 数据值
     */
    public void addData(Long time,Object value){
        Object[] list={time,value};
        rows.add(list);
    }
}
