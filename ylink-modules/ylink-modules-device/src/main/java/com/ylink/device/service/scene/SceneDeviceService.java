package com.ylink.device.service.scene;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ylink.device.entity.scene.SceneDevice;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.service.scene
 * @Author: shuyu
 * @CreateTime: 2022-12-18  11:57
 * @Description:
 * @Version: 1.0
 */
public interface SceneDeviceService extends IService<SceneDevice> {

    /**
     * 获取列表
     * @param  sceneId 场景id
     * @return  场景设备关系表
     */
    List<SceneDevice> getList(String sceneId);

    boolean add(SceneDevice sceneDevice);

    boolean update(SceneDevice sceneDevice);

    boolean delete(String id);
}
