package com.ylink.device.service.iotdb.impl;

import com.ylink.common.core.utils.StringUtils;
import com.ylink.device.config.IotdbConfig;
import com.ylink.device.entity.iotdb.DataLast;
import com.ylink.device.entity.iotdb.DataSearchVo;
import com.ylink.device.entity.iotdb.DataSeries;
import com.ylink.device.service.iotdb.IotdbDMLService;
import org.apache.iotdb.rpc.IoTDBConnectionException;
import org.apache.iotdb.rpc.StatementExecutionException;
import org.apache.iotdb.session.pool.SessionDataSetWrapper;
import org.apache.iotdb.session.pool.SessionPool;
import org.apache.iotdb.tsfile.read.common.Field;
import org.apache.iotdb.tsfile.read.common.RowRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.service.iotdb.impl
 * @Author: shuyu
 * @CreateTime: 2022-11-11  16:20
 * @Description:
 * @Version: 1.0
 */
@Service
public class IotdbDMLServiceImpl implements IotdbDMLService {

    public static final Logger LOGGER = LoggerFactory.getLogger(IotdbDMLServiceImpl.class);

    @Autowired
    SessionPool sessionPool;

    private SessionDataSetWrapper execQuerySql(String sql) {
        try {
            return sessionPool.executeQueryStatement(sql);
        } catch (IoTDBConnectionException | StatementExecutionException e) {
            LOGGER.error("execQuerySql exec error Case:" + e.getMessage());
        }
        return null;
    }

    @Override
    public long getCount(String path) {
        String getCountSql = String.format("select count(*) from %s group by level=1", path);
        SessionDataSetWrapper sessionDataSetWrapper = execQuerySql(getCountSql);
        if (sessionDataSetWrapper != null) {

            try {
                if (sessionDataSetWrapper.hasNext()) {
                    RowRecord next = sessionDataSetWrapper.next();
                    return next.getFields().get(0).getLongV();
                }
            } catch (IoTDBConnectionException | StatementExecutionException e) {
                LOGGER.error("getCount exec error Case:" + e.getMessage());
            } finally {
                sessionDataSetWrapper.close();
            }
        }
        return 0;
    }

    @Override
    public DataSeries getSeriesCount(DataSearchVo dataSearchVo) {
        long slipTime = getSlip(dataSearchVo);
        String sql = String.format("select count(%s) from root.%s.%s group by ([%s,%s),%ss)",
                dataSearchVo.getDataId(), dataSearchVo.getRootId(), dataSearchVo.getDeviceId(),
                dataSearchVo.getStart(), dataSearchVo.getEnd(), slipTime);
        return getSeriesCount(sql);
    }

    @Override
    public DataSeries getSeriesSumCount(DataSearchVo dataSearchVo) {
        long slipTime = getSlip(dataSearchVo);
        String sql = String.format("select sum(%s) from root.%s.%s group by ([%s,%s),%ss)",
                dataSearchVo.getDataId(), dataSearchVo.getRootId(), dataSearchVo.getDeviceId(),
                dataSearchVo.getStart(), dataSearchVo.getEnd(), slipTime);
        return getSeriesCount(sql);
    }

    private DataSeries getSeriesCount(String sql){
        SessionDataSetWrapper sessionDataSetWrapper = execQuerySql(sql);
        DataSeries dataSeries = new DataSeries();
        if (sessionDataSetWrapper != null) {
            try {
                while (sessionDataSetWrapper.hasNext()) {
                    RowRecord next = sessionDataSetWrapper.next();
                    double sum=0;
                    for (Field field : next.getFields()) {
                        sum+=Double.parseDouble(field.getStringValue()) ;
                    }
                    dataSeries.addData(next.getTimestamp(), sum);
                }
            } catch (IoTDBConnectionException | StatementExecutionException e) {
                LOGGER.error("getSeriesCount error Case:" + e.getMessage());
            } finally {
                sessionDataSetWrapper.close();
            }
        }
        return dataSeries;
    }


    /**
     * 获取数据序列
     *
     * @param dataSearchVo 查询参数
     * @return 数据序列
     */
    @Override
    public DataSeries getDataSeries(DataSearchVo dataSearchVo) {
        long slipTime = getSlip(dataSearchVo);
        String sql;
        if (slipTime == -1) {
            sql = String.format("select %s from root.%s.%s order by time desc  WITHOUT NULL ALL limit " + IotdbConfig.searchSize,
                    dataSearchVo.getDataId(), dataSearchVo.getRootId(), dataSearchVo.getDeviceId());
        } else {
            sql = String.format("select avg(%s) from root.%s.%s group by ([%s,%s),%ss) WITHOUT NULL ALL",
                    dataSearchVo.getDataId(), dataSearchVo.getRootId(), dataSearchVo.getDeviceId(),
                    dataSearchVo.getStart(), dataSearchVo.getEnd(), slipTime);
        }
        SessionDataSetWrapper sessionDataSetWrapper = execQuerySql(sql);
        if (sessionDataSetWrapper != null) {
            DataSeries dataSeries = new DataSeries();
            if (sessionDataSetWrapper.getColumnNames().size() > 1) {
                dataSeries.setDataType(sessionDataSetWrapper.getColumnTypes().get(1));
                dataSeries.setName(sessionDataSetWrapper.getColumnNames().get(0));
            }

            try {
                while (sessionDataSetWrapper.hasNext()) {
                    RowRecord next = sessionDataSetWrapper.next();
                    dataSeries.addData(next.getTimestamp(),String.format("%.2f",Double.valueOf(next.getFields().get(0).getStringValue())) );
                }
            } catch (IoTDBConnectionException | StatementExecutionException e) {
                LOGGER.error("getDataSeries error Case:" + e.getMessage());
            } finally {
                sessionDataSetWrapper.close();
            }
            return dataSeries;
        }
        return null;
    }

    @Override
    public List<DataLast> getLastData(String rootId, String deviceId) {
        String sql = String.format("select last * from root.%s.%s ", rootId, deviceId);
        SessionDataSetWrapper sessionDataSetWrapper = execQuerySql(sql);

        if (StringUtils.isNotNull(sessionDataSetWrapper)) {
            List<DataLast> dataLasts = new ArrayList<>();
            try {

                while (sessionDataSetWrapper.hasNext()) {
                    RowRecord next = sessionDataSetWrapper.next();
                    List<Field> fields = next.getFields();

                    DataLast dataLast = new DataLast();
                    String name = fields.get(0).getStringValue();

                    dataLast.setTime(new Date(next.getTimestamp()));
                    dataLast.setDataId(name.substring(name.lastIndexOf('.') + 1));
                    dataLast.setValue(fields.get(1).getStringValue());
                    dataLast.setType(fields.get(2).getStringValue());
                    dataLasts.add(dataLast);
                }
            } catch (IoTDBConnectionException | StatementExecutionException e) {
                e.printStackTrace();
            } finally {
                sessionDataSetWrapper.close();
            }
            return dataLasts;
        }
        return null;
    }


    /**
     * 获取时间分割
     *
     * @param dataSearchVo 查询参数类
     * @return 返回分割间隔
     */
    private long getSlip(DataSearchVo dataSearchVo) {
        if (dataSearchVo.getStart() == null || dataSearchVo.getEnd() == null) {
            return -1;
        }
        return (dataSearchVo.getEnd() - dataSearchVo.getStart()) / (IotdbConfig.searchSize * 1000L);
    }

}
