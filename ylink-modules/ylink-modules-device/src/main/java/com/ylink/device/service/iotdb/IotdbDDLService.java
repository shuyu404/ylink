package com.ylink.device.service.iotdb;

public interface IotdbDDLService {

    /**
     * 删除时间序列
     * @param rootId    根id
     * @param seriesId  序列id
     */
    void deleteTimeSeries(String rootId,String seriesId);


    /**
     * 刷新或者设置ttl
     * @param rootId    根id
     * @param ttl   时间
     */
    void refreshTtl(String rootId,Long ttl);

    /**
     * 删除设备数据
     * @param rootId    根id
     * @param deviceId  设备id
     */
    void deleteDevice(String rootId,String deviceId);
}
