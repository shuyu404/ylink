package com.ylink.device.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ylink.device.api.entity.Device;

import java.util.List;

/**
 * @author shuyu
 */
public interface IDeviceService extends IService<Device> {

    /**
     * 添加设备
     * @param device 设备数据
     * @return  是否执行成功
     */
    boolean add(Device device);

    /**
     * 获取设备列表
     * @param rootId 根id
     * @return  设备列表
     */
    List<Device> getList(String rootId);

    /**
     * 获取设备详细信息
     * @param clientId  设备id
     * @return  设备信息
     */
    Device getInfo(String clientId);

    /**
     * 更新设备数据
     * @param device 设备数据
     * @return  是否执行成功
     */
    boolean updateData(Device device);

    /**
     * 删除设备数据
     * @param clientId  设备id
     * @return  是否执行成功
     */
    boolean deleteData(String clientId);

    /**
     * 获取模型的设备列表
     * @param modelId  模型id
     * @return  设备列表
     */
    List<Device> getListByModel(String modelId);
}
