package com.ylink.device.service.scene.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ylink.device.entity.scene.SceneItemClass;
import com.ylink.device.mapper.scene.SceneItemClassMapper;
import com.ylink.device.service.scene.SceneItemClassService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.service.scene.impl
 * @Author: shuyu
 * @CreateTime: 2022-12-20  10:04
 * @Description:
 * @Version: 1.0
 */
@Service
public class SceneItemClassServiceImpl extends ServiceImpl<SceneItemClassMapper, SceneItemClass> implements SceneItemClassService {
    @Override
    public List<SceneItemClass> getAllList() {
        return this.baseMapper.selectList(new QueryWrapper<SceneItemClass>().isNotNull("id"));
    }

}
