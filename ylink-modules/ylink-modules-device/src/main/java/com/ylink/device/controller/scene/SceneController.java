package com.ylink.device.controller.scene;

import com.ylink.common.core.domain.R;
import com.ylink.common.core.web.controller.BaseController;
import com.ylink.common.core.web.page.TableDataInfo;
import com.ylink.common.core.web.vo.SelectOption;
import com.ylink.common.security.utils.SecurityUtils;
import com.ylink.device.entity.scene.Scene;
import com.ylink.device.service.scene.SceneService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.controller.scene
 * @Author: shuyu
 * @CreateTime: 2022-12-18  13:20
 * @Description:
 * @Version: 1.0
 */
@Api(tags = "设备模型")
@RestController
@RequestMapping("scene")
public class SceneController extends BaseController {
    @Autowired
    SceneService sceneService;

    @ApiOperation("获取场景列表")
    @GetMapping("/list")
    public TableDataInfo getList(){
        String rootId = SecurityUtils.getSysUser().getRootId();
        startPage();
        List<Scene> list = sceneService.getList(rootId);
        return getDataTable(list);
    }

    @ApiOperation("获取下拉列表")
    @GetMapping("/selectList")
    public R getSelectList(){
        String rootId = SecurityUtils.getSysUser().getRootId();
        List<Scene> list = sceneService.getList(rootId);
        List<SelectOption> selectOptions=new ArrayList<>();
        list.forEach(scene -> {
            selectOptions.add(new SelectOption(scene.getName(),scene.getId()));
        });
        return R.ok().put("data",selectOptions);
    }

    @ApiOperation("添加场景")
    @PostMapping("/add")
    public R add(@RequestBody Scene scene){
        String rootId = SecurityUtils.getSysUser().getRootId();
        scene.setRootId(rootId);
        boolean add = sceneService.add(scene);
        if(add){
            return R.ok();
        }else {
            return R.error();
        }
    }
    @ApiOperation("更新场景")
    @PutMapping("/update")
    public R update(@RequestBody Scene scene){
      sceneService.update(scene);
      return R.ok();
    }

    @ApiOperation("删除场景")
    @DeleteMapping("/delete/{sceneId}")
    public R delete(@PathVariable("sceneId") String sceneId){
        String rootId = SecurityUtils.getSysUser().getRootId();
        boolean b= sceneService.delete(rootId,sceneId);
        if(b){
            return R.ok();
        }else {
            return R.error();
        }
    }


}
