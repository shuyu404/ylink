package com.ylink.device.service.scene;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ylink.device.entity.scene.SceneItemData;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Alie
 */

public interface SceneItemDataService extends IService<SceneItemData> {
    /**
     * 删除组态
     * @param configId 组态id
     */
    void deleteBoard(String configId);

    /**保存画板
     * @param list 组件列表
     * @return 是否执行成功
     */
    boolean saveBoard(List<SceneItemData> list);

    /**
     * 获取画板
     * @param configId 组态id
     * @return 组件列表
     */
    List<SceneItemData> getBoard(String configId);
}
