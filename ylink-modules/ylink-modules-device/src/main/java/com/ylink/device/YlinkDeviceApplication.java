package com.ylink.device;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device
 * @Author: shuyu
 * @CreateTime: 2022-11-08  13:47
 * @Description:
 * @Version: 1.0
 */
@EnableFeignClients(basePackages = {"com.ylink.broker.api"})
@SpringBootApplication(scanBasePackages = {"com.ylink.common.*","com.ylink.device.*","com.ylink.broker.api"})
public class YlinkDeviceApplication {
    public static void main(String[] args) {
        SpringApplication.run(YlinkDeviceApplication.class,args);
    }
}

