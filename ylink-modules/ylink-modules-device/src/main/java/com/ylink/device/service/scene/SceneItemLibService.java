package com.ylink.device.service.scene;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ylink.device.entity.scene.SceneItemLib;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Alie
 */

public interface SceneItemLibService extends IService<SceneItemLib> {

    /**
     * 根据id获取组件列表
     * @param classId 类型id
     * @return  返回组件列表
     */
    List<SceneItemLib> getList(String classId);
}
