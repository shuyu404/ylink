package com.ylink.device.mapper.scene;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ylink.device.entity.scene.SceneConfig;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Alie
 */
@Mapper
public interface SceneConfigMapper extends BaseMapper<SceneConfig> {
}
