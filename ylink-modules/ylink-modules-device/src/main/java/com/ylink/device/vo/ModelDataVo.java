package com.ylink.device.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ylink.common.core.web.domain.BaseEntity;
import lombok.Data;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.entity
 * @Author: shuyu
 * @CreateTime: 2022-11-09  13:44
 * @Description:
 * @Version: 1.0
 */
@Data
public class ModelDataVo extends BaseEntity {

    /**
     * 数据id
     */

    String dataId;

    /**
     * 数据名称
     */
    String name;

    /**
     * 数据标识
     */
    String flag;

    /**
     * 数据类型
     */
    String type;

    /**
     * 模型id
     */
    String modelId;

}
