package com.ylink.device.service.scene.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ylink.device.entity.scene.Scene;
import com.ylink.device.mapper.scene.SceneMapper;
import com.ylink.device.service.scene.SceneService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.service.scene.impl
 * @Author: shuyu
 * @CreateTime: 2022-12-18  13:05
 * @Description:
 * @Version: 1.0
 */
@Service
public class SceneServiceImpl extends ServiceImpl<SceneMapper, Scene> implements SceneService {

    @Override
    public List<Scene> getList(String rootId) {
        return this.baseMapper.selectList(new QueryWrapper<Scene>().eq("root_id", rootId));
    }

    @Override
    public boolean add(Scene scene) {
        String idStr = IdWorker.getIdStr();
        scene.setId(idStr);
        scene.setCreateTime(new Date());
        return baseMapper.insert(scene) >0;
    }

    @Override
    public boolean delete(String rootId, String sceneId) {
        int delete = baseMapper.delete(new QueryWrapper<Scene>().eq("root_id", rootId).eq("id", sceneId));
        return delete>0;
    }

    @Override
    public void update(Scene scene) {
        baseMapper.updateById(scene);
    }


}
