package com.ylink.device.controller.iotdb;

import com.ylink.common.core.utils.ServletUtils;
import com.ylink.device.entity.iotdb.DataSearchVo;

import java.util.Date;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device.controller.iotdb
 * @Author: shuyu
 * @CreateTime: 2022-12-31  17:37
 * @Description:
 * @Version: 1.0
 */

public class BaseIotdbController {
    public static final String DATA_ID="dataId";
    public static final String START="start";
    public static final String END="end";


    DataSearchVo getDataSearchVo(String rootId){
        DataSearchVo dataSearchVo = new DataSearchVo();
        dataSearchVo.setRootId(rootId);
        dataSearchVo.setDataId(ServletUtils.getParameter(DATA_ID));
        dataSearchVo.setStart(Long.parseLong(ServletUtils.getParameter(START)));
        dataSearchVo.setEnd(Long.parseLong(ServletUtils.getParameter(END)));
        return dataSearchVo;
    }
}
