package com.ylink.device;

import com.ylink.device.entity.iotdb.DataSearchVo;
import com.ylink.device.service.iotdb.IotdbDDLService;
import com.ylink.device.service.iotdb.IotdbDMLService;
import org.apache.iotdb.rpc.IoTDBConnectionException;
import org.apache.iotdb.rpc.StatementExecutionException;
import org.apache.iotdb.session.pool.SessionDataSetWrapper;
import org.apache.iotdb.session.pool.SessionPool;
import org.apache.iotdb.tsfile.read.common.RowRecord;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.device
 * @Author: shuyu
 * @CreateTime: 2022-11-09  12:03
 * @Description:
 * @Version: 1.0
 */
@SpringBootTest
public class ApplicationTest {
    @Autowired
    IotdbDMLService iotdbDMLService;

    @Autowired
    IotdbDDLService iotdbDDLServcie;

    @Autowired
    SessionPool sessionPool;

    @Test
    void Test(){
//        DataSearchVo dataSearchVo = new DataSearchVo();
//        dataSearchVo.setDataId("1590692422584213505");
//        dataSearchVo.setRootId("1");
//        dataSearchVo.setDeviceId("1590592578427416577");
//        System.out.println(iotdbDMLService.getDataSeries(dataSearchVo));
  //      System.out.println(iotdbDMLService.getCount("root.1"));
        iotdbDDLServcie.deleteTimeSeries("1","1590692422584213505");
    }

    @Test
    void Test2(){
        try {

            SessionDataSetWrapper sessionDataSetWrapper = sessionPool.executeQueryStatement("select last * from root.1.1590592578427416577");
            System.out.println(sessionDataSetWrapper.getColumnNames());
            System.out.println(sessionDataSetWrapper.getColumnTypes());
            if (sessionDataSetWrapper.hasNext()) {
                RowRecord next = sessionDataSetWrapper.next();
                System.out.println(next.getFields());
            }
        } catch (IoTDBConnectionException | StatementExecutionException e) {
            e.printStackTrace();
        }
    }

    @Test
    void Test3(){
        DataSearchVo dataSearchVo = new DataSearchVo();
        dataSearchVo.setRootId("1");
        dataSearchVo.setDeviceId("1590592578427416577");
        dataSearchVo.setDataId("1590692739883311106");
        System.out.println(iotdbDMLService.getDataSeries(dataSearchVo));

    }
}
