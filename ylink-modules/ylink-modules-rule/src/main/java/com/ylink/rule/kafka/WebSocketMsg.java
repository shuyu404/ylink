package com.ylink.rule.kafka;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.kafka
 * @Author: shuyu
 * @CreateTime: 2022-12-24  18:25
 * @Description:
 * @Version: 1.0
 */
@Data
@AllArgsConstructor
public class WebSocketMsg {
    String type;
    String status;
    Object data;

    Long time;
}
