package com.ylink.rule.linkage;

import com.ylink.broker.api.RemoteBrokerService;
import com.ylink.broker.api.entity.SendControllerData;
import com.ylink.rule.constant.CommonConstant;
import com.ylink.rule.entity.RuleLinkage;
import com.ylink.rule.entity.RuleLinkageDevice;
import com.ylink.rule.service.RuleLinkageDeviceService;
import com.ylink.rule.service.RuleLinkageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.linkage
 * @Author: shuyu
 * @CreateTime: 2022-12-26  15:40
 * @Description:
 * @Version: 1.0
 */
@Component
public class LinkageService {

    static RemoteBrokerService remoteBrokerService;
    static RuleLinkageService ruleLinkageService;
    static RuleLinkageDeviceService linkageDeviceService;

    @Autowired
    public void setRemoteBrokerService(RemoteBrokerService remoteBrokerService) {
        LinkageService.remoteBrokerService = remoteBrokerService;
    }

    @Autowired
    public void setRuleLinkageService(RuleLinkageService ruleLinkageService) {
        LinkageService.ruleLinkageService = ruleLinkageService;
    }

    @Autowired
    public void setLinkageDeviceService(RuleLinkageDeviceService linkageDeviceService) {
        LinkageService.linkageDeviceService = linkageDeviceService;
    }

    public static void handlerLinkage(String triggerId,String status) {
        List<RuleLinkage> listByTriggerId = ruleLinkageService.getListByTriggerId(triggerId);
        if (listByTriggerId.size() > 0) {
            listByTriggerId.forEach(ruleLinkage -> {
                if (CommonConstant.OPEN.equals(ruleLinkage.getStatus())) {
                    List<RuleLinkageDevice> list = linkageDeviceService.getList(ruleLinkage.getId());
                    if (list.size()>0){
                      list.forEach(ruleLinkageDevice -> {
                          if (CommonConstant.WARN.equals(status)){
                              sendControllerData(ruleLinkage.getRootId(),ruleLinkageDevice.getDeviceId(),ruleLinkage.getType(),ruleLinkage.getWarnData());
                          }else if(CommonConstant.COMMON.equals(status)){
                              sendControllerData(ruleLinkage.getRootId(),ruleLinkageDevice.getDeviceId(),ruleLinkage.getType(),ruleLinkage.getCommonData());
                          }
                      });
                    }
                }
            });
        }
    }

    private static void sendControllerData(String rootId,String deviceId,String type,String data){
        SendControllerData sendControllerData = new SendControllerData();
        sendControllerData.setData(data);
        sendControllerData.setType(type);
        sendControllerData.setRootId(rootId);
        sendControllerData.setDeviceId(deviceId);
        remoteBrokerService.send(sendControllerData);
    }
}
