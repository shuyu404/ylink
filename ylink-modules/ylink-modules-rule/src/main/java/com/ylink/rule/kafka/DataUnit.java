package com.ylink.rule.kafka;

import com.ylink.rule.entity.RuleTrigger;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.kafka
 * @Author: shuyu
 * @CreateTime: 2022-12-22  18:33
 * @Description:
 * @Version: 1.0
 */
@Data
@NoArgsConstructor
public class DataUnit {
    String name;

    String type;

    String value;

    RuleTrigger ruleTrigger;

    String dataName;

    String deviceName;
    public DataUnit(String name,String type, String value, String dataName,String deviceName,RuleTrigger ruleTrigger) {
        this.name=name;
        this.type = type;
        this.value = value;
        this.ruleTrigger = ruleTrigger;
        this.dataName=dataName;
        this.deviceName=deviceName;
    }
}
