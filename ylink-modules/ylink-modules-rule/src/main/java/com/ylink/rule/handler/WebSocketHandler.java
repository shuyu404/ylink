package com.ylink.rule.handler;

import com.alibaba.fastjson2.JSON;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelId;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.util.AttributeKey;
import io.netty.util.concurrent.GlobalEventExecutor;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.handler
 * @Author: shuyu
 * @CreateTime: 2022-12-24  15:56
 * @Description:
 * @Version: 1.0
 */
@Component
public class WebSocketHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {
    public static final String ROOT_ID = "root_id";
    private static ChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    private static Map<String, Set<ChannelId>> storeMap = new HashMap<>();

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        channels.add(ctx.channel());
        System.out.println("通道激活");
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        String root = ctx.channel().attr(AttributeKey.valueOf(ROOT_ID)).get().toString();
        Set<ChannelId> channelIds = storeMap.get(root);
        channelIds.remove(ctx.channel().id());
        if (channelIds.size() == 0) {
            storeMap.remove(root);
        }
        channels.remove(ctx.channel());
        System.out.println("通道关闭");
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, TextWebSocketFrame textWebSocketFrame) throws Exception {
        InfoData infoData = JSON.parseObject(textWebSocketFrame.text(), InfoData.class);
        if (infoData != null && infoData.rootId != null) {
            String rootId = infoData.rootId;
            Channel channel = channelHandlerContext.channel();
            channel.attr(AttributeKey.valueOf(ROOT_ID)).set(rootId);
            storeMap.computeIfAbsent(rootId, k -> new HashSet<>());
            storeMap.get(rootId).add(channel.id());
            System.out.println(storeMap);
        }

    }

    public static void sendMsg(String rootId,String msg){
        Set<ChannelId> channelIds = storeMap.get(rootId);
        if(channelIds!=null){
            channelIds.forEach(e->{
                Channel channel = channels.find(e);
                channel.writeAndFlush(new TextWebSocketFrame(msg));
            });
        }
    }

    @Data
    static class InfoData {
        String rootId;
    }
}
