package com.ylink.rule.kafka;

import com.alibaba.fastjson2.JSON;
import com.ylink.rule.cache.service.SessionStoreService;
import com.ylink.rule.cache.service.SubscribeStoreService;
import com.ylink.rule.protocol.Publish;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;

import java.nio.charset.StandardCharsets;
import java.util.Set;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.kafka
 * @Author: shuyu
 * @CreateTime: 2022-12-23  10:28
 * @Description: 发送实时数据
 * @Version: 1.0
 */

public class DataRealTime {
    public static void sendData(String deviceId,DataUnit dataUnit){
        String topicName = String.format("%s/%s/message", deviceId, dataUnit.getName());
        Set<String> clientIds = SubscribeStoreService.searchClientId(topicName);
        if(clientIds!=null&&clientIds.size()>0){
            byte[] bytes = JSON.toJSONString(dataUnit).getBytes(StandardCharsets.UTF_8);
            ByteBuf byteBuf = PooledByteBufAllocator.DEFAULT.directBuffer().writeBytes(bytes);
            Publish.sendMsg(topicName,byteBuf);
            byteBuf.release();
        }
    }
}
