package com.ylink.rule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule
 * @Author: shuyu
 * @CreateTime: 2022-12-21  18:31
 * @Description:
 * @Version: 1.0
 */
@EnableFeignClients(basePackages = {"com.ylink.broker.api"})
@SpringBootApplication(scanBasePackages = {"com.ylink.common.*","com.ylink.rule","com.ylink.broker.api"})
public class YlinkRuleApplication {
    public static void main(String[] args) {
        SpringApplication.run(YlinkRuleApplication.class,args);
    }
}
