package com.ylink.rule.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ylink.rule.entity.RuleLinkage;

import java.util.List;

/**
 * @author Alie
 */
public interface RuleLinkageService extends IService<RuleLinkage> {
    /**
     * 获取根下的联动
     * @param rootId 根id
     * @return 返回控制列表
     */
    List<RuleLinkage> getList(String rootId);

    boolean add(RuleLinkage ruleLinkage);

    /**
     * 删除
     * @param linkageId id
     * @return  结果
     */
    boolean delete(String linkageId);

    /**
     * 获取详细信息
     * @param linkageId id
     * @return 返回
     */
    RuleLinkage getInfo(String linkageId);

    /**
     * 更新数据
     * @param ruleLinkage
     * @return
     */
    boolean update(RuleLinkage ruleLinkage);

    /**
     * 通过触发器获取列表
     * @param triggerId 触发器id
     * @return  返回id
     */
    List<RuleLinkage> getListByTriggerId(String triggerId);
}
