package com.ylink.rule.iotdb;

import org.apache.iotdb.rpc.IoTDBConnectionException;
import org.apache.iotdb.rpc.StatementExecutionException;
import org.apache.iotdb.session.pool.SessionPool;
import org.apache.iotdb.tsfile.file.metadata.enums.TSDataType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.iotdb
 * @Author: shuyu
 * @CreateTime: 2022-12-31  15:32
 * @Description:
 * @Version: 1.0
 */
@Component
public class IotdbSaveWarn {
    static SessionPool sessionPool;
    static List<String> name=new ArrayList<>();
    static List<TSDataType> type=new ArrayList<>();
    static {
        name.add("warn");
        type.add(TSDataType.INT32);
    }

    @Autowired
    void setSessionPool(SessionPool s){
        sessionPool=s;
    }

    public static void save(String rootId,String deviceId,int count){
        String devicePath = String.format("root.%s.%s", rootId, deviceId);
        List<Object> value=new ArrayList<>();
        long time=System.currentTimeMillis();
        value.add(count);

        try {
            sessionPool.insertRecord(devicePath,time,name,type,value);
        } catch (IoTDBConnectionException | StatementExecutionException e) {
            e.printStackTrace();
        }
    }
}
