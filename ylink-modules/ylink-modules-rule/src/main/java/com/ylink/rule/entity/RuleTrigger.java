package com.ylink.rule.entity;

import com.ylink.common.core.web.domain.BaseEntity;
import lombok.Data;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.entity
 * @Author: shuyu
 * @CreateTime: 2022-12-22  10:44
 * @Description: 规则类
 * @Version: 1.0
 */
@Data
public class RuleTrigger extends BaseEntity {

    String id;

    String name;
    /**
     * 规则类型
     */
    Integer type;
    /**
     * 数字a
     */
    Float numberA;
    /**
     * 数字b
     */
    Float numberB;
    /**
     * 报警消息
     */
    String warnMsg;
    /**
     * 正常消息
     */
    String commonMsg;
    /**
     * 报警冷却时间 s
     */
    Long delay;
    /**
     * 根id
     */
    String rootId;
    /**
     * 状态
     */
    String status;
}
