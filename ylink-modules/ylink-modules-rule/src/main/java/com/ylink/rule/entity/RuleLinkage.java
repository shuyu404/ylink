package com.ylink.rule.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.ylink.common.core.web.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.entity
 * @Author: shuyu
 * @CreateTime: 2022-12-26  11:08
 * @Description: 联动控制
 * @Version: 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class RuleLinkage extends BaseEntity {
    String id;

    String name;

    String warnData;

    String commonData;

    String type;

    String rootId;

    String triggerId;

    String status;

    @TableField(exist = false)
    List<String> deviceList;
}
