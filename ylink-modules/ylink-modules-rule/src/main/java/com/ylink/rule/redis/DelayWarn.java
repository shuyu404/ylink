package com.ylink.rule.redis;

import com.ylink.common.core.constant.device.DeviceCacheConstants;
import com.ylink.common.redis.service.RedisService;
import org.apache.kafka.common.protocol.types.Field;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.cache.RedisCache;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.redis
 * @Author: shuyu
 * @CreateTime: 2022-12-26  15:03
 * @Description:
 * @Version: 1.0
 */
@Component
public class DelayWarn {

    static RedisService redisService;

    @Autowired
    void setRedisService(RedisService service) {
        redisService = service;
    }


    /**
     * 是否存在key
     */
    public static boolean hasKey(String deviceId, String dataId) {
        return redisService.hasKey(DeviceCacheConstants.DEVICE_WARN + deviceId + ":" + dataId);
    }

    /**
     * 添加key
     */
    public static void addKey(String deviceId, String dataId, long delay) {
        delay*=60;
        if (delay == 0) {
            delay = 1;
        }
        redisService.setCacheObject(DeviceCacheConstants.DEVICE_WARN + deviceId + ":" + dataId, 1, delay, TimeUnit.SECONDS);
    }

    /**
     * 移除key
     */
    public static void removeKey(String deviceId, String dataId) {
        redisService.deleteObject(DeviceCacheConstants.DEVICE_WARN + deviceId + ":" + dataId);
    }

}
