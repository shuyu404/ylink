package com.ylink.rule.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.entity
 * @Author: shuyu
 * @CreateTime: 2022-12-26  11:11
 * @Description:
 * @Version: 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RuleLinkageDevice {

    String deviceId;

    String linkageId;

}
