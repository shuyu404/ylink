package com.ylink.rule.entity;

import lombok.Data;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.entity
 * @Author: shuyu
 * @CreateTime: 2022-12-22  14:23
 * @Description: 设备与触发器的绑定
 * @Version: 1.0
 */
@Data
public class RuleDevice {

    /**
     * 设备id
     */
    String deviceId;

    /**
     * 数据id
     */
    String dataId;

    /**
     * 触发器id
     */
    String triggerId;
}
