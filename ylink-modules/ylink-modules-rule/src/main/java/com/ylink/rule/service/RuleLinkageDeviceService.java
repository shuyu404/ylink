package com.ylink.rule.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ylink.rule.entity.RuleLinkageDevice;

import java.util.List;

/**
 * @author Alie
 */
public interface RuleLinkageDeviceService extends IService<RuleLinkageDevice> {
    /**
     * @param ruleLinkageDevices 添加列表
     */
    void addList(List<RuleLinkageDevice> ruleLinkageDevices);

    /**
     *
     * @param linkageId 删除的id
     */
    void delete(String linkageId);

    /**
     * 获取设备与联动关系
     * @param linkageId id
     * @return  关系组
     */
    List<RuleLinkageDevice> getList(String linkageId);
}
