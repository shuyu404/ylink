package com.ylink.rule.protocol;

import io.netty.channel.Channel;
import io.netty.handler.codec.mqtt.MqttMessage;
import org.springframework.stereotype.Component;

/**
 * @BelongsProject: ylinkMqtt
 * @BelongsPackage: com.example.ylinkmqtt.protocol
 * @Author: shuyu
 * @CreateTime: 2022-11-06  22:26
 * @Description:
 * @Version: 1.0
 */
@Component
public class DisConnect {
    public void disConnect(Channel channel, MqttMessage message){
        channel.close();
    }
}
