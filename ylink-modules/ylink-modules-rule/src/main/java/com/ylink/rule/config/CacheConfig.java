package com.ylink.rule.config;

import org.springframework.boot.autoconfigure.cache.CacheProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.config
 * @Author: shuyu
 * @CreateTime: 2022-12-22  17:00
 * @Description:
 * @Version: 1.0
 */
@EnableConfigurationProperties(CacheProperties.class)
@Configuration
public class CacheConfig {
    @Bean
    RedisCacheConfiguration redisCacheConfiguration(CacheProperties cacheProperties){
        RedisCacheConfiguration config = RedisCacheConfiguration.defaultCacheConfig();
        //config = config.entryTtl();

        config=config.serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(new StringRedisSerializer()));

        GenericJackson2JsonRedisSerializer genericJackson2JsonRedisSerializer=new GenericJackson2JsonRedisSerializer();

        config=config.serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(genericJackson2JsonRedisSerializer));

        //将配置文件生效
        CacheProperties.Redis redis = cacheProperties.getRedis();
        if(redis.getTimeToLive()!=null){
            config=config.entryTtl(redis.getTimeToLive());
        }
        if(!redis.isCacheNullValues()){
            config=config.disableCachingNullValues();
        }
        if(!redis.isUseKeyPrefix()){
            config=config.disableKeyPrefix();
        }
        return config;
    }
}
