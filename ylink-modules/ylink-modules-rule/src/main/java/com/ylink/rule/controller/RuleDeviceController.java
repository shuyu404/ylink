package com.ylink.rule.controller;

import com.ylink.common.core.domain.R;
import com.ylink.rule.entity.RuleDevice;
import com.ylink.rule.service.RuleDeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.controller
 * @Author: shuyu
 * @CreateTime: 2022-12-22  14:32
 * @Description:
 * @Version: 1.0
 */
@RestController
@RequestMapping("/ruleDevice")
public class RuleDeviceController {

    @Autowired
    RuleDeviceService deviceService;

    @GetMapping("/info/{deviceId}/{dataId}")
    public R getInfo(@PathVariable("deviceId") String deviceId, @PathVariable("dataId") String dataId) {
        RuleDevice ruleDevice = deviceService.getByDeviceAndDataId(deviceId, dataId);
        return R.ok().put("data", ruleDevice);
    }

    @PostMapping("/add")
    public R add(@RequestBody RuleDevice ruleDevice) {
        boolean b = deviceService.add(ruleDevice);
        if(b){
            return R.ok();
        }else {
            return R.error();
        }
    }

}
