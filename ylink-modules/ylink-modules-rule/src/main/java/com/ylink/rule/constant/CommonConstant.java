package com.ylink.rule.constant;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.constant
 * @Author: shuyu
 * @CreateTime: 2022-12-26  16:16
 * @Description:
 * @Version: 1.0
 */

public class CommonConstant {
    public static final String OPEN="0";
    public static final String CLOSE="1";

    public static final String WARN="warn";
    public static final String COMMON="common";
}
