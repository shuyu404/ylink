package com.ylink.rule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ylink.rule.entity.RuleLinkage;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Alie
 */
@Mapper
public interface RuleLinkageMapper extends BaseMapper<RuleLinkage> {
}
