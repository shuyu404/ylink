package com.ylink.rule.protocol;


import com.ylink.rule.cache.entity.SessionStore;
import com.ylink.rule.cache.service.ChannelService;
import com.ylink.rule.cache.service.SessionStoreService;
import com.ylink.rule.config.BrokerConfig;
import com.ylink.rule.constant.SessionConstant;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.handler.codec.mqtt.*;
import io.netty.handler.timeout.IdleStateHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @BelongsProject: ylinkMqtt
 * @BelongsPackage: com.example.ylinkmqtt.protocol
 * @Author: shuyu
 * @CreateTime: 2022-11-06  21:16
 * @Description: 连接指令
 * @Version: 1.0
 */
@Component
public class Connect {

    private static final Logger LOGGER = LoggerFactory.getLogger(Connect.class);

    public void connect(Channel channel, MqttConnectMessage message) {

        String clientId = message.payload().clientIdentifier();
        if (clientId == null) {
            MqttConnAckMessage connAckMessage = (MqttConnAckMessage) MqttMessageFactory.newMessage(
                    new MqttFixedHeader(MqttMessageType.CONNACK, false, MqttQoS.AT_MOST_ONCE, false, 0),
                    new MqttConnAckVariableHeader(MqttConnectReturnCode.CONNECTION_REFUSED_IDENTIFIER_REJECTED, false), null);
            channel.writeAndFlush(connAckMessage);
            channel.close();
            return;
        }


        if (BrokerConfig.isAuth) {

        }

        //session初始化
        SessionStore sessionStore = new SessionStore();
        sessionStore.setClientId(clientId);
        SessionStoreService.initSession(channel, sessionStore);


        // 处理连接心跳检测
        int expire = 0;
        if (message.variableHeader().keepAliveTimeSeconds() > 0) {
            if (channel.pipeline().names().contains(SessionConstant.IDLE)) {
                channel.pipeline().remove(SessionConstant.IDLE);
            }
            expire = Math.round(message.variableHeader().keepAliveTimeSeconds() * 1.5f);
            channel.pipeline().addFirst("idle", new IdleStateHandler(0, 0, expire));
        }

        //处理遗嘱消息
        if (message.variableHeader().isWillFlag()) {
            MqttPublishMessage willMessage = (MqttPublishMessage) MqttMessageFactory.newMessage(
                    new MqttFixedHeader(MqttMessageType.PUBLISH, false, MqttQoS.valueOf(message.variableHeader().willQos()), message.variableHeader().isWillRetain(), 0),
                    new MqttPublishVariableHeader(message.payload().willTopic(), 0), Unpooled.buffer().writeBytes(message.payload().willMessageInBytes()));
            sessionStore.setWillMassage(willMessage);
        }

        //连接通道初始化
        ChannelService.channelInit(channel, sessionStore);

        MqttConnAckMessage okResp = (MqttConnAckMessage) MqttMessageFactory.newMessage(
                new MqttFixedHeader(MqttMessageType.CONNACK, false, MqttQoS.AT_MOST_ONCE, false, 0),
                new MqttConnAckVariableHeader(MqttConnectReturnCode.CONNECTION_ACCEPTED, false), null);
        channel.writeAndFlush(okResp);
    }
}
