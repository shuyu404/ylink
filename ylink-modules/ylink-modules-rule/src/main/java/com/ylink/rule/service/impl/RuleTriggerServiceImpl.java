package com.ylink.rule.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ylink.rule.constant.CacheConstant;
import com.ylink.rule.entity.RuleTrigger;
import com.ylink.rule.mapper.RuleTriggerMapper;
import com.ylink.rule.service.RuleDeviceService;
import com.ylink.rule.service.RuleTriggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.service.impl
 * @Author: shuyu
 * @CreateTime: 2022-12-22  10:55
 * @Description:
 * @Version: 1.0
 */
@Service
public class RuleTriggerServiceImpl extends ServiceImpl<RuleTriggerMapper, RuleTrigger> implements RuleTriggerService {
    static final String OPEN ="0";
    static final String CLOSE ="1";
    @Autowired
    RuleDeviceService deviceService;

    @Override
    public List<RuleTrigger> getList(String rootId) {
        return this.baseMapper.selectList(new QueryWrapper<RuleTrigger>().eq("root_id", rootId));
    }

    @Override
    public boolean add(RuleTrigger ruleTrigger) {
        ruleTrigger.setStatus(OPEN);
        ruleTrigger.setCreateTime(new Date());
        return baseMapper.insert(ruleTrigger) > 0;
    }

    @CacheEvict(value = CacheConstant.RULE_TRIGGER,key = "#root.args[0]")
    @Override
    public boolean delete(String triggerId) {
        deviceService.deleteByTriggerId(triggerId);
        return this.baseMapper.deleteById(triggerId) > 0;
    }

    @CacheEvict(value = CacheConstant.RULE_TRIGGER,key = "#root.args[0].id")
    @Override
    public boolean update(RuleTrigger ruleTrigger) {
        ruleTrigger.setUpdateTime(new Date());
        return this.baseMapper.updateById(ruleTrigger) > 0;
    }

    @Cacheable(value = CacheConstant.RULE_TRIGGER,key = "#root.args[0]")
    @Override
    public RuleTrigger getInfo(String id) {
        return this.baseMapper.selectById(id);
    }
}
