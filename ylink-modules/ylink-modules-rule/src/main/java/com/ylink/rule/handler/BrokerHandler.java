package com.ylink.rule.handler;



import com.ylink.rule.cache.service.ChannelService;
import com.ylink.rule.protocol.ProtocolProcess;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.mqtt.*;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.AttributeKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @BelongsProject: ylinkMqtt
 * @BelongsPackage: com.example.ylinkmqtt.handler
 * @Author: shuyu
 * @CreateTime: 2022-11-06  14:43
 * @Description:
 * @Version: 1.0
 */
@Component
@ChannelHandler.Sharable
public class BrokerHandler extends SimpleChannelInboundHandler<MqttMessage> {
    private static final Logger LOGGER = LoggerFactory.getLogger(BrokerHandler.class);

    @Autowired
    ProtocolProcess protocolProcess;

    @Override
    protected void channelRead0(ChannelHandlerContext cxt, MqttMessage message) throws Exception {

        //解码失败
        if (message.decoderResult().isFailure()) {
            Throwable cause = message.decoderResult().cause();
            if (cause instanceof MqttUnacceptableProtocolVersionException) {

                cxt.writeAndFlush(MqttMessageFactory.newMessage(
                        new MqttFixedHeader(MqttMessageType.CONNACK, false, MqttQoS.AT_MOST_ONCE, false, 0),
                        new MqttConnAckVariableHeader(MqttConnectReturnCode.CONNECTION_REFUSED_UNACCEPTABLE_PROTOCOL_VERSION, false), null));

            } else if (cause instanceof MqttIdentifierRejectedException) {
                cxt.writeAndFlush(MqttMessageFactory.newMessage(
                        new MqttFixedHeader(MqttMessageType.CONNACK, false, MqttQoS.AT_MOST_ONCE, false, 0),
                        new MqttConnAckVariableHeader(MqttConnectReturnCode.CONNECTION_REFUSED_IDENTIFIER_REJECTED, false), null));
            }
            cxt.close();
        }

        //判断消息类型进行操作
        switch (message.fixedHeader().messageType()){
            case CONNECT:
                protocolProcess.connect().connect(cxt.channel(),(MqttConnectMessage) message);
                break;
            case CONNACK:
                break;
//            case PUBLISH:
//                protocolProcess.publish().publish(cxt.channel(), (MqttPublishMessage) message);
//                break;
            case PUBACK:
                break;
            case PUBREC:
                break;
            case PUBCOMP:
                break;
            case PUBREL:
                break;
            case SUBSCRIBE:
                protocolProcess.subscribe().subscribe(cxt.channel(),(MqttSubscribeMessage)message);
                break;
            case SUBACK:
                break;
            case UNSUBSCRIBE:
                protocolProcess.unSubscribe().unSubscribe(cxt.channel(),(MqttUnsubscribeMessage)message);
                break;
            case UNSUBACK:
                break;
            case PINGREQ:
                protocolProcess.pingReq().pingReq(cxt.channel(),message);
                break;
            case PINGRESP:
                break;
            case DISCONNECT:
                protocolProcess.disConnectl().disConnect(cxt.channel(),message);
                break;
            case AUTH:
                break;
            default:
                break;

        }

    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        //通达激活添加到通道缓存
        super.channelActive(ctx);
        ChannelService.addChannel(ctx.channel());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        //通道不活跃关闭通道
        super.channelInactive(ctx);
        ChannelService.remove(ctx.channel());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        if (cause instanceof IOException) {
            // 远程主机强迫关闭了一个现有的连接的异常
            ctx.close();
        } else {
            super.exceptionCaught(ctx, cause);
        }
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent idleStateEvent = (IdleStateEvent) evt;
            if (idleStateEvent.state() == IdleState.ALL_IDLE) {
                Channel channel = ctx.channel();
                String clientId = (String) channel.attr(AttributeKey.valueOf("clientId")).get();
            }
            LOGGER.info("连接超时断开连接");
            ctx.close();
        } else {
            super.userEventTriggered(ctx, evt);
        }
    }
}
