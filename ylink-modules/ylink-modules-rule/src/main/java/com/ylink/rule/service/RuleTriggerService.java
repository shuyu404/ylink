package com.ylink.rule.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ylink.rule.entity.RuleTrigger;

import java.util.List;

/**
 * @author Alie
 */
public interface RuleTriggerService extends IService<RuleTrigger> {
    /**
     * 获取列表
     * @param rootId 根id
     * @return  规则列表
     */
    List<RuleTrigger> getList(String rootId);

    /**
     * 添加规则列表
     * @param ruleTrigger
     * @return
     */
    boolean add(RuleTrigger ruleTrigger);

    /**
     * 删除触发器
     * @param triggerId
     * @return
     */
    boolean delete(String triggerId);

    /**
     * 更新
     * @param ruleTrigger 规则
     * @return  是否执行成功
     */
    boolean update(RuleTrigger ruleTrigger);

    /**获取触发器详情
     * @param id id
     * @return 触发器
     */
    RuleTrigger getInfo(String id);
}
