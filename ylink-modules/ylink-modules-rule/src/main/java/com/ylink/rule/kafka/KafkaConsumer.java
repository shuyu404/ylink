package com.ylink.rule.kafka;

import com.alibaba.fastjson2.JSON;
import com.ylink.common.core.constant.kafka.KafkaConstants;
import com.ylink.common.core.utils.StringUtils;
import com.ylink.rule.constant.CommonConstant;
import com.ylink.rule.entity.RuleDevice;
import com.ylink.rule.entity.RuleTrigger;
import com.ylink.rule.handler.WebSocketHandler;
import com.ylink.rule.iotdb.IotdbSaveWarn;
import com.ylink.rule.linkage.LinkageService;
import com.ylink.rule.redis.DelayWarn;
import com.ylink.rule.service.RuleDeviceService;
import com.ylink.rule.service.RuleTriggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.kafka
 * @Author: shuyu
 * @CreateTime: 2022-12-21  18:45
 * @Description:
 * @Version: 1.0
 */
@Service
public class KafkaConsumer {
    @Autowired
    RuleDeviceService deviceService;
    @Autowired
    RuleTriggerService triggerService;
    static final String OPEN = "0";

    @KafkaListener(topics = KafkaConstants.TO_RULE_MESSAGE)
    public void consumerTopic(String msg) {
        DataSave dataSave = JSON.parseObject(msg, DataSave.class);
        String clientId = dataSave.getClientId();
        int count=0;
        for (int i = 0; i < dataSave.getNames().size(); i++) {
            String dataId = dataSave.getNames().get(i);
            DataUnit dataUnit = new DataUnit(dataId,dataSave.getTypes().get(i), dataSave.getValues().get(i),dataSave.getDataNames().get(i),dataSave.getDeviceName(), null);
            DataRealTime.sendData(clientId,dataUnit);
            RuleDevice byDeviceAndDataId = deviceService.getByDeviceAndDataId(clientId, dataId);
            //如果未绑定则为空
            if (StringUtils.isNotNull(byDeviceAndDataId)) {
                if (StringUtils.isNotEmpty(byDeviceAndDataId.getTriggerId())) {
                    RuleTrigger info = triggerService.getInfo(byDeviceAndDataId.getTriggerId());
                    //如果触发器关闭则不执行
                    if (StringUtils.isNotNull(info) && OPEN.equals(info.getStatus())) {
                        dataUnit.setRuleTrigger(info);
                        if (HandlerData.handlerData(dataUnit)) {
                            count++;
                            if(!DelayWarn.hasKey(clientId, dataId)){
                                System.out.println("warning");
                                DelayWarn.addKey(clientId,dataId,info.getDelay());
                                WebSocketHandler.sendMsg(dataSave.rootId,JSON.toJSONString(new WebSocketMsg("message","warn",dataUnit,dataSave.time)));
                                LinkageService.handlerLinkage(info.getId(), CommonConstant.WARN);
                            }
                        }else {
                            if (DelayWarn.hasKey(clientId,dataId)){
                                System.out.println("common");
                                DelayWarn.removeKey(clientId,dataId);
                                WebSocketHandler.sendMsg(dataSave.rootId,JSON.toJSONString(new WebSocketMsg("message","common",dataUnit,dataSave.time)));
                                LinkageService.handlerLinkage(info.getId(), CommonConstant.COMMON);
                            }
                        }
                    }
                }
            }
        }

        //如果报警大于0写入数据库记录
        if (count>0){
            IotdbSaveWarn.save(dataSave.getRootId(), dataSave.getClientId(),count);
        }
    }

}
