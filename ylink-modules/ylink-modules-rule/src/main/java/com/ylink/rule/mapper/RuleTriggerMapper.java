package com.ylink.rule.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ylink.rule.entity.RuleTrigger;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author Alie
 */
@Mapper
public interface RuleTriggerMapper extends BaseMapper<RuleTrigger> {
}
