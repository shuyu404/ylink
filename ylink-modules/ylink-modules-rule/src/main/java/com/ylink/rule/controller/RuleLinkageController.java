package com.ylink.rule.controller;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.ylink.common.core.domain.R;
import com.ylink.common.core.web.controller.BaseController;
import com.ylink.common.core.web.page.TableDataInfo;
import com.ylink.common.security.utils.SecurityUtils;
import com.ylink.rule.entity.RuleLinkage;
import com.ylink.rule.entity.RuleLinkageDevice;
import com.ylink.rule.service.RuleLinkageDeviceService;
import com.ylink.rule.service.RuleLinkageService;
import com.ylink.system.api.entity.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.controller
 * @Author: shuyu
 * @CreateTime: 2022-12-26  11:21
 * @Description:
 * @Version: 1.0
 */
@RestController
@RequestMapping("/ruleLinkage")
public class RuleLinkageController extends BaseController {
    @Autowired
    RuleLinkageService linkageService;
    @Autowired
    RuleLinkageDeviceService linkageDeviceService;

    @GetMapping("/list")
    public TableDataInfo getList() {
        String rootId = SecurityUtils.getSysUser().getRootId();
        startPage();
        List<RuleLinkage> list = linkageService.getList(rootId);
        return getDataTable(list);
    }

    @GetMapping("/info/{linkageId}")
    public R getInfo(@PathVariable("linkageId") String linkageId) {
        RuleLinkage ruleLinkage=linkageService.getInfo(linkageId);
        List<RuleLinkageDevice> list=linkageDeviceService.getList(linkageId);
        List<String> deviceList=new ArrayList<>();
        list.forEach(e->{
            deviceList.add(e.getDeviceId());
        });
        ruleLinkage.setDeviceList(deviceList);
        return R.ok().put("data",ruleLinkage);
    }

    @PostMapping("/add")
    @Transactional
    public R add(@RequestBody RuleLinkage ruleLinkage) {
        SysUser sysUser = SecurityUtils.getSysUser();
        ruleLinkage.setRootId(sysUser.getRootId());
        ruleLinkage.setCreateBy(sysUser.getUserId());
        ruleLinkage.setId(IdWorker.getIdStr());
        List<RuleLinkageDevice> ruleLinkageDevices = new ArrayList<>();
        for (String s : ruleLinkage.getDeviceList()) {
            ruleLinkageDevices.add(new RuleLinkageDevice(s, ruleLinkage.getId()));
        }
        linkageDeviceService.addList(ruleLinkageDevices);
        boolean b = linkageService.add(ruleLinkage);
        if (b) {
            return R.ok();
        } else {
            return R.error();
        }
    }

    @PutMapping("/update")
    public R update(@RequestBody RuleLinkage ruleLinkage){
        String userId = SecurityUtils.getSysUser().getUserId();
        ruleLinkage.setUpdateBy(userId);
        List<RuleLinkageDevice> ruleLinkageDevices = new ArrayList<>();
        for (String s : ruleLinkage.getDeviceList()) {
            ruleLinkageDevices.add(new RuleLinkageDevice(s, ruleLinkage.getId()));
        }
        if(ruleLinkage.getDeviceList().size()==0){
            linkageDeviceService.delete(ruleLinkage.getId());
        }
        linkageDeviceService.addList(ruleLinkageDevices);
        boolean b= linkageService.update(ruleLinkage);
        if(b){
            return R.ok();
        }else {
            return R.error();
        }
    }

    @PutMapping("/changeStatus")
    public R changeStatus(@RequestBody RuleLinkage ruleLinkage){
        boolean b= linkageService.update(ruleLinkage);
        if(b){
            return R.ok();
        }else {
            return R.error();
        }
    }

    @DeleteMapping("/delete/{linkageId}")
    public R delete(@PathVariable("linkageId")String linkageId){
        linkageDeviceService.delete(linkageId);
        boolean b=linkageService.delete(linkageId);
        if(b){
            return R.ok();
        }else {
            return R.error();
        }
    }

}
