package com.ylink.rule.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ylink.rule.constant.CacheConstant;
import com.ylink.rule.entity.RuleDevice;
import com.ylink.rule.mapper.RuleDeviceMapper;
import com.ylink.rule.service.RuleDeviceService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.service.impl
 * @Author: shuyu
 * @CreateTime: 2022-12-22  14:28
 * @Description:
 * @Version: 1.0
 */
@Service
public class RuleDeviceServiceImpl extends ServiceImpl<RuleDeviceMapper, RuleDevice> implements RuleDeviceService {
    static final String NONE="none";

    @Cacheable(value = CacheConstant.RULE_DEVICE_DATA,key = "#root.args[0]+'_'+#root.args[1]")
    @Override
    public RuleDevice getByDeviceAndDataId(String deviceId, String dataId) {
        return this.baseMapper.selectOne(new QueryWrapper<RuleDevice>().eq("device_id", deviceId).eq("data_id", dataId));
    }

    @CacheEvict(value = CacheConstant.RULE_DEVICE_DATA,key = "#root.args[0].deviceId+'_'+#root.args[0].dataId")
    @Override
    public boolean add(RuleDevice ruleDevice) {
        if(NONE.equals(ruleDevice.getTriggerId())){
            return delete(ruleDevice.getDeviceId(),ruleDevice.getDataId());
        }
        RuleDevice byDeviceAndDataId = getByDeviceAndDataId(ruleDevice.getDeviceId(), ruleDevice.getDataId());
        if(byDeviceAndDataId!=null){
          return  this.baseMapper.update(ruleDevice,new QueryWrapper<RuleDevice>().eq("device_id", ruleDevice.getDeviceId()).eq("data_id", ruleDevice.getDataId()))>0;
        }
        return this.baseMapper.insert(ruleDevice)>0;
    }

    @CacheEvict(value = CacheConstant.RULE_DEVICE_DATA,key = "#root.args[0]+'_'+#root.args[1]")
    @Override
    public boolean delete(String deviceId, String dataId) {
        return this.baseMapper.delete(new QueryWrapper<RuleDevice>().eq("device_id", deviceId).eq("data_id", dataId))>0;
    }

    @Override
    public void deleteByTriggerId(String triggerId) {
        this.baseMapper.delete(new QueryWrapper<RuleDevice>().eq("trigger_id",triggerId));
    }
}
