package com.ylink.rule.cache.entity;

import io.netty.channel.ChannelId;
import io.netty.handler.codec.mqtt.MqttPublishMessage;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * @BelongsProject: ylinkMqtt
 * @BelongsPackage: com.example.ylinkmqtt.cache.entity
 * @Author: shuyu
 * @CreateTime: 2022-11-06  15:21
 * @Description: 会话存储
 * @Version: 1.0
 */
@Data
public class SessionStore implements Serializable {
    private static final long serialVersionUID = -1L;

    private String rootId;

    private String clientId;


    private ChannelId channelId;

    private String dataType;

    private String type;

    private Set<String> topics;

    private int expire;



    private MqttPublishMessage willMassage;

}
