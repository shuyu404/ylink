package com.ylink.rule.config;

import org.apache.iotdb.session.pool.SessionPool;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.broker.config
 * @Author: shuyu
 * @CreateTime: 2022-11-11  11:13
 * @Description:
 * @Version: 1.0
 */
@Configuration
public class IotdbConfig {

    @Value("${iotdb.host}")
    private String host;

    @Value("${iotdb.port}")
    private int port;

    @Value("${iotdb.username}")
    private String username;

    @Value("${iotdb.password}")
    private String password;

    @Value("${iotdb.poolSize}")
    private int poolSize;

    public static int searchSize;

    public static Long TTL;


    @Value("${iotdb.searchSize:200}")
    public void setSearchSize(int searchSize){
        IotdbConfig.searchSize=searchSize;
    }

    @Value("${iotdb.ttl}")
    public static void setTTL(Long TTL) {
        IotdbConfig.TTL = TTL;
    }

    @Bean
    public SessionPool iotdbSessionPool(){
        return new SessionPool(host,port,username,password,poolSize);
    }

}
