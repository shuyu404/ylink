package com.ylink.rule.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ylink.rule.entity.RuleDevice;

/**
 * @author Alie
 */

public interface RuleDeviceService extends IService<RuleDevice> {

    /**
     * 获取设备绑定规则
     * @param deviceId 设备id
     * @param dataId    数据id
     * @return  绑定规则
     */
    RuleDevice getByDeviceAndDataId(String deviceId, String dataId);

    /**
     * 添加信息
     * @param ruleDevice 绑定关系
     * @return  是否执行成功
     */
    boolean add(RuleDevice ruleDevice);

    /**
     * 删除绑定关系
     * @param deviceId 设备id
     * @param dataId 数据id
     * @return 是否执行成功
     */
    boolean delete(String deviceId,String dataId);

    /**
     * 删除触发器所有绑定
     * @param triggerId 绑定触发器id
     * @return  是否执行成功
     */
    void deleteByTriggerId(String triggerId);
}
