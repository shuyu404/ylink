package com.ylink.rule.protocol;

import com.ylink.rule.cache.service.SubscribeStoreService;
import com.ylink.rule.constant.SessionConstant;
import io.netty.channel.Channel;
import io.netty.handler.codec.mqtt.*;
import io.netty.util.AttributeKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: ylinkMqtt
 * @BelongsPackage: com.example.ylinkmqtt.handler
 * @Author: shuyu
 * @CreateTime: 2022-11-06  22:08
 * @Description:
 * @Version: 1.0
 */
@Component
public class Subscribe {
    private static final Logger LOGGER = LoggerFactory.getLogger(Subscribe.class);

    public void subscribe(Channel channel, MqttSubscribeMessage message) {

        String clientId = (String) channel.attr(AttributeKey.valueOf(SessionConstant.CLIENT_ID)).get();

        List<Integer> mqttQosList = new ArrayList<Integer>();
        for (MqttTopicSubscription topicSubscription : message.payload().topicSubscriptions()) {
            SubscribeStoreService.put(topicSubscription.topicName(), clientId);
            MqttQoS mqttQoS = topicSubscription.qualityOfService();
            mqttQosList.add(mqttQoS.value());
        }

        MqttSubAckMessage subAckMessage = (MqttSubAckMessage) MqttMessageFactory.newMessage(
                new MqttFixedHeader(MqttMessageType.SUBACK, false, MqttQoS.AT_MOST_ONCE, false, 0),
                MqttMessageIdVariableHeader.from(message.variableHeader().messageId()),
                new MqttSubAckPayload(mqttQosList));
        channel.writeAndFlush(subAckMessage);
    }
}
