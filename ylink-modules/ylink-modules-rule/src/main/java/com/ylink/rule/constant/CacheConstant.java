package com.ylink.rule.constant;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.constant
 * @Author: shuyu
 * @CreateTime: 2022-12-22  17:11
 * @Description:
 * @Version: 1.0
 */

public class CacheConstant {

    public static final String RULE_DEVICE_DATA = "rule:device_data";

    public static final String RULE_TRIGGER = "rule:trigger";

    public static final String RULE_LINKAGE = "rule:linkage";

    public static final String RULE_LINKAGE_DEVICE = "rule:linkage_device";
}
