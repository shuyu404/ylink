package com.ylink.rule.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ylink.rule.constant.CacheConstant;
import com.ylink.rule.entity.RuleLinkageDevice;
import com.ylink.rule.mapper.RuleLinkageDeviceMapper;
import com.ylink.rule.service.RuleDeviceService;
import com.ylink.rule.service.RuleLinkageDeviceService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.service.impl
 * @Author: shuyu
 * @CreateTime: 2022-12-26  11:19
 * @Description:
 * @Version: 1.0
 */
@Service
public class RuleLinkageDeviceServiceImpl extends ServiceImpl<RuleLinkageDeviceMapper, RuleLinkageDevice> implements RuleLinkageDeviceService {


    @Override
    public void addList(List<RuleLinkageDevice> ruleLinkageDevices) {
        if (ruleLinkageDevices.size()>0){
            delete(ruleLinkageDevices.get(0).getLinkageId());
            ruleLinkageDevices.forEach(e->{
                this.baseMapper.insert(e);
            });
        }
    }

    @Override
    @CacheEvict(value = CacheConstant.RULE_LINKAGE_DEVICE,key = "#root.args[0]")
    public void delete(String linkageId) {
        this.baseMapper.delete(new QueryWrapper<RuleLinkageDevice>().eq("linkage_id",linkageId));
    }

    @Override
    @CacheEvict(value = CacheConstant.RULE_LINKAGE_DEVICE,key = "#root.args[0]")
    public List<RuleLinkageDevice> getList(String linkageId) {
        return this.baseMapper.selectList(new QueryWrapper<RuleLinkageDevice>().eq("linkage_id", linkageId));
    }
}
