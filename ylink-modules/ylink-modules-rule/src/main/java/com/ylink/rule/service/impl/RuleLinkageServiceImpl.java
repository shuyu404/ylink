package com.ylink.rule.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ylink.rule.constant.CacheConstant;
import com.ylink.rule.constant.CommonConstant;
import com.ylink.rule.entity.RuleLinkage;
import com.ylink.rule.mapper.RuleLinkageMapper;
import com.ylink.rule.service.RuleLinkageService;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.service.impl
 * @Author: shuyu
 * @CreateTime: 2022-12-26  11:18
 * @Description:
 * @Version: 1.0
 */
@Service
public class RuleLinkageServiceImpl extends ServiceImpl<RuleLinkageMapper, RuleLinkage> implements RuleLinkageService {

    @Override
    public List<RuleLinkage> getList(String rootId) {
        return this.baseMapper.selectList(new QueryWrapper<RuleLinkage>().eq("root_id", rootId));
    }

    @Override
    @CacheEvict(value = CacheConstant.RULE_DEVICE_DATA+":listBytrigger",key = "#root.args[0].triggerId")
    public boolean add(RuleLinkage ruleLinkage) {
        ruleLinkage.setCreateTime(new Date());
        ruleLinkage.setStatus(CommonConstant.OPEN);
        return this.baseMapper.insert(ruleLinkage) > 0;
    }

    @Override
    public boolean delete(String linkageId) {
        return this.baseMapper.delete(new QueryWrapper<RuleLinkage>().eq("id", linkageId)) > 0;
    }

    @Override
    public RuleLinkage getInfo(String linkageId) {
        return this.baseMapper.selectById(linkageId);
    }


    @Override
    @CacheEvict(value = CacheConstant.RULE_DEVICE_DATA+":listBytrigger",key = "#root.args[0].triggerId")
    public boolean update(RuleLinkage ruleLinkage) {
        System.out.println("up");
        System.out.println(ruleLinkage.getTriggerId());
        return this.baseMapper.updateById(ruleLinkage) > 0;
    }


    @Override
    @Cacheable(value = CacheConstant.RULE_DEVICE_DATA+":listBytrigger",key = "#root.args[0]")
    public List<RuleLinkage> getListByTriggerId(String triggerId) {
        return this.baseMapper.selectList(new QueryWrapper<RuleLinkage>().eq("trigger_id", triggerId));
    }

}
