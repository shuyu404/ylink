package com.ylink.rule.kafka;

import com.ylink.rule.entity.RuleTrigger;

import java.util.logging.Logger;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.kafka
 * @Author: shuyu
 * @CreateTime: 2022-12-22  18:28
 * @Description:
 * @Version: 1.0
 */

public class HandlerData {
    final static int EQUALS = 1;
    final static int SMALL = 2;
    final static int BIG = 3;
    final static int BIG_A_AND_SMALL_B = 4;
    final static int SMALL_A_OR_BIG_B = 5;
    final static float EPSILON = 1f - 5;

    public static boolean handlerData(DataUnit dataUnit) {
        return getDataStatus(dataUnit);
    }

    /**
     * true为报警
     *
     * @param dataUnit
     * @return
     */
    static boolean getDataStatus(DataUnit dataUnit) {
        RuleTrigger ruleTrigger = dataUnit.getRuleTrigger();
        float value = Float.parseFloat(dataUnit.getValue());

        Float numberA = ruleTrigger.getNumberA();
        Float numberB = ruleTrigger.getNumberB();
        switch (ruleTrigger.getType()) {
            case EQUALS:
                if (numberA == null) {
                    return false;
                }
                return (numberA - value) < EPSILON;
            case SMALL:
                if (numberA == null) {
                    return false;
                }
                return value < numberA;
            case BIG:
                if (numberA == null) {
                    return false;
                }
                return value > numberA;
            case BIG_A_AND_SMALL_B:
                if (numberA == null || numberB == null) {
                    return false;
                }
                return numberA < value && value < numberB;
            case SMALL_A_OR_BIG_B:
                if (numberA == null || numberB == null) {
                    return false;
                }
                return value < numberA || value > numberB;
            default:
                return false;
        }
    }
}
