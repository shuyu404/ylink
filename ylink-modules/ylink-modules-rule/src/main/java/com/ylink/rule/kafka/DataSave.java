package com.ylink.rule.kafka;

import lombok.Data;

import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.kafka
 * @Author: shuyu
 * @CreateTime: 2022-12-21  19:46
 * @Description:
 * @Version: 1.0
 */
@Data
public class DataSave {
    String rootId;

    String clientId;

    /**
     * 数据名称列表
     */
    List<String> names;
    /**
     * 设备名称
     */
    String deviceName;

    /**
     * 数组列表
     */
    List<String> values;
    /**
     * 列表名称
     */
    List<String> dataNames;
    /**
     * 数据列表
     */
    List<String> types;

    /**
     * 时间
     */
    Long time;
}
