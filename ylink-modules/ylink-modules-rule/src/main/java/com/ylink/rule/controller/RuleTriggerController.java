package com.ylink.rule.controller;

import com.ylink.common.core.domain.R;
import com.ylink.common.core.web.controller.BaseController;
import com.ylink.common.core.web.page.TableDataInfo;
import com.ylink.common.core.web.vo.SelectOption;
import com.ylink.common.security.utils.SecurityUtils;
import com.ylink.rule.entity.RuleTrigger;
import com.ylink.rule.service.RuleTriggerService;
import com.ylink.system.api.entity.SysUser;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule.controller
 * @Author: shuyu
 * @CreateTime: 2022-12-22  10:56
 * @Description:
 * @Version: 1.0
 */
@RestController
@RequestMapping("/trigger")
public class RuleTriggerController extends BaseController {

    @Autowired
    RuleTriggerService ruleTriggerService;

    @GetMapping("/list")
    public TableDataInfo getList() {
        String rootId = SecurityUtils.getSysUser().getRootId();
        startPage();
        List<RuleTrigger> list = ruleTriggerService.getList(rootId);
        return getDataTable(list);
    }

    @GetMapping("/selectList")
    public R getSelectList() {
        String rootId = SecurityUtils.getSysUser().getRootId();
        List<RuleTrigger> list = ruleTriggerService.getList(rootId);
        List<SelectOption> selectOptions = new ArrayList<>();
        list.forEach(e -> {
            selectOptions.add(new SelectOption(e.getName(), e.getId()));
        });
        return R.ok().put("data", selectOptions);
    }

    @PostMapping("/add")
    public R add(@RequestBody RuleTrigger ruleTrigger) {
        SysUser sysUser = SecurityUtils.getSysUser();
        ruleTrigger.setRootId(sysUser.getRootId());
        ruleTrigger.setCreateBy(sysUser.getUserId());
        boolean b = ruleTriggerService.add(ruleTrigger);
        if (b) {
            return R.ok();
        } else {
            return R.error();
        }
    }

    @DeleteMapping("/delete/{triggerId}")
    public R delete(@PathVariable("triggerId")String triggerId){
        boolean b= ruleTriggerService.delete(triggerId);
        if(b){
            return R.ok();
        }else {
            return R.error();
        }
    }

    @PutMapping("/update")
    public R update(@RequestBody RuleTrigger ruleTrigger){
        String userId = SecurityUtils.getSysUser().getUserId();
        ruleTrigger.setUpdateBy(userId);
        boolean b= ruleTriggerService.update(ruleTrigger);
        if (b){
            return R.ok();
        }else {
            return R.error();
        }
    }
}
