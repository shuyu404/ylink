package com.ylink.rule.cache.service;

import com.ylink.rule.cache.entity.SessionStore;
import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @BelongsProject: ylinkMqtt
 * @BelongsPackage: com.example.ylinkmqtt.cache.service.impl
 * @Author: shuyu
 * @CreateTime: 2022-11-06  15:27
 * @Description:
 * @Version: 1.0
 */
@Component
public class SessionStoreService {



    private static final Logger LOGGER = LoggerFactory.getLogger(ChannelService.class);

    private static final Map<String, SessionStore> SESSION_STORE_MAP = new ConcurrentHashMap<>();

    public static void put(String clientId, SessionStore sessionStore) {
        SESSION_STORE_MAP.put(clientId, sessionStore);
        //更新缓存

        LOGGER.info(String.format("session增加当前session为：%d", SESSION_STORE_MAP.size()));
    }


    public static SessionStore getSessionStore(String clientId) {
        return SESSION_STORE_MAP.get(clientId);
    }


    public static void remove(String clientId) {
        SubscribeStoreService.removeForClient(clientId);
        SESSION_STORE_MAP.remove(clientId);
        //更新缓存内容


        LOGGER.info(String.format("session减少当前session为：%d", SESSION_STORE_MAP.size()));
    }

    public static boolean hasSession(String clientId) {
        return getSessionStore(clientId) != null;
    }


    /**session初始化*/
    public static void initSession(Channel channel, SessionStore sessionStore) {

        sessionStore.setChannelId(channel.id());

    }

}
