package com.ylink.rule;

import com.ylink.rule.codec.WebSocketMqttCodec;
import com.ylink.rule.config.BrokerConfig;
import com.ylink.rule.handler.BrokerHandler;
import com.ylink.rule.handler.WebSocketHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpContentCompressor;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.codec.mqtt.MqttDecoder;
import io.netty.handler.codec.mqtt.MqttEncoder;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @BelongsProject: ylink
 * @BelongsPackage: com.ylink.rule
 * @Author: shuyu
 * @CreateTime: 2022-12-23  09:11
 * @Description: mqtt启动类
 * @Version: 1.0
 */
@ChannelHandler.Sharable
@Component
public class BootStrap {
    @Autowired
    public BootStrap(BrokerHandler brokerHandler,WebSocketHandler webSocketHandler) {
        this.brokerHandler = brokerHandler;
    }

    private EventLoopGroup bossGroup;
    private EventLoopGroup workGroup;
    private final BrokerHandler brokerHandler;

    @PostConstruct
    public void start() {
        bossGroup = new NioEventLoopGroup();
        workGroup = new NioEventLoopGroup();
        try {
            mqttStart();
            webSocketStart();
            System.out.println("broker启动成功");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void mqttStart() throws InterruptedException {
        ServerBootstrap serverBootstrap = new ServerBootstrap();
        serverBootstrap.group(bossGroup, workGroup).channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        ChannelPipeline pipeline = socketChannel.pipeline();
                        //添加心跳检测
                        pipeline.addFirst("idle", new IdleStateHandler(0, 0, BrokerConfig.OUT_TIME));
                        // 将请求和应答消息编码或解码为HTTP消息
                        pipeline.addLast("http-codec", new HttpServerCodec());
                        // 将HTTP消息的多个部分合成一条完整的HTTP消息
                        pipeline.addLast("aggregator", new HttpObjectAggregator(1048576));
                        // 将HTTP消息进行压缩编码
                        pipeline.addLast("compressor ", new HttpContentCompressor());
                        pipeline.addLast("protocol", new WebSocketServerProtocolHandler(BrokerConfig.WS_PATH, "mqtt,mqttv3.1,mqttv3.1.1", true, 65536));
                        //对websocket进行解码
                        pipeline.addLast("mqttWebSocket", new WebSocketMqttCodec());
                        pipeline.addLast("decoder", new MqttDecoder());
                        pipeline.addLast("encoder", MqttEncoder.INSTANCE);
                        //进行Mqtt处理
                        pipeline.addLast("borker", brokerHandler);
                    }
                }).option(ChannelOption.SO_KEEPALIVE, BrokerConfig.IS_KEEP_ALIVE);
        serverBootstrap.bind(BrokerConfig.WS_PORT).sync();
    }


    private void webSocketStart() throws InterruptedException {
        ServerBootstrap serverBootstrap = new ServerBootstrap();
        serverBootstrap.group(bossGroup,workGroup).channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel socketChannel) throws Exception {
                        System.out.println("接收到新连接");
                        //websocket基于http先进行http解码
                        socketChannel.pipeline().addLast(new HttpServerCodec());
                        //以块的方式来处理
                        socketChannel.pipeline().addLast(new ChunkedWriteHandler());
                        socketChannel.pipeline().addLast(new HttpObjectAggregator(8192));
                        socketChannel.pipeline().addLast(new WebSocketServerProtocolHandler("/ws",null,true,65536*10));
                        socketChannel.pipeline().addLast(new WebSocketHandler());
                    }
                }).option(ChannelOption.SO_KEEPALIVE,BrokerConfig.IS_KEEP_ALIVE);
        serverBootstrap.bind(BrokerConfig.WS).sync();
    }
}
