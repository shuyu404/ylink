# 系统部署

部署系统：Linux 性能要求：2c8g



## 安装中间件



1. 安装docker

   安装前置工具

   ```sh
   sudo yum install -y yum-utils
   sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
   ```

   安装docker引擎

   ```sh
   sudo yum install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
   ```

   启动容器

   ```sh
   sudo systemctl start docker
   ```

   设置容器自启

   ```sh
   sudo systemctl start docker
   ```

   **创建docker网络**

   ```sh
   docker network create --driver bridge --subnet=172.18.100.0/24 --gateway=172.18.100.1 mynet
   #获取docker 网络相关信息
   docker network inspect mynet
   ```

   

2. 安装mysql

   启动mysql并且同时指定初始化网络和ip

   ```sh
   docker run --network=mynet \
   --ip=172.18.100.10 \
   --name mysql -e MYSQL_ROOT_PASSWORD=shuyu404 \
   -p 3306:3306 -d mysql
   ```

3. 安装Nacos

   先创建数据库并且导入 数据库文件 在nacos发行版conf中nacos-config 也可直接导入下述文件

   ```sql
   /******************************************/
   /*   数据库全名 = nacos_config   */
   /*   表名称 = config_info   */
   /******************************************/
   CREATE TABLE `config_info` (
     `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
     `data_id` varchar(255) NOT NULL COMMENT 'data_id',
     `group_id` varchar(255) DEFAULT NULL,
     `content` longtext NOT NULL COMMENT 'content',
     `md5` varchar(32) DEFAULT NULL COMMENT 'md5',
     `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
     `src_user` text COMMENT 'source user',
     `src_ip` varchar(20) DEFAULT NULL COMMENT 'source ip',
     `app_name` varchar(128) DEFAULT NULL,
     `tenant_id` varchar(128) DEFAULT '' COMMENT '租户字段',
     `c_desc` varchar(256) DEFAULT NULL,
     `c_use` varchar(64) DEFAULT NULL,
     `effect` varchar(64) DEFAULT NULL,
     `type` varchar(64) DEFAULT NULL,
     `c_schema` text,
     PRIMARY KEY (`id`),
     UNIQUE KEY `uk_configinfo_datagrouptenant` (`data_id`,`group_id`,`tenant_id`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='config_info';
   
   /******************************************/
   /*   数据库全名 = nacos_config   */
   /*   表名称 = config_info_aggr   */
   /******************************************/
   CREATE TABLE `config_info_aggr` (
     `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
     `data_id` varchar(255) NOT NULL COMMENT 'data_id',
     `group_id` varchar(255) NOT NULL COMMENT 'group_id',
     `datum_id` varchar(255) NOT NULL COMMENT 'datum_id',
     `content` longtext NOT NULL COMMENT '内容',
     `gmt_modified` datetime NOT NULL COMMENT '修改时间',
     `app_name` varchar(128) DEFAULT NULL,
     `tenant_id` varchar(128) DEFAULT '' COMMENT '租户字段',
     PRIMARY KEY (`id`),
     UNIQUE KEY `uk_configinfoaggr_datagrouptenantdatum` (`data_id`,`group_id`,`tenant_id`,`datum_id`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='增加租户字段';
   
   
   /******************************************/
   /*   数据库全名 = nacos_config   */
   /*   表名称 = config_info_beta   */
   /******************************************/
   CREATE TABLE `config_info_beta` (
     `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
     `data_id` varchar(255) NOT NULL COMMENT 'data_id',
     `group_id` varchar(128) NOT NULL COMMENT 'group_id',
     `app_name` varchar(128) DEFAULT NULL COMMENT 'app_name',
     `content` longtext NOT NULL COMMENT 'content',
     `beta_ips` varchar(1024) DEFAULT NULL COMMENT 'betaIps',
     `md5` varchar(32) DEFAULT NULL COMMENT 'md5',
     `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
     `src_user` text COMMENT 'source user',
     `src_ip` varchar(20) DEFAULT NULL COMMENT 'source ip',
     `tenant_id` varchar(128) DEFAULT '' COMMENT '租户字段',
     PRIMARY KEY (`id`),
     UNIQUE KEY `uk_configinfobeta_datagrouptenant` (`data_id`,`group_id`,`tenant_id`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='config_info_beta';
   
   /******************************************/
   /*   数据库全名 = nacos_config   */
   /*   表名称 = config_info_tag   */
   /******************************************/
   CREATE TABLE `config_info_tag` (
     `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
     `data_id` varchar(255) NOT NULL COMMENT 'data_id',
     `group_id` varchar(128) NOT NULL COMMENT 'group_id',
     `tenant_id` varchar(128) DEFAULT '' COMMENT 'tenant_id',
     `tag_id` varchar(128) NOT NULL COMMENT 'tag_id',
     `app_name` varchar(128) DEFAULT NULL COMMENT 'app_name',
     `content` longtext NOT NULL COMMENT 'content',
     `md5` varchar(32) DEFAULT NULL COMMENT 'md5',
     `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
     `src_user` text COMMENT 'source user',
     `src_ip` varchar(20) DEFAULT NULL COMMENT 'source ip',
     PRIMARY KEY (`id`),
     UNIQUE KEY `uk_configinfotag_datagrouptenanttag` (`data_id`,`group_id`,`tenant_id`,`tag_id`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='config_info_tag';
   
   /******************************************/
   /*   数据库全名 = nacos_config   */
   /*   表名称 = config_tags_relation   */
   /******************************************/
   CREATE TABLE `config_tags_relation` (
     `id` bigint(20) NOT NULL COMMENT 'id',
     `tag_name` varchar(128) NOT NULL COMMENT 'tag_name',
     `tag_type` varchar(64) DEFAULT NULL COMMENT 'tag_type',
     `data_id` varchar(255) NOT NULL COMMENT 'data_id',
     `group_id` varchar(128) NOT NULL COMMENT 'group_id',
     `tenant_id` varchar(128) DEFAULT '' COMMENT 'tenant_id',
     `nid` bigint(20) NOT NULL AUTO_INCREMENT,
     PRIMARY KEY (`nid`),
     UNIQUE KEY `uk_configtagrelation_configidtag` (`id`,`tag_name`,`tag_type`),
     KEY `idx_tenant_id` (`tenant_id`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='config_tag_relation';
   
   /******************************************/
   /*   数据库全名 = nacos_config   */
   /*   表名称 = group_capacity   */
   /******************************************/
   CREATE TABLE `group_capacity` (
     `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
     `group_id` varchar(128) NOT NULL DEFAULT '' COMMENT 'Group ID，空字符表示整个集群',
     `quota` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '配额，0表示使用默认值',
     `usage` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '使用量',
     `max_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
     `max_aggr_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '聚合子配置最大个数，，0表示使用默认值',
     `max_aggr_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
     `max_history_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最大变更历史数量',
     `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
     PRIMARY KEY (`id`),
     UNIQUE KEY `uk_group_id` (`group_id`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='集群、各Group容量信息表';
   
   /******************************************/
   /*   数据库全名 = nacos_config   */
   /*   表名称 = his_config_info   */
   /******************************************/
   CREATE TABLE `his_config_info` (
     `id` bigint(64) unsigned NOT NULL,
     `nid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
     `data_id` varchar(255) NOT NULL,
     `group_id` varchar(128) NOT NULL,
     `app_name` varchar(128) DEFAULT NULL COMMENT 'app_name',
     `content` longtext NOT NULL,
     `md5` varchar(32) DEFAULT NULL,
     `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
     `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
     `src_user` text,
     `src_ip` varchar(20) DEFAULT NULL,
     `op_type` char(10) DEFAULT NULL,
     `tenant_id` varchar(128) DEFAULT '' COMMENT '租户字段',
     PRIMARY KEY (`nid`),
     KEY `idx_gmt_create` (`gmt_create`),
     KEY `idx_gmt_modified` (`gmt_modified`),
     KEY `idx_did` (`data_id`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='多租户改造';
   
   
   /******************************************/
   /*   数据库全名 = nacos_config   */
   /*   表名称 = tenant_capacity   */
   /******************************************/
   CREATE TABLE `tenant_capacity` (
     `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
     `tenant_id` varchar(128) NOT NULL DEFAULT '' COMMENT 'Tenant ID',
     `quota` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '配额，0表示使用默认值',
     `usage` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '使用量',
     `max_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单个配置大小上限，单位为字节，0表示使用默认值',
     `max_aggr_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '聚合子配置最大个数',
     `max_aggr_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '单个聚合数据的子配置大小上限，单位为字节，0表示使用默认值',
     `max_history_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最大变更历史数量',
     `gmt_create` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     `gmt_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
     PRIMARY KEY (`id`),
     UNIQUE KEY `uk_tenant_id` (`tenant_id`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='租户容量信息表';
   
   
   CREATE TABLE `tenant_info` (
     `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
     `kp` varchar(128) NOT NULL COMMENT 'kp',
     `tenant_id` varchar(128) default '' COMMENT 'tenant_id',
     `tenant_name` varchar(128) default '' COMMENT 'tenant_name',
     `tenant_desc` varchar(256) DEFAULT NULL COMMENT 'tenant_desc',
     `create_source` varchar(32) DEFAULT NULL COMMENT 'create_source',
     `gmt_create` bigint(20) NOT NULL COMMENT '创建时间',
     `gmt_modified` bigint(20) NOT NULL COMMENT '修改时间',
     PRIMARY KEY (`id`),
     UNIQUE KEY `uk_tenant_info_kptenantid` (`kp`,`tenant_id`),
     KEY `idx_tenant_id` (`tenant_id`)
   ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='tenant_info';
   
   CREATE TABLE `users` (
   	`username` varchar(50) NOT NULL PRIMARY KEY,
   	`password` varchar(500) NOT NULL,
   	`enabled` boolean NOT NULL
   );
   
   CREATE TABLE `roles` (
   	`username` varchar(50) NOT NULL,
   	`role` varchar(50) NOT NULL,
   	UNIQUE INDEX `idx_user_role` (`username` ASC, `role` ASC) USING BTREE
   );
   
   CREATE TABLE `permissions` (
       `role` varchar(50) NOT NULL,
       `resource` varchar(512) NOT NULL,
       `action` varchar(8) NOT NULL,
       UNIQUE INDEX `uk_role_permission` (`role`,`resource`,`action`) USING BTREE
   );
   
   INSERT INTO users (username, password, enabled) VALUES ('nacos', '$2a$10$EuWPZHzz32dJN7jexM34MOeYirDdFAZm2kuWj7VEOJhhZkDrxfvUu', TRUE);
   
   INSERT INTO roles (username, role) VALUES ('nacos', 'ROLE_ADMIN');
   
   ```

   

   启动nacos并且配置持久化

   ```sh
   docker run -d -p 8848:8848 \
   --name nacos \
   --network=mynet \
   --ip 172.18.100.11 \
   -e MODE=standalone \
   -e SPRING_DATASOURCE_PLATFORM=mysql \
   -e MYSQL_SERVICE_HOST=172.18.100.10 \
   -e MYSQL_SERVICE_PORT=3306 \
   -e MYSQL_SERVICE_DB_NAME=nacos \
   -e MYSQL_SERVICE_USER=root \
   -e MYSQL_SERVICE_PASSWORD=shuyu404 \
   nacos/nacos-server
   
   ```

   

4. 安装kafka

   单机模式安装Kafka

   compose方式

   ```sh
   version: "3.6"
   services:
     kafka1:
       container_name: kafka1
       image: 'bitnami/kafka:3.3.1'
       user: root
       ports:
         - '19092:9092'
         - '19093:9093'
       environment:
         # 允许使用Kraft
         - KAFKA_ENABLE_KRAFT=yes
         - KAFKA_CFG_PROCESS_ROLES=broker,controller
         - KAFKA_CFG_CONTROLLER_LISTENER_NAMES=CONTROLLER
         # 定义kafka服务端socket监听端口（Docker内部的ip地址和端口）
         - KAFKA_CFG_LISTENERS=PLAINTEXT://:9092,CONTROLLER://:9093
         # 定义安全协议
         - KAFKA_CFG_LISTENER_SECURITY_PROTOCOL_MAP=CONTROLLER:PLAINTEXT,PLAINTEXT:PLAINTEXT
         #定义外网访问地址（宿主机ip地址和端口）
         - KAFKA_CFG_ADVERTISED_LISTENERS=PLAINTEXT://11.21.13.15:19092
         - KAFKA_BROKER_ID=1
         - KAFKA_KRAFT_CLUSTER_ID=iZWRiSqjZAlYwlKEqHFQWI
         - KAFKA_CFG_CONTROLLER_QUORUM_VOTERS=1@172.23.0.11:9093,2@172.23.0.12:9093,3@172.23.0.13:9093
         - ALLOW_PLAINTEXT_LISTENER=yes
         # 设置broker最大内存，和初始内存
         - KAFKA_HEAP_OPTS=-Xmx512M -Xms256M
       volumes:
         - /opt/volume/kafka/broker01:/bitnami/kafka:rw
       networks:
         netkafka:
           ipv4_address: 172.23.0.11
     kafka2:
       container_name: kafka2
       image: 'bitnami/kafka:3.3.1'
       user: root
       ports:
         - '29092:9092'
         - '29093:9093'
       environment:
         - KAFKA_ENABLE_KRAFT=yes
         - KAFKA_CFG_PROCESS_ROLES=broker,controller
         - KAFKA_CFG_CONTROLLER_LISTENER_NAMES=CONTROLLER
         - KAFKA_CFG_LISTENERS=PLAINTEXT://:9092,CONTROLLER://:9093
         - KAFKA_CFG_LISTENER_SECURITY_PROTOCOL_MAP=CONTROLLER:PLAINTEXT,PLAINTEXT:PLAINTEXT
         - KAFKA_CFG_ADVERTISED_LISTENERS=PLAINTEXT://11.21.13.15:29092  #修改宿主机ip
         - KAFKA_BROKER_ID=2
         - KAFKA_KRAFT_CLUSTER_ID=iZWRiSqjZAlYwlKEqHFQWI #哪一，三个节点保持一致
         - KAFKA_CFG_CONTROLLER_QUORUM_VOTERS=1@172.23.0.11:9093,2@172.23.0.12:9093,3@172.23.0.13:9093
         - ALLOW_PLAINTEXT_LISTENER=yes
         - KAFKA_HEAP_OPTS=-Xmx512M -Xms256M
       volumes:
         - /opt/volume/kafka/broker02:/bitnami/kafka:rw
       networks:
         netkafka:
           ipv4_address: 172.23.0.12
     kafka3:
       container_name: kafka3
       image: 'bitnami/kafka:3.3.1'
       user: root
       ports:
         - '39092:9092'
         - '39093:9093'
       environment:
         - KAFKA_ENABLE_KRAFT=yes
         - KAFKA_CFG_PROCESS_ROLES=broker,controller
         - KAFKA_CFG_CONTROLLER_LISTENER_NAMES=CONTROLLER
         - KAFKA_CFG_LISTENERS=PLAINTEXT://:9092,CONTROLLER://:9093
         - KAFKA_CFG_LISTENER_SECURITY_PROTOCOL_MAP=CONTROLLER:PLAINTEXT,PLAINTEXT:PLAINTEXT
         - KAFKA_CFG_ADVERTISED_LISTENERS=PLAINTEXT://11.21.13.15:39092  #修改宿主机ip
         - KAFKA_BROKER_ID=3
         - KAFKA_KRAFT_CLUSTER_ID=iZWRiSqjZAlYwlKEqHFQWI
         - KAFKA_CFG_CONTROLLER_QUORUM_VOTERS=1@172.23.0.11:9093,2@172.23.0.12:9093,3@172.23.0.13:9093
         - ALLOW_PLAINTEXT_LISTENER=yes
         - KAFKA_HEAP_OPTS=-Xmx512M -Xms256M
       volumes:
         - /opt/volume/kafka/broker03:/bitnami/kafka:rw
       networks:
         netkafka:
           ipv4_address: 172.23.0.13
   networks:
     name:
     netkafka:
       driver: bridge
       name: netkafka
       ipam:
         driver: default
         config:
           - subnet: 172.23.0.0/25
             gateway: 172.23.0.1
   
   ```

   使用命令行的方式

   ```sh
   version: "3.6"
   services:
     kafka1:
       container_name: kafka1
       image: 'bitnami/kafka:3.3.1'
       user: root
       ports:
         - '19092:9092'
         - '19093:9093'
       environment:
         # 允许使用Kraft
         - KAFKA_ENABLE_KRAFT=yes
         - KAFKA_CFG_PROCESS_ROLES=broker,controller
         - KAFKA_CFG_CONTROLLER_LISTENER_NAMES=CONTROLLER
         # 定义kafka服务端socket监听端口（Docker内部的ip地址和端口）
         - KAFKA_CFG_LISTENERS=PLAINTEXT://:9092,CONTROLLER://:9093
         # 定义安全协议
         - KAFKA_CFG_LISTENER_SECURITY_PROTOCOL_MAP=CONTROLLER:PLAINTEXT,PLAINTEXT:PLAINTEXT
         #定义外网访问地址（宿主机ip地址和端口）
         - KAFKA_CFG_ADVERTISED_LISTENERS=PLAINTEXT://11.21.13.15:19092
         - KAFKA_BROKER_ID=1
         - KAFKA_KRAFT_CLUSTER_ID=iZWRiSqjZAlYwlKEqHFQWI
         - KAFKA_CFG_CONTROLLER_QUORUM_VOTERS=1@172.23.0.11:9093,2@172.23.0.12:9093,3@172.23.0.13:9093
         - ALLOW_PLAINTEXT_LISTENER=yes
         # 设置broker最大内存，和初始内存
         - KAFKA_HEAP_OPTS=-Xmx512M -Xms256M
       volumes:
         - /opt/volume/kafka/broker01:/bitnami/kafka:rw
       networks:
         netkafka:
           ipv4_address: 172.23.0.11
           
   docker run -d -p 9092:9092 -p 9093:9093 \
   --name=kafka \
   --network=mynet \
   --ip=172.18.100.12 \
   -e KAFKA_ENABLE_KRAFT=yes \
   -e KAFKA_CFG_PROCESS_ROLES=broker,controller \
   -e KAFKA_CFG_LISTENERS=PLAINTEXT://:9092,CONTROLLER://:9093 \
   -e KAFKA_CFG_ADVERTISED_LISTENERS=PLAINTEXT://cloud.zqwl.cloud:9092 \
   -e KAFKA_CFG_CONTROLLER_LISTENER_NAMES=CONTROLLER \
   -e KAFKA_BROKER_ID=1 \
   -e KAFKA_CFG_NODE_ID=1 \
   -e KAFKA_KRAFT_CLUSTER_ID=iZWRiSqjZAlYwlKEqHFQWI \
   -e KAFKA_CFG_CONTROLLER_QUORUM_VOTERS=1@172.18.100.12:9093 \
   -e ALLOW_PLAINTEXT_LISTENER=yes \
   bitnami/kafka
   ```

   

5. 安装iotdb

   ```sh
   docker run -d -p 6667:6667 -p 31999:31999 -p 8181:8181 \
   -m 1g \
   --network=mynet \
   --ip=172.18.100.13 \
   --name iotdb apache/iotdb:0.12.5
   ```

   

6. 安装redis

   ```sh
   docker run -d  -p 6379:6379 \
   --network=mynet \
   --ip=172.18.100.14 \
   --name redis  -d redis --requirepass "123456"
   ```

   



## 安装系统

1. gateway模块

   ```sh
   docker run -d  -p 8808:8808 \
   --network=mynet \
   --ip=172.18.100.20 \
   -e HOST_IP=172.18.100.20 \
   -e NACOS_IP=172.18.100.11 \
   --name gateway  -d registry.cn-shenzhen.aliyuncs.com/y-link/ylink-gateway:0.2
   ```

   

2. auth模块

   ```sh
   docker run -d  -p 9200:9200 \
   -m 300m \
   --network=mynet \
   --ip=172.18.100.21 \
   -e HOST_IP=172.18.100.21 \
   -e NACOS_IP=172.18.100.11 \
   --name auth  -d registry.cn-shenzhen.aliyuncs.com/y-link/ylink-auth:0.2
   ```

   

3. broker模块

   ```sh
   docker run -d  -p 9203:9203 -p 1883:1883  \
   -m 300m \
   --network=mynet \
   --ip=172.18.100.22 \
   -e HOST_IP=172.18.100.22 \
   -e NACOS_IP=172.18.100.11 \
   --name broker  -d registry.cn-shenzhen.aliyuncs.com/y-link/ylink-broker:0.2
   ```

   

   

4. system模块

   ```sh
   docker run -d  -p 9201:9201 \
   -m 300m \
   --network=mynet \
   --ip=172.18.100.23 \
   -e HOST_IP=172.18.100.23 \
   -e NACOS_IP=172.18.100.11 \
   --name system  -d registry.cn-shenzhen.aliyuncs.com/y-link/ylink-system:0.2
   ```

   

5. device模块

   ```sh
   docker run -d  -p 9202:9202 \
   -m 300m \
   --network=mynet \
   --ip=172.18.100.24 \
   -e HOST_IP=172.18.100.24 \
   -e NACOS_IP=172.18.100.11 \
   --name device  -d registry.cn-shenzhen.aliyuncs.com/y-link/ylink-device:0.2
   ```

   

6. rule模块

```sh
docker run -d  -p 9204:9204 -p 8083:8083 -p 18083:18083 \
-m 300m \
--network=mynet \
--ip=172.18.100.25 \
-e HOST_IP=172.18.100.25 \
-e NACOS_IP=172.18.100.11 \
--name rule  -d registry.cn-shenzhen.aliyuncs.com/y-link/ylink-rule:0.2
```

## 前端页面部署

1. 安装nginx中间件

   ```sh
   docker run -d  -p 80:80  \
   -m 300m \
   --network=mynet \
   --ip=172.18.100.31 \
   -v /home/nginx/conf:/etc/nginx \
   -v /home/nginx/html:/usr/share/nginx/html \
   -v /home/nginx/logs:/var/log/nginx \
   --name nginx  -d nginx
   ```

   

2. 更改nginx配置文件

   ```sh
       server {
           listen       80;
           listen  [::]:80;
           server_name  localhost;
   
           #access_log  /var/log/nginx/host.access.log  main;
   
           location / {
               root   /usr/share/nginx/html;
               index  index.html index.htm;
           }
   
          location ^~ /api {
                   root   html;
                   index  index.html index.htm;
                   proxy_pass http://172.18.100.20:8808/api;
               }
   
           #error_page  404              /404.html;
   
           # redirect server error pages to the static page /50x.html
           #
           error_page   500 502 503 504  /50x.html;
           location = /50x.html {
               root   /usr/share/nginx/html;
           }
       }
   ```

   

   

